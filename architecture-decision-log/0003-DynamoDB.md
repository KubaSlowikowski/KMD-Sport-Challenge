# 2. Database costs

Date: 28.07.2024

## Problem
**Reduce the future costs of _MongoDB Atlas_ data storage.**

Now we use MongoDB Atlas Free Tier that provides only 500MB of the storage. Our data size increses and soon we'll reach the limit.

MongoDB Atlas is horribly expensive. The price of storing **only 5GB** of data is $25/month (M5 Cluster Tier). For sure our data will reach 5GB and what then? We'll need to buy a dedicated cluster (the price for 80GB of the storage starts from $2.20/hr=$1500/month **net**).
What is more, M5 clusters are hosted on shared environment, which means that Mongo Atlas doesn't guarantee any stable network performance and slowdowns may occur. It already happened during KMD Summer Challenge 2024 and timeouts were taking place.

## Decision 
We decided to change MongoDB Atlas to **Amazon DynamoDB**.

We did the following steps before we made the decision above:

- First of all, we were trying to stay with MongoDB and find a cheaper hosting solution. 
We checked the most popular/cheapest hosting sites. In most of them, the price was starting at ~$15/month for a little storage size. 
What is more, using hosting sites requires an additional configuration. For those reasons, we decided to abandon this idea and continue our research.
- Knowing that managed MongoDB options, like Atlas, were too expensive, we thought about hosting MongoDB on on-premise servers. We checked AWS, Google and Azure offers.
We quickly abandoned this idea, because for now it is still too expensive (paid for hours), and we'll need to manage the whole server configuration on our own - we don't have time for it now.      
- Then we took a break and considered why we actually use non-relational Database engine and why it is MongoDB? The reason is that our data schema evaluates dynamically, so NoSQL database fits the best. 
Also, we don't know how much space our data will consume after we lunch our application on prod, so it would be nice to scale DB horizontally. 
- Knowing that MongoDB is too expensive, and we need NoSQL database, we started research. DynamoDB seemed to be the most suitable for our current needs.

- **We chose DynamoDB because:**
  - It is NoSQL database provided by AWS. We already have our infrastructure hosted there, so future integration will be much easier (like triggering AWS Lambdas).
  - DynamoDB is a fully managed database platform provided by AWS. This means you don't have to worry about servers, maintenance, etc.
  - For our current needs, it is very affordable: ~$1/month.
  - It is serverless, so we pay only for used resources, and we can easily scale the storage horizontally.
  - There are a lot of similarities between items in MongoDB and DynamoDB. The foundations of both objects are basically the same as they both use key-value pairs structured in a JSON object. 

## Consequences
When it comes to the financial aspect, for now DynamoDB will cost us ~$1/month. 

The biggest challenge will be to create a new database and plan all new tables. We must consider our data access pattern, indexes, etc.
We also need to write a new DynamoDB adapter in Node.js and smoothly redirect the whole traffic there. For sure, it may cause some bugs, so it will require an additional effort to test it. 
Also, we need to remember that we use a MongoDB Atlas trigger that publishes an event to AWS EventBridge to invoke AWS Lambda function to process strava events (`mongoTriggerEventsProcessor`).

We found a cool MongoDB -> DynamoDB migration guide https://dynobase.dev/migrate-from-mongodb-to-dynamodb/.


## Considered alternatives
We pay for each GB of the storage and read,write operation. If our data size increases highly, we need to consider storing it in `AWS S3` buckets to reduce cost.
For example, if our storage takes 100GB, the monthly cost reaches $26 net, but don't worry. Now after 3 challenges in KMD the whole data weights only 31MB).
