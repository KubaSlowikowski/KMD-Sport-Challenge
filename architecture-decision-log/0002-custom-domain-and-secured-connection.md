# 2. Custom domain and secured connection.

Date: 22.07.2024

## Problem
- Provide a secured connection using HTTPS protocol.
- Host our application on custom domain e.g., _mypage.com_.

## Decision
We decided to use the following AWS services:

- `AWS Route 53` to buy and register a custom domain https://sports-app.net, and create a 'hosted zone' that will redirect user to another resource (e.g., static website on `AWS S3 Bucket`).
- `AWS Certificate Manager (ACM)` to request a **free** public TLS/SSL certificate.
- `Amazon CloudFront` to securely deliver content with low latency and high-transfer speeds (CDN). We needed it to provide a secured HTTPS connection, because we host our static website in `S3 Bucket` and HTTPS was not achievable in this setup.

## Consequences
We managed to successfully provide secured custom domain. The whole solution is on the AWS side.

Custom domain costs $15 per year. Hosted zone that is required by Route 53 costs $0.50 per month. AWS Cloudfront is affordable - first 1 TB data transfer free each month and $0.0100 per 10,000 HTTPS requests.


## Considered alternatives
- We could've bought a custom domain from an external provider, but still we'll have to pay for it.
- Request a TLS/SSL certificate from an external provider. It's not worth the trouble, because ACM public certificates are for free. 

## Notes
We based on the following article that describes how to serve a static secured website on S3: https://adamtheautomator.com/aws-s3-static-ssl-website/.
