- Create a new app on https://app.serverless.com/<org-name>/apps.
- Create new or update existing serverless.yaml file. The most important is to change `org` and `app` properties. 
- Locally run all database migration scripts `api/src/db/dynamoDB/scripts` to create DynamoDB tables (including triggers). Remember to update `.env` file in /api folder.
- Run `sls deploy` command in /api folder to deploy the whole backend code on AWS. New CloudFormation stack will be created (mostly AWS Lambdas to jobs and REST API).
- Verify that newly created stack works - test a '/' API hello-world endpoint (for example, https://lorem123.execute-api.us-east-1.amazonaws.com/).
- Create a S3 Bucket with public access enabled where all frontend files will be stored.
- Add newly created API and S3 Bucket URLs to `CORS_WHITELIST`, `REDIRECT_URI_WHITELIST` and configure `environment.ts` file in /frontend directory. Also remember to update environmental variables under `serverless-<env>.yml` (may depend on env name).
- To receive Strava webhook events, a **new subscription must be created**. Update the following Strava properties in serverless-*.yaml config file: `CLIENT_ID`, `CLIENT_SECRET`, `STRAVA_WEBHOOK_CALLBACK_URL` (if you want to run app locally, update `.env` file as well). Remember that Strava-webhooks subscription allows only one application can be connected to a Strava account. 
If your current Strava account is connected to another instance of our application, you need to delete the old subscription or create a new account. 
To list existing subscription invoke [https://{{aws-api-url}}/webhooks/viewSubscription](), to unsubscribe go to [https://{{aws-api-url}}/webhooks/unsubscribe/{{subscription_id}}]()
- Set a proper 'Authorization Callback Domain' at https://www.strava.com/settings/api -> Edit.
- The new app instance UI is available under S3 public address. Go there. You should now be able to join the challenge.

