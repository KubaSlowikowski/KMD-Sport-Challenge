# 1. Introduction

Date: 22.07.2024

Inside this folder, you will be adding a new Architecture Decision Record (ADR) each time you decide on anything related to architecture:
- Adding, modifying, or replacing infrastructure components.

- Deciding for another architectural pattern.

- Using another logical pattern.


![img.png](images/0001-intro-1.png)

Each record contains a full description of the architectural decision:

- Title.
- Date when it occurred.
- Problem description.
- Decision.
- Consequences of the decision.

![img.png](images/0001-intro-2.png)

### Notes:
_Idea based on the following article: https://newsletter.fractionalarchitect.io/p/12-document-it-architecture-decision_
