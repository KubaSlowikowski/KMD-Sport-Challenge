// Centralized error handling middleware.

export function errorHandlingMiddleware(err, req, res, next): void {
  console.error(err.stack); // Log error stack trace
  res.status(500).json({ message: 'An unexpected error occurred.' });
}
