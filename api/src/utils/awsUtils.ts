export const sendJsonResponse = (statusCode: number) => {
  return {
    statusCode: statusCode,
    headers: {
      'Content-Type': 'application/json',
    },
  };
};

export const sendJsonData = (data: any) => {
  return {
    statusCode: 200,
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data),
  };
};

export const sendJsonError = (errorMessage: string, statusCode: number) => {
  return {
    statusCode: statusCode,
    headers: {
      'Content-Type': 'application/json',
    },
    body: errorMessage,
  };
};

// export const APPLICATION_NAME_PREFIX = 'sports-appka-api-dev'
export const APPLICATION_NAME_PREFIX = 'sports-app-test-dev';
