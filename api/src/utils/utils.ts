import { CHALLENGE_END_DATE, CHALLENGE_START_DATE } from '../config/challengeConfig';
import process from 'process';

export function dateTimeStringToTimestamp(dateTimeString: string): number {
  if (!dateTimeString) {
    console.error("Error in 'dateTimeStringToTimestamp()': 'dateTimeString' param is undefined or null.");
    return null;
  }
  const date = dateTimeString ? new Date(dateTimeString) : new Date(); // Create a Date object from the string
  const timestamp = date.getTime(); // Get the Unix timestamp (milliseconds since January 1, 1970)
  const timestampInSeconds = Math.floor(timestamp / 1000); // Convert to seconds if needed

  return timestampInSeconds;
}

export function getChallengeStartTimestamp(): number {
  const [day, month, year] = CHALLENGE_START_DATE.split('/');
  const date = new Date(+year, +month - 1, +day);
  const timestamp = date.getTime();

  return Math.floor(timestamp / 1000);
}

export function getChallengeEndTimestamp(): number {
  const [day, month, year] = CHALLENGE_END_DATE.split('/');
  const date = new Date(+year, +month - 1, +day);
  const timestamp = date.getTime();

  return Math.floor(timestamp / 1000);
}

export function getCurrentTimestamp(): number {
  return new Date().getTime() / 1000;
}

export enum ENDPOINTS {
  STRAVA_TOKEN = 'https://www.strava.com/oauth/token',
  STRAVA_OAUTH_AUTHORIZE = 'https://www.strava.com/oauth/authorize',
  STRAVA_ATHLETE_ACTIVITIES = 'https://www.strava.com/api/v3/athlete/activities',
  STRAVA_ACTIVITY = 'https://www.strava.com/api/v3/activities',
  STRAVA_WEBHOOK_PUSH_SUBSCRIPTION = 'https://www.strava.com/api/v3/push_subscriptions',
  CHAT_GPT_SEND_REQUEST_URL = 'https://api.openai.com/v1/chat/completions',
  STRAVA_GET_LOGGEDIN_ATHLETE = 'https://www.strava.com/api/v3/athlete',
}

export enum GRANT_TYPES {
  REFRESH_TOKEN = 'refresh_token',
  AUTHORIZATION_CODE = 'authorization_code',
}

// Scopes for requesting Strava access
export enum SCOPES {
  READ = 'read', // read public segments, public routes, public profile data, public posts, public events, club feeds, and leaderboards
  ACTIVITY_READ = 'activity:read', // read the user's activity data for activities that are visible to Everyone and Followers, excluding privacy zone data
  ACTIVITY_READ_ALL = 'activity:read_all', // read the user's activity data for activities that are visible to Everyone and Followers, excluding privacy zone data
}

export const API_PROD_URL = 'https://cvvp7qnqcj.execute-api.us-east-1.amazonaws.com'; // TODO move to new file urlUtils.ts
export const API_TEST_URL = 'https://iy59c8hgl6.execute-api.eu-north-1.amazonaws.com';
export const FRONTEND_PROD_URL = 'http://sports-appka.s3-website-us-east-1.amazonaws.com';
export const FRONTEND_TEST_URL = 'http://sports-appka-test.s3-website.eu-north-1.amazonaws.com';
export const FRONTEND_LOCAL_URL = 'http://localhost:4200';
export const SPORTS_APP_URL = 'https://sports-appka.s3.amazonaws.com';
export const CUSTOM_DOMAIN_URL = 'https://sports-app.net';

export const REDIRECT_URI_WHITELIST = [
  '/',
  FRONTEND_LOCAL_URL,
  API_PROD_URL,
  API_TEST_URL,
  FRONTEND_PROD_URL,
  FRONTEND_TEST_URL,
  SPORTS_APP_URL,
  CUSTOM_DOMAIN_URL,
];

export const CORS_WHITELIST = [FRONTEND_LOCAL_URL, FRONTEND_PROD_URL, FRONTEND_TEST_URL, CUSTOM_DOMAIN_URL];

/**
 * Determines whether the application is working as a test environment or not.
 * If set to 'true' - application uses test environment, 'false' - application uses the production environment.
 * TODO 21.05.2024 - currently application uses the same AWS environment. Only the data sources are distinguished.
 */
export const TEST_MODE_ENABLED: boolean = !process.env.PRODUCTION || process.env.PRODUCTION !== 'true';
