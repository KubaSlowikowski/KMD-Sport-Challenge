import * as process from 'process';
import { TEST_MODE_ENABLED } from '../utils/utils';

const CHALLENGE_NAME = 'challenge-test2';
const ATHLETES_COLLECTION_NAME = `athletes-${CHALLENGE_NAME}`;
const STRAVA_WEBHOOK_EVENTS_COLLECTION_NAME = `strava-webhook-events`;
const ACTIVITY_OVERRIDES_COLLECTION_NAME = `activity-overrides-${CHALLENGE_NAME}`;
const PREFEED_COLLECTION_NAME = `prefeed-${CHALLENGE_NAME}`;
const FEED_COLLECTION_NAME = `feed-${CHALLENGE_NAME}`;
const USER_SETTINGS_COLLECTION_NAME = `user-settings-${CHALLENGE_NAME}`;

class MongoDBConfig {
  uri: string;
  databaseName: string;
  athletesCollectionName: string;
  stravaWebhookEventsCollectionName: string;
  activityOverridesCollectionName: string;
  prefeedCollectionName: string;
  feedCollectionName: string;
  userSettingsCollectionName: string;

  constructor() {
    this.uri = `mongodb+srv://${process.env.MONGO_DB_USERNAME}:${process.env.MONGO_DB_PASSWORD}@${process.env.MONGO_DB_USERNAME}.6gxfke7.mongodb.net/?retryWrites=true&w=majority&appName=${process.env.MONGO_DB_APPNAME}`;

    if (TEST_MODE_ENABLED) {
      this.databaseName = 'test';
    } else {
      this.databaseName = process.env.MONGO_DB_NAME;
    }

    this.athletesCollectionName = ATHLETES_COLLECTION_NAME; // TODO fetch challenge number from DB.
    this.stravaWebhookEventsCollectionName = STRAVA_WEBHOOK_EVENTS_COLLECTION_NAME;
    this.activityOverridesCollectionName = ACTIVITY_OVERRIDES_COLLECTION_NAME;
    this.prefeedCollectionName = PREFEED_COLLECTION_NAME;
    this.feedCollectionName = FEED_COLLECTION_NAME;
    this.userSettingsCollectionName = USER_SETTINGS_COLLECTION_NAME;
  }
}

export const databaseConfig = new MongoDBConfig();
