import { connectToDatabase } from './mongoDBAdapter';
import { Db } from 'mongodb';
import { databaseConfig } from './MongoDBConfig';

async function init() {
  const db: Db = await connectToDatabase();

  await db.createCollection(databaseConfig.athletesCollectionName);
  await db.createCollection(databaseConfig.stravaWebhookEventsCollectionName);
  await db.createCollection(databaseConfig.activityOverridesCollectionName);
  await db.createCollection(databaseConfig.prefeedCollectionName);
  await db.createCollection(databaseConfig.feedCollectionName);
  await db.createCollection(databaseConfig.userSettingsCollectionName);

  db[databaseConfig.athletesCollectionName].createIndex({
    'activities.activityId': 1,
  });
  db[databaseConfig.athletesCollectionName].createIndex({ athleteId: 1 });
  db[databaseConfig.stravaWebhookEventsCollectionName].createIndex({
    createdAt: -1,
  });
  db[databaseConfig.activityOverridesCollectionName].createIndex({
    athleteId: 1,
    activityId: 1,
  });
  db[databaseConfig.userSettingsCollectionName].createIndex({ athleteId: 1 });
}
