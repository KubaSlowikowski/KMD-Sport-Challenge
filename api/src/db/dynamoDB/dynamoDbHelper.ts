import { PutCommand, PutCommandOutput, ScanCommand, ScanCommandInput, ScanCommandOutput } from '@aws-sdk/lib-dynamodb';
import { DynamoDBClient } from '@aws-sdk/client-dynamodb';
import { dbClient, TableNames } from './dynamoDb';
import { isCommandOutput2xxSuccessful } from '../../repository/utils';

export class DynamoDbHelper {
  private readonly dbClient: DynamoDBClient;

  constructor(dbClient: DynamoDBClient) {
    this.dbClient = dbClient;
  }

  async getAllItems<T>(params: ScanCommandInput): Promise<T[]> {
    let items: any[] = [];

    // DynamoDB scan operation has a 1MB data response limit.
    // If our table has more than 1MB, the result items will be limited.
    // To avoid it, we must use do-while loop.
    do {
      try {
        const data: ScanCommandOutput = await this.dbClient.send(new ScanCommand(params));
        items = items.concat(data.Items);
        params.ExclusiveStartKey = data.LastEvaluatedKey;
      } catch (err) {
        console.error(`Unable to scan the '${params.TableName}' table. Error JSON:`, JSON.stringify(err, null, 2));
        break;
      }
    } while (params.ExclusiveStartKey);

    return items;
  }

  async save<T>(itemToSave: T, tableName: TableNames): Promise<boolean> {
    const command = new PutCommand({
      TableName: tableName,
      Item: itemToSave,
    });

    const response: PutCommandOutput = await this.dbClient.send(command);
    const success: boolean = isCommandOutput2xxSuccessful(response);

    if (!success) {
      console.error(`Unable to save an item in '${tableName}' table.`);
      console.log(response);
    }

    return success;
  }
}

export const dynamoDbHelper: DynamoDbHelper = new DynamoDbHelper(dbClient);
