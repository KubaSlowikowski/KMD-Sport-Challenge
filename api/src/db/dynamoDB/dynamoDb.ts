import { DynamoDBClient } from '@aws-sdk/client-dynamodb';
import process from 'process';
import { DynamoDBDocumentClient } from '@aws-sdk/lib-dynamodb';

export enum TableNames {
  Athlete = 'Athlete',
  Activity = 'Activity',
  ActivityOverride = 'ActivityOverride',
  StravaWebhookEvent = 'StravaWebhookEvent',
  Prefeed = 'Prefeed',
  Feed = 'Feed',
}

export enum IndexNames {
  GSI_Activity_AthleteId = `gsi1_${TableNames.Activity}`,
  GSI_StravaWebhookEvent = `gsi1_${TableNames.StravaWebhookEvent}`,
}

export const dbClient: DynamoDBDocumentClient = DynamoDBDocumentClient.from(
  new DynamoDBClient({ region: process.env.AWS_REGION }),
);
