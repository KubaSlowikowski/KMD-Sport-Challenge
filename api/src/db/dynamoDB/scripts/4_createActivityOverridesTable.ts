import { CreateTableCommand, CreateTableCommandOutput } from '@aws-sdk/client-dynamodb';
import { dbClient, TableNames } from '../dynamoDb';

export const createActivityOverridesTable = async () => {
  const command = new CreateTableCommand({
    TableName: TableNames.ActivityOverride,
    AttributeDefinitions: [{ AttributeName: 'activityId', AttributeType: 'N' }],
    KeySchema: [{ AttributeName: 'activityId', KeyType: 'HASH' }],
    BillingMode: 'PAY_PER_REQUEST',
  });

  const response: CreateTableCommandOutput = await dbClient.send(command);
  console.log(response);

  return response;
};
