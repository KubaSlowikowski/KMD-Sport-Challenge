import { CreateTableCommand, CreateTableCommandOutput } from '@aws-sdk/client-dynamodb';
import { dbClient, TableNames } from '../dynamoDb';

export const createAthletesTable = async () => {
  const command = new CreateTableCommand({
    TableName: TableNames.Athlete,
    AttributeDefinitions: [{ AttributeName: 'athleteId', AttributeType: 'N' }],
    KeySchema: [{ AttributeName: 'athleteId', KeyType: 'HASH' }],
    BillingMode: 'PAY_PER_REQUEST',
  });

  const response: CreateTableCommandOutput = await dbClient.send(command);
  console.log(response);

  return response;
};
