import { CreateTableCommand, CreateTableCommandOutput } from '@aws-sdk/client-dynamodb';
import { dbClient, IndexNames, TableNames } from '../dynamoDb';

export const createActivitiesTable = async () => {
  const command = new CreateTableCommand({
    TableName: TableNames.Activity,
    AttributeDefinitions: [
      { AttributeName: 'id', AttributeType: 'N' },
      { AttributeName: 'athleteId', AttributeType: 'N' },
    ],
    KeySchema: [{ AttributeName: 'id', KeyType: 'HASH' }],
    GlobalSecondaryIndexes: [
      {
        IndexName: IndexNames.GSI_Activity_AthleteId,
        KeySchema: [{ AttributeName: 'athleteId', KeyType: 'HASH' }],
        Projection: {
          ProjectionType: 'ALL',
        },
      },
    ],
    BillingMode: 'PAY_PER_REQUEST',
  });

  const response: CreateTableCommandOutput = await dbClient.send(command);
  console.log(response);

  return response;
};
