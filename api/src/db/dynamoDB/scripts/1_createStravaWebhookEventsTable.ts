import { CreateTableCommand, CreateTableCommandOutput } from '@aws-sdk/client-dynamodb';
import { dbClient, IndexNames, TableNames } from '../dynamoDb';
import process from 'process';
import {
  CreateEventSourceMappingCommand,
  CreateEventSourceMappingCommandOutput,
  LambdaClient,
} from '@aws-sdk/client-lambda';
import { dynamoDbEventsProcessorFunctionName } from '../../../jobs/dynamoDbTriggerEventsProcessor';
import { APPLICATION_NAME_PREFIX } from '../../../utils/awsUtils';

export const createStravaWebhookEventsTable = async () => {
  const command = new CreateTableCommand({
    TableName: TableNames.StravaWebhookEvent,
    AttributeDefinitions: [
      { AttributeName: 'id', AttributeType: 'S' },
      { AttributeName: 'status', AttributeType: 'S' },
    ],
    KeySchema: [{ AttributeName: 'id', KeyType: 'HASH' }],
    GlobalSecondaryIndexes: [
      {
        IndexName: IndexNames.GSI_StravaWebhookEvent,
        KeySchema: [{ AttributeName: 'status', KeyType: 'HASH' }],
        Projection: {
          ProjectionType: 'ALL',
        },
      },
    ],
    BillingMode: 'PAY_PER_REQUEST',
    StreamSpecification: {
      StreamEnabled: true,
      StreamViewType: 'NEW_IMAGE',
    },
  });

  const response: CreateTableCommandOutput = await dbClient.send(command);
  console.log(response);

  const streamArn: string = response.TableDescription.LatestStreamArn;
  if (streamArn) {
    await createTrigger(streamArn);
  }

  return response;
};

async function createTrigger(eventStreamArn: string) {
  const lambdaClient = new LambdaClient({ region: process.env.AWS_REGION });

  const command = new CreateEventSourceMappingCommand({
    EventSourceArn: eventStreamArn,
    FunctionName: `${APPLICATION_NAME_PREFIX}-${dynamoDbEventsProcessorFunctionName}`,
    StartingPosition: 'LATEST',
    Enabled: true,
    BatchSize: 30,
  });

  const response: CreateEventSourceMappingCommandOutput = await lambdaClient.send(command);
  return response;
}
