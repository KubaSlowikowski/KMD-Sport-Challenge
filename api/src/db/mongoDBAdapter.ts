import { Collection, Db, MongoClient, ServerApiVersion } from 'mongodb';
import { databaseConfig } from './MongoDBConfig';
import { UserSetting } from '../models/domain/UserSetting';

// Create a MongoClient with a MongoClientOptions object to set the Stable API version
const client: MongoClient = new MongoClient(databaseConfig.uri, {
  serverApi: {
    version: ServerApiVersion.v1,
    strict: true,
    deprecationErrors: true,
  },
});

export async function connectToDatabase(): Promise<Db> {
  try {
    const database = client.db(databaseConfig.databaseName);
    return database;
  } catch (error) {
    console.error('Unable to connect to the MongoDB database. Closing a client. Error:', error);
    await client.close();
    throw error;
  }
}

export async function getUserSettingsCollection(): Promise<Collection<UserSetting>> {
  const database = await connectToDatabase();
  return database.collection<UserSetting>(databaseConfig.userSettingsCollectionName);
}

export async function closeMongoDBClient(): Promise<void> {
  await client.close();
}
