import { FeedModel } from '../../feed/models';
import { ManualFeedAddDto } from '../../manual-feed';
import crypto from 'crypto';
import { feedService } from '../../feed/database/feedService';

export async function addManualFeed(addModel: ManualFeedAddDto): Promise<FeedModel> {
  const feedModel = buildFeedModel(addModel);
  await feedService.save(feedModel);
  return feedModel;
}

function buildFeedModel(addModel: ManualFeedAddDto): FeedModel {
  return {
    id: crypto.randomUUID().toString(),
    text: addModel.text,
    referencedAthletes: [],
    createdDate: new Date().toISOString(),
    metaData: {
      addModel,
    },
  };
}
