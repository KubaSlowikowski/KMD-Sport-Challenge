import { PrefeedSourceModel } from '../../feed/models/prefeed-source.model';
import { AiMetadataModel, AiStyleType, ChatGptResponse } from '../../chat-gpt/models';
import { buildActivityAddedAiPrompt } from '../../activity-added-prefeed/logic/build-activity-added-ai-prompt.function';
import { ChatGptService } from '../../chat-gpt/logic';

/*
    Invokes 
*/
export async function invokeAiFlow(prefeedModel: PrefeedSourceModel): Promise<{
  metadata: AiMetadataModel;
  aiResponse: ChatGptResponse;
  aiText: string;
}> {
  // TODO add types
  const metadata = buildAiPromptFunction(prefeedModel);
  const chatGptService = new ChatGptService();
  const response = (await chatGptService.sendRequest(metadata.prompt)).data;
  return {
    metadata,
    aiResponse: response,
    aiText: response.choices[0].message.content,
  };
}

function buildAiPromptFunction(prefeedModel: PrefeedSourceModel): AiMetadataModel {
  const style = getRandomAiStyle();
  const prompt = buildAiPrompt(prefeedModel, style);
  return {
    style,
    prompt: prompt,
  };
}

function getRandomAiStyle(): AiStyleType {
  const AI_STYLES = ['Funny', 'Motivation'];
  return AI_STYLES[Math.floor(Math.random() * AI_STYLES.length)] as AiStyleType;
}

function buildAiPrompt(prefeedModel: PrefeedSourceModel, style: AiStyleType) {
  if (prefeedModel.type === 'ActivityAdded') {
    return buildActivityAddedAiPrompt(prefeedModel, style);
  } else {
    throw new Error(`${prefeedModel.type} not supported`);
  }
}
