import { invokeActivityAddedFlowFunction } from '../../activity-added-prefeed/logic/invoke-activity-added-flow.function';
import { invokeAiFlow } from './invoke-ai-flow.function';
import { AiMetadataModel, ChatGptResponse } from '../../chat-gpt/models';
import { FeedModel } from '../../feed/models';
import {
  ActivityAddedPrefeedConfigModel,
  ActivityAddedPrefeedModel,
  PrefeedHighlightModel,
  PrefeedHighlightType,
} from '../../activity-added-prefeed/models';
import { ReferenceAthleteModel } from '../../prefeed/models';
import crypto from 'crypto';
import { CHALLENGE_START_DATE } from '../../../config/challengeConfig';
import { feedService } from '../../feed/database/feedService';

/**
 Invokes the entire flow of adding new feed, after activity has been added.
 This functions, checks if there is something special in activity, translate feed text using AI to nicer one, and adds feed to db.
 @param activityId - activityId of recently added activity, which might be included in flow
 @param athleteId - athleteId to who activity belongs
 @param config - additional configuration which changes flow execution
 @returns - feed model, or there is nothing special about activity
 **/
export async function invokeActivityAddedFeedFlow(
  activityId: number,
  athleteId: number,
  config?: ActivityAddedPrefeedConfigModel,
): Promise<FeedModel> {
  console.log(`Started invokeActivityAddedFeedFlow for activityId='${activityId}', athleteId='${athleteId}'`);
  console.log(
    'invokeActivityAddedFeedFlow - ' + (config?.simulate ?? false) ? 'Run in simulation mode' : 'Run in real mode',
  );
  console.log(
    'invokeActivityAddedFeedFlow - ' + (config?.keepHistory ?? false)
      ? 'Run in keepHistory mode'
      : 'Run without keepHistory mode',
  );
  const activityAddedPrefeed = await invokeActivityAddedFlowFunction(activityId, athleteId, config);
  if (!isActivitySupported(activityAddedPrefeed)) {
    console.log('Activity is not supported.');
    return null;
  }

  if (!isSpecial(activityAddedPrefeed)) {
    console.log('Nothing special...');
    return null;
  }

  if (!shouldBeenCreated(activityAddedPrefeed)) {
    console.log(`Feed shouldn't be created. The challenge has just started.`);
    return null;
  }

  const { metadata, aiResponse, aiText } = await invokeAiFlow(activityAddedPrefeed);
  const feedModel = buildFeedModel(aiText, aiResponse, activityAddedPrefeed, metadata, config);

  if (!(config?.simulate ?? false)) {
    await feedService.save(feedModel);
    console.log(`Feed flow finished. Created feedModel with id='${feedModel.id}'.`);
  }

  return feedModel;
}

function isActivitySupported(activityAddedPrefeed: ActivityAddedPrefeedModel) {
  return activityAddedPrefeed != null;
}

function isSpecial(activityAddedPrefeed: ActivityAddedPrefeedModel) {
  return activityAddedPrefeed.highlights.length > 0;
}

function buildFeedModel(
  aiText: string,
  aiResponse: ChatGptResponse,
  prefeed: ActivityAddedPrefeedModel,
  aiMetadata: AiMetadataModel,
  config?: ActivityAddedPrefeedConfigModel,
): FeedModel {
  const keepHistory = config?.keepHistory ?? false;
  return {
    id: crypto.randomUUID().toString(),
    text: aiText,
    referencedAthletes: getReferencedAthletes(prefeed),
    createdDate: keepHistory ? prefeed.activity.endDate : new Date().toISOString(),
    activityId: prefeed.activity.activityId,
    athlete: prefeed.athlete,
    athleteTokens: prefeed.athleteTokens,
    highlights: prefeed.highlights.map(h => h.highlight),

    metaData: {
      aiMetadata,
      aiResponse,
    },
  };
}

function getReferencedAthletes(activityAddedPrefeed: ActivityAddedPrefeedModel): ReferenceAthleteModel[] {
  return activityAddedPrefeed.highlights
    .flatMap(h => getReferencedAthletesFromHighlight(h))
    .reduce((p, c) => (!p.some(x => x.athleteId === c.athleteId) ? [...p, c] : p), [] as ReferenceAthleteModel[]);
}

function getReferencedAthletesFromHighlight(highlight: PrefeedHighlightModel): ReferenceAthleteModel[] {
  switch (highlight.highlight) {
    case 'GlobalRecordPoints':
      return [highlight.previousAthlete];
    case 'RankingAdvancement':
      return highlight.athletesBeaten;
    default:
      return [];
  }
}

function shouldBeenCreated(activityAddedPrefeed: ActivityAddedPrefeedModel): boolean {
  const MIN_PUBLISH_DAYS = 4;
  const dateParts = CHALLENGE_START_DATE.split('/');
  const challengeStartDate = new Date(+dateParts[2], +dateParts[1] - 1, +dateParts[0]);
  const NUMBER_OF_DAYS_AFTER_CHALLENGE = (new Date().getTime() - challengeStartDate.getTime()) / (24 * 60 * 60 * 1000);
  return (
    !activityAddedPrefeed.highlights.every(h => isHighlightOfTypeWhichShouldNotBeShownToEarly(h)) ||
    NUMBER_OF_DAYS_AFTER_CHALLENGE > MIN_PUBLISH_DAYS
  );

  function isHighlightOfTypeWhichShouldNotBeShownToEarly(h: PrefeedHighlightModel): unknown {
    return (['RankingAdvancement', 'PersonalRecordPoints'] as PrefeedHighlightType[]).includes(h.highlight);
  }
}
