import { BasePrefeedModel } from '../models';
import { DynamoDBClient } from '@aws-sdk/client-dynamodb';
import { TableNames } from '../../../db/dynamoDB/dynamoDb';
import { PutCommand, PutCommandOutput } from '@aws-sdk/lib-dynamodb';
import { isCommandOutput2xxSuccessful } from '../../../repository/utils';

export abstract class PrefeedRepository<TPrefeed extends BasePrefeedModel> {
  private readonly dbClient: DynamoDBClient;

  constructor(dbClient: DynamoDBClient) {
    this.dbClient = dbClient;
  }

  async save(prefeedModel: TPrefeed): Promise<boolean> {
    const parsedModel = this.parse(prefeedModel);
    const command = new PutCommand({
      TableName: TableNames.Prefeed,
      Item: parsedModel,
    });

    const response: PutCommandOutput = await this.dbClient.send(command);
    const success: boolean = isCommandOutput2xxSuccessful(response);

    if (!success) {
      console.log(response);
    }
    return success;
  }

  protected abstract parse(prefeed: TPrefeed): unknown;
}
