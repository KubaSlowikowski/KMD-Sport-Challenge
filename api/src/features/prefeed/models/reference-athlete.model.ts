export interface ReferenceAthleteModel {
  athleteId: number;
  athleteName: string;
  sex: 'M' | 'F';
}
