import { PrefeedHighlightType } from 'src/features/activity-added-prefeed/models';
import { AthleteTokenModel } from '../../activity-added-prefeed/models/athlete-token.model';
import { ReferenceAthleteModel } from '../../prefeed/models';

export interface FeedModel {
  id: string;
  text: string;
  referencedAthletes: ReferenceAthleteModel[];
  createdDate: string;

  highlights?: PrefeedHighlightType[];
  athlete?: ReferenceAthleteModel;
  activityId?: number;
  athleteTokens?: AthleteTokenModel[];
  metaData: unknown;
}
