import { ActivityAddedPrefeedModel } from '../../activity-added-prefeed/models';

export type PrefeedSourceModel = ActivityAddedPrefeedModel;
