import { FeedModel } from '../models';
import { DynamoDBClient } from '@aws-sdk/client-dynamodb';
import { dynamoDbHelper, DynamoDbHelper } from '../../../db/dynamoDB/dynamoDbHelper';
import { dbClient, TableNames } from '../../../db/dynamoDB/dynamoDb';
import { ScanCommand, ScanCommandOutput } from '@aws-sdk/lib-dynamodb';
import { isCommandOutput2xxSuccessful } from '../../../repository/utils';

export class FeedRepository {
  private readonly dbClient: DynamoDBClient;
  private readonly dynamoDbHelper: DynamoDbHelper;

  constructor(dbClient: DynamoDBClient, dynamoDbHelper: DynamoDbHelper) {
    this.dbClient = dbClient;
    this.dynamoDbHelper = dynamoDbHelper;
  }

  async save(feedModel: FeedModel): Promise<boolean> {
    const success: boolean = await this.dynamoDbHelper.save(feedModel, TableNames.Feed);
    if (success) {
      console.log(`Successfully saved a feed with id='${feedModel.id}'.`);
    } else {
      console.log(`Failed to save a feed with id='${feedModel.id}'.`);
    }

    return success;
  }

  async getPage(pageNumber: number, pageSize: number): Promise<FeedModel[]> {
    const command = new ScanCommand({
      TableName: TableNames.Feed,
    });

    /**
     * FIXME
     * Now we're using Scan command to fetch THE WHOLE table items, then sort them by 'createdDate' descending and return particular page.
     * It is very inefficient and for now I have 2 possible enhancements:
     * - Create CQRS (read-only) table with pre-sorted feeds and read them directly,
     * - Redesign our table to be able to use Query commands and fetch data by partition keys.
     *
     * Also, we can enable TTL (time to live) to decrease the number of feeds.
     */
    const response: ScanCommandOutput = await this.dbClient.send(command);
    if (!isCommandOutput2xxSuccessful(response)) {
      console.error(`Failed to fetch all feeds from DynamoDB.`);
      return;
    }

    const items: FeedModel[] = response.Items as FeedModel[];
    const sortedFeeds: FeedModel[] = items.sort((a, b) => {
      if (a.createdDate > b.createdDate) {
        return -1;
      } else if (a.createdDate < b.createdDate) {
        return 1;
      } else {
        return 0;
      }
    });

    const startIndex = pageNumber > 0 ? pageNumber * pageSize : 0;
    const page: FeedModel[] = sortedFeeds.slice(startIndex, startIndex + pageSize);

    return page;
  }
}

export const feedRepository: FeedRepository = new FeedRepository(dbClient, dynamoDbHelper);
