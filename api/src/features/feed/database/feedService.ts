import { feedRepository, FeedRepository } from './feedRepository';
import { FeedModel } from '../models';

class FeedService {
  private readonly PAGE_SIZE: number = 15;
  private readonly repository: FeedRepository;

  constructor(repository: FeedRepository) {
    this.repository = repository;
  }

  async save(feedModel: FeedModel): Promise<boolean> {
    if (!feedModel) throw new Error(`save(): given 'feedModel' is null or undefined.`);

    return this.repository.save(feedModel);
  }

  async getPage(pageNumber: number, pageSize?: number): Promise<FeedModel[]> {
    return this.repository.getPage(pageNumber, pageSize ?? this.PAGE_SIZE);
  }
}

export const feedService = new FeedService(feedRepository);
