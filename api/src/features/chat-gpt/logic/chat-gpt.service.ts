import { AxiosResponse } from 'axios';
import { ENDPOINTS } from '../../../utils/utils';
import { AiPromptModel, ChatGptResponse } from '../models';

const axios = require('axios');

export class ChatGptService {
  public async sendRequest(request: AiPromptModel): Promise<AxiosResponse<ChatGptResponse>> {
    const body = this.buildRequest(request);
    return await axios.post(`${ENDPOINTS.CHAT_GPT_SEND_REQUEST_URL}`, body, {
      headers: { Authorization: `Bearer ${process.env.CHAT_GPT_TOKEN}` },
    });
  }

  private buildRequest(request: AiPromptModel) {
    return {
      model: 'gpt-3.5-turbo',
      messages: [
        {
          role: 'system',
          content: request.system,
        },
        {
          role: 'user',
          content: request.user,
        },
      ],
    };
  }
}
