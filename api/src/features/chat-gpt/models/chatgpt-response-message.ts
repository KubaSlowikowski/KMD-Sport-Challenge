export interface ChatGptResponseMessage {
  role: string;
  content: string;
}
