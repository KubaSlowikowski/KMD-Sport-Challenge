import { ChatGptResponseMessage } from './chatgpt-response-message';

export interface ChatGptResponseChoice {
  index: number;
  message: ChatGptResponseMessage;
  logprobs: any;
  finish_reason: string;
}
