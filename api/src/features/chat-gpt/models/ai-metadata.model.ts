import { AiPromptModel } from './ai-prompt.model';
import { AiStyleType } from './ai-style.type';

export interface AiMetadataModel {
  style: AiStyleType;
  prompt: AiPromptModel;
}
