import { ChatGptResponseChoice } from './chatgpt-response-choice';
import { ChatGptResponseUsage } from './chatgpt-response-usage';

export interface ChatGptResponse {
  id: string;
  object: string;
  created: number;
  model: string;
  choices: ChatGptResponseChoice[];
  usage: ChatGptResponseUsage;
  system_fingerprint: any;
}
