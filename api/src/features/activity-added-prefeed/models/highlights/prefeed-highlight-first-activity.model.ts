import { PrefeedHighlightBaseModel } from './prefeed-highlight-base.model';

export interface PrefeedHighlightFirstActivityModel extends PrefeedHighlightBaseModel {
  highlight: 'FirstActivity';
}
