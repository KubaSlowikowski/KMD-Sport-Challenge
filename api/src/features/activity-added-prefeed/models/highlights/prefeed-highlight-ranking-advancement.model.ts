import { PrefeedHighlightBaseModel } from './prefeed-highlight-base.model';
import { ReferenceAthleteModel } from '../../../prefeed/models';

export interface PrefeedHighlightRankingAdvancementModel extends PrefeedHighlightBaseModel {
  highlight: 'RankingAdvancement';
  fromPosition: number;
  toPosition: number;
  /** Athletes who after given activity, were higher in ranking */
  athletesBeaten: ReferenceAthleteModel[];
}
