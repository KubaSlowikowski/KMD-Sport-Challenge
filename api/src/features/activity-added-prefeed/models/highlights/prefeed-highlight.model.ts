import { PrefeedHighlightFirstActivityModel } from './prefeed-highlight-first-activity.model';
import { PrefeedHighlightGlobalRecordPointsModel } from './prefeed-highlight-global-record-points.model';
import { PrefeedHighlightPersonalRecordPointsModel } from './prefeed-highlight-personal-record-points.model';
import { PrefeedHighlightRankingAdvancementModel } from './prefeed-highlight-ranking-advancement.model';

export type PrefeedHighlightModel =
  | PrefeedHighlightFirstActivityModel
  | PrefeedHighlightPersonalRecordPointsModel
  | PrefeedHighlightGlobalRecordPointsModel
  | PrefeedHighlightRankingAdvancementModel;
