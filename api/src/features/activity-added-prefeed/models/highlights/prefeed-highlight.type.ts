export type PrefeedHighlightType =
  | 'GlobalRecordPoints'
  | 'PersonalRecordPoints'
  | 'RankingAdvancement'
  | 'FirstActivity';
