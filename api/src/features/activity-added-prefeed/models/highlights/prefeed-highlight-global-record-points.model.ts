import { ReferenceAthleteModel } from 'src/features/prefeed/models';
import { PrefeedHighlightBaseModel } from './prefeed-highlight-base.model';

export interface PrefeedHighlightGlobalRecordPointsModel extends PrefeedHighlightBaseModel {
  highlight: 'GlobalRecordPoints';
  previousRecordInPoints: number;
  /** Previous athlete who owned global record for points */
  previousAthlete: ReferenceAthleteModel;
}
