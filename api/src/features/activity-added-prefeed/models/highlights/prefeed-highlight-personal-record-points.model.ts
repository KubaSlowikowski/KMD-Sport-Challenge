import { PrefeedHighlightBaseModel } from './prefeed-highlight-base.model';

export interface PrefeedHighlightPersonalRecordPointsModel extends PrefeedHighlightBaseModel {
  highlight: 'PersonalRecordPoints';
  previousRecordInPoints: number;
}
