import { PrefeedHighlightType } from './prefeed-highlight.type';

export interface PrefeedHighlightBaseModel {
  highlight: PrefeedHighlightType;
}
