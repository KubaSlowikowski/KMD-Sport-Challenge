export interface AthleteTokenModel {
  id: number;
  tokenText: string;
  athleteId: number;
  athleteName: string;
}
