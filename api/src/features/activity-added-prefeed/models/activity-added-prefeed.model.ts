import { PrefeedRequestActivityModel } from './prefeed-request-activity.model';
import { PrefeedHighlightModel } from './highlights';
import { ReferenceAthleteModel } from '../../prefeed/models';
import { AthleteTokenModel } from './athlete-token.model';

/**
 * Contains highlight and additional metadata for activity. This is prefeed model, so
 * potential candidate to create feed model from it.
 */
export interface ActivityAddedPrefeedModel {
  /** Prefeed unique id */
  id: string;
  /** Type of prefeed */
  type: 'ActivityAdded';
  /** activity owner data */
  athlete: ReferenceAthleteModel;
  /** highlights of activity. It contains special properties of activity in challange context, which can be presented in feed to athletes. */
  highlights: PrefeedHighlightModel[];
  /** activity data */
  activity: PrefeedRequestActivityModel;
  /** prefeed created date */
  created: string;

  athleteTokens: AthleteTokenModel[];
}
