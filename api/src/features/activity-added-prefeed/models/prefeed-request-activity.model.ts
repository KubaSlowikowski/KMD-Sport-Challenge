import { ActivityType } from '../../../strava/types/activity.type';

/** Activity data which will be used in prefeed request flow. */
export interface PrefeedRequestActivityModel {
  activityId: number;
  sportType: ActivityType;
  supported: boolean;
  points: number;
  createdDate: string;
  endDate: string;
}
