import { Activity } from '../../../models/domain/Activity';
import { Athlete } from '../../../models/domain/Athlete';

/**
 * This object is used to configure, and change flow execution of adding new prefeed for activity.
 */
export interface ActivityAddedPrefeedConfigModel {
  /** When true while processing activity, it will not take into account activities which ends after
   * current activity. It is used to checks activities at the moment when they were created, not in current context.
   */
  keepHistory?: boolean;
  /**
   * When processing multiple athletes, this property can be used, to prevent flow from getting all
   * athletes from db for every activity.
   */
  cachedAthletes?: Athlete[];
  /**
   * When processing multiple activities, this property can be used, to prevent flow from getting all
   * activities from db for every activity.
   */
  cachedActivities?: Activity[];
  /**
   * When true, will not add anything to database, but only return added feed.
   */
  simulate?: boolean;
}
