import { ActivityAddedPrefeedModel } from '.';
import { PrefeedBuildContextModel } from './result-build-context.model';

/**
 * Function type which is used to determine hightlights in given activity.
 * If no highlight is found fn should return result given in args.
 * If any highlight is found, fn should return clonned result, with added hightlight info.
 * It needs to make sure to copy current state of result to returned value.
 * @param result - current state of determining hightlights, which will be returned as final result
 * after execution of all functions.
 * @param context - current challenge data, agreggated to variables, which can be used to determine hightlights.
 */
export type PrefeedBuildFn = (
  result: ActivityAddedPrefeedModel,
  context: PrefeedBuildContextModel,
) => ActivityAddedPrefeedModel;
