import { PrefeedRequestActivityModel, PrefeedRequestAthleteModel } from '.';

/** Agreggated data, which will be used by prefeedBuildFn to activity highlights.
 * Data is prepared in special way to simplify process of finding highlights.
 */
export interface PrefeedBuildContextModel {
  /** Activity owner data */
  currentAthlete: PrefeedRequestAthleteModel;
  /** Current activity data */
  currentActivity: PrefeedRequestActivityModel;
  /** Other athletes data */
  otherAthletes: PrefeedRequestAthleteModel[];
  /** Activity owner's activities. Does not contain current activity. */
  currentAthleteOtherActivitiesSorted: PrefeedRequestActivityModel[];
  /** Other's athletes all activities */
  otherAthleteActivitiesSorted: PrefeedRequestActivityModel[];
}
