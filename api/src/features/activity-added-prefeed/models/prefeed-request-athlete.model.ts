import { PrefeedRequestActivityModel } from './prefeed-request-activity.model';

/** Athletes data which will be used in prefeed request flow. */
export interface PrefeedRequestAthleteModel {
  athleteId: number;
  athleteName: string;
  sex: 'M' | 'F';
  activities: PrefeedRequestActivityModel[];
}
