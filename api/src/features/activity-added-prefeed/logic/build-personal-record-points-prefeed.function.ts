import { PrefeedBuildContextModel } from '../models';
import { ActivityAddedPrefeedModel } from '../models';

/**
 * Finds if activity beat current athlete's points for one activity record.
 * @param result current result of add activity prefeed flow
 * @param context data from challenge aggregated into helpful fields.
 * @returns result with additional highlight if given activity beat current athlete's record
 */
export function buildPersonalRecordPointsPrefeed(
  result: ActivityAddedPrefeedModel,
  context: PrefeedBuildContextModel,
): ActivityAddedPrefeedModel {
  if (context.currentAthleteOtherActivitiesSorted.length === 0) {
    return result;
  }

  const currentPersonalRecordActivity = context.currentAthleteOtherActivitiesSorted.reduce(
    (p, c) => (p == null ? c : c.points > p.points ? c : p),
    null,
  );
  if (currentPersonalRecordActivity.points >= context.currentActivity.points) {
    return result;
  }

  return {
    ...result,
    highlights: [
      ...result.highlights,
      {
        highlight: 'PersonalRecordPoints',
        previousRecordInPoints: currentPersonalRecordActivity.points,
      },
    ],
  };
}
