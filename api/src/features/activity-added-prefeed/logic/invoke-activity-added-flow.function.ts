import { PrefeedBuildFn } from '../models/prefeed-build-fn.type';
import { ActivityAddedPrefeedConfigModel, ActivityAddedPrefeedModel } from '../models';
import { buildActivityAddedArgs } from './build-activity-added-args.function';
import { buildActivityAddedPrefeed } from './build-activity-added-prefeed.logic';
import { buildFirstActivityPrefeed } from './build-first-activity-prefeed.function';
import { buildGlobalRecordPointsPrefeed } from './build-global-record-points-prefeed.function';
import { buildPersonalRecordPointsPrefeed } from './build-personal-record-points-prefeed.function';
import { buildRankingAdvancementPrefeed } from './build-ranking-advancement-prefeed.function';
import { activityAddedPrefeedService } from '../database/activity-added-prefeed.service';

/**
 * Collection of functions which are used to determine highlights in given activity.
 */
const ACTIVITY_ADDED_PREFEED_BUILD_FNS: PrefeedBuildFn[] = [
  (result, context) => buildFirstActivityPrefeed(result, context),
  (result, context) => buildPersonalRecordPointsPrefeed(result, context),
  (result, context) => buildGlobalRecordPointsPrefeed(result, context),
  (result, context) => buildRankingAdvancementPrefeed(result, context),
];

/** This function is used to invoke the flow of determining if there is something special in given activity. As a result of this function, new
 * prefeed model might be added to db, which contains all information about activity in context of current challenge.
 * @param athleteId - Athlete's ids, who created activity
 * @param activityId - Activity id, which will be checked
 * @param config - additional configuration which changes flow execution
 * @return activity added prefeed model which contains all data about activity
 */
export async function invokeActivityAddedFlowFunction(
  athleteId: number,
  activityId: number,
  config?: ActivityAddedPrefeedConfigModel,
): Promise<ActivityAddedPrefeedModel> {
  const allAthletes = await buildActivityAddedArgs(config);
  const prefeed = buildActivityAddedPrefeed(
    allAthletes,
    athleteId,
    activityId,
    ACTIVITY_ADDED_PREFEED_BUILD_FNS,
    config,
  );
  if (!isSupported(prefeed)) {
    return null;
  }

  if (!(config?.simulate ?? false)) {
    await activityAddedPrefeedService.save(prefeed);
  }
  return prefeed;
}

function isSupported(prefeed: ActivityAddedPrefeedModel) {
  return prefeed != null;
}
