import { buildActivityAddedPrefeed } from './build-activity-added-prefeed.logic';
import { PrefeedRequestAthleteModelBuilder } from '../test/prefeed-request-athlete-model.builder';
import { PrefeedRequestActivityModelBuilder } from '../test/prefeed-request-activity-model.builder';
import { PrefeedBuildFn } from '../models/prefeed-build-fn.type';
import { PrefeedBuildContextModel } from '../models';

describe(buildActivityAddedPrefeed.name, () => {
  const CURRENT_ATHLETE_ID = 2;
  const CURRENT_ACTIVITY_ID = 103;

  const athleteBuilder = new PrefeedRequestAthleteModelBuilder();
  const activityBuilder = new PrefeedRequestActivityModelBuilder();

  it('should throw error when athlete not found', () => {
    const NON_EXISTING_ATHLETE_ID = -1;
    const ACTIVITY_ID = 1;

    const fn = () => buildActivityAddedPrefeed([], NON_EXISTING_ATHLETE_ID, ACTIVITY_ID, []);

    expect(fn).toThrow(`Athlete with id '${NON_EXISTING_ATHLETE_ID}' not exists`);
  });

  it('should throw error when activity not found', () => {
    const NON_EXISTING_ACTIVITY_ID = 3;
    const athlete = athleteBuilder.withAthleteId(CURRENT_ATHLETE_ID).withNonActivities().build();

    const fn = () => buildActivityAddedPrefeed([athlete], CURRENT_ATHLETE_ID, NON_EXISTING_ACTIVITY_ID, []);

    expect(fn).toThrow(
      `ActivityId '${NON_EXISTING_ACTIVITY_ID}' not exists for athlete with id '${CURRENT_ATHLETE_ID}'`,
    );
  });

  it('should return valid athlete', () => {
    const OTHER_ATHLETE_ID = 123;
    const CURRENT_ATHLETE_NAME = 'John';
    const activity = activityBuilder.withActivityId(CURRENT_ACTIVITY_ID).build();
    const athlete = athleteBuilder
      .withAthleteId(CURRENT_ATHLETE_ID)
      .withAthleteName(CURRENT_ATHLETE_NAME)
      .withActivities([activity])
      .build();
    const athlete2 = athleteBuilder.withAthleteId(OTHER_ATHLETE_ID).withActivities([activity]).build();

    const result = buildActivityAddedPrefeed([athlete2, athlete], CURRENT_ATHLETE_ID, CURRENT_ACTIVITY_ID, []);

    expect(result.athlete.athleteId).toBe(CURRENT_ATHLETE_ID);
    expect(result.athlete.athleteName).toBe(CURRENT_ATHLETE_NAME);
  });

  it('should return activity', () => {
    const activity = activityBuilder.withActivityId(CURRENT_ACTIVITY_ID).build();
    const athlete = athleteBuilder.withAthleteId(CURRENT_ATHLETE_ID).withActivities([activity]).build();

    const result = buildActivityAddedPrefeed([athlete], CURRENT_ATHLETE_ID, CURRENT_ACTIVITY_ID, []);

    expect(result.activity).toBe(activity);
  });

  it('should generate id', () => {
    const activity = activityBuilder.withActivityId(CURRENT_ACTIVITY_ID).build();
    const athlete = athleteBuilder.withAthleteId(CURRENT_ATHLETE_ID).withActivities([activity]).build();

    const result = buildActivityAddedPrefeed([athlete], CURRENT_ATHLETE_ID, CURRENT_ACTIVITY_ID, []);

    expect(result.id).toBeDefined();
  });

  it('buildFn should receive current activity in build fn args', () => {
    const OTHER_ACTIVITY_ID = 123;
    const activity = activityBuilder.withActivityId(CURRENT_ACTIVITY_ID).build();
    const activity2 = activityBuilder.withActivityId(OTHER_ACTIVITY_ID).build();
    const athlete = athleteBuilder.withAthleteId(CURRENT_ATHLETE_ID).withActivities([activity2, activity]).build();
    let receivedContext: PrefeedBuildContextModel;
    const buildFn: PrefeedBuildFn = (result, context) => {
      receivedContext = context;
      return result;
    };

    buildActivityAddedPrefeed([athlete], CURRENT_ATHLETE_ID, CURRENT_ACTIVITY_ID, [buildFn]);

    expect(receivedContext.currentActivity).toBe(activity);
  });

  it('buildFn should receive current athlete in build fn args', () => {
    const OTHER_ATHLETE_ID = 123;
    const activity = activityBuilder.withActivityId(CURRENT_ACTIVITY_ID).build();
    const athlete = athleteBuilder.withAthleteId(CURRENT_ATHLETE_ID).withActivities([activity]).build();
    const athlete2 = athleteBuilder.withAthleteId(OTHER_ATHLETE_ID).withActivities([activity]).build();
    let receivedContext: PrefeedBuildContextModel;
    const buildFn: PrefeedBuildFn = (result, context) => {
      receivedContext = context;
      return result;
    };

    buildActivityAddedPrefeed([athlete2, athlete], CURRENT_ATHLETE_ID, CURRENT_ACTIVITY_ID, [buildFn]);

    expect(receivedContext.currentAthlete).toStrictEqual(athlete);
  });

  it('buildFn should receive other athletes without current athlete in build fn args', () => {
    const activity = activityBuilder.withActivityId(CURRENT_ACTIVITY_ID).build();
    const athlete = athleteBuilder.withAthleteId(CURRENT_ATHLETE_ID).withActivities([activity]).build();
    const athlete2 = athleteBuilder.withAthleteId(123).build();
    const athlete3 = athleteBuilder.withAthleteId(124).build();
    const athlete4 = athleteBuilder.withAthleteId(125).build();
    let receivedContext: PrefeedBuildContextModel;
    const buildFn: PrefeedBuildFn = (result, context) => {
      receivedContext = context;
      return result;
    };

    buildActivityAddedPrefeed([athlete2, athlete, athlete3, athlete4], CURRENT_ATHLETE_ID, CURRENT_ACTIVITY_ID, [
      buildFn,
    ]);

    expect(receivedContext.otherAthletes).toHaveLength(3);
    expect(receivedContext.otherAthletes[0]).toStrictEqual(athlete2);
    expect(receivedContext.otherAthletes[1]).toStrictEqual(athlete3);
    expect(receivedContext.otherAthletes[2]).toStrictEqual(athlete4);
  });

  it('buildFn should receive currentAthleteOtherActivitiesSorted in build fn args', () => {
    const activity = activityBuilder.withActivityId(CURRENT_ACTIVITY_ID).build();
    const activity2 = activityBuilder.withActivityId(7).build();
    const activity3 = activityBuilder.withActivityId(9).build();
    const athlete = athleteBuilder
      .withAthleteId(CURRENT_ATHLETE_ID)
      .withActivities([activity2, activity, activity3])
      .build();
    const athlete2 = athleteBuilder
      .withAthleteId(123)
      .withActivities([activityBuilder.withActivityId(5).build(), activityBuilder.withActivityId(6).build()])
      .build();
    let receivedContext: PrefeedBuildContextModel;
    const buildFn: PrefeedBuildFn = (result, context) => {
      receivedContext = context;
      return result;
    };

    buildActivityAddedPrefeed([athlete2, athlete], CURRENT_ATHLETE_ID, CURRENT_ACTIVITY_ID, [buildFn]);

    expect(receivedContext.currentAthleteOtherActivitiesSorted).toHaveLength(2);
    expect(receivedContext.currentAthleteOtherActivitiesSorted[0]).toBe(activity2);
    expect(receivedContext.currentAthleteOtherActivitiesSorted[1]).toBe(activity3);
  });

  it('buildFn should receive sorted activities in currentAthleteOtherActivitiesSorted in build fn args', () => {
    const activity = activityBuilder
      .createdAt(new Date(2024, 6, 23).toISOString())
      .withActivityId(CURRENT_ACTIVITY_ID)
      .build();
    const activity2 = activityBuilder
      .createdAt(new Date(2024, 5, 23).toISOString())
      .withActivityId(7)
      .build();
    const activity3 = activityBuilder
      .createdAt(new Date(2023, 4, 23).toISOString())
      .withActivityId(9)
      .build();
    const activity4 = activityBuilder
      .createdAt(new Date(2023, 4, 22).toISOString())
      .withActivityId(9)
      .build();
    const athlete = athleteBuilder
      .withAthleteId(CURRENT_ATHLETE_ID)
      .withActivities([activity, activity2, activity3, activity4])
      .build();
    let receivedContext: PrefeedBuildContextModel;
    const buildFn: PrefeedBuildFn = (result, context) => {
      receivedContext = context;
      return result;
    };

    buildActivityAddedPrefeed([athlete], CURRENT_ATHLETE_ID, CURRENT_ACTIVITY_ID, [buildFn]);

    expect(receivedContext.currentAthleteOtherActivitiesSorted).toHaveLength(3);
    expect(receivedContext.currentAthleteOtherActivitiesSorted[0]).toBe(activity4);
    expect(receivedContext.currentAthleteOtherActivitiesSorted[1]).toBe(activity3);
    expect(receivedContext.currentAthleteOtherActivitiesSorted[2]).toBe(activity2);
  });

  it('buildFn should receive otherActivitiesSorted in build fn args', () => {
    const activity = activityBuilder.withActivityId(CURRENT_ACTIVITY_ID).build();
    const activity2 = activityBuilder.withActivityId(7).build();
    const otherActivity1 = activityBuilder.withActivityId(8).build();
    const otherActivity2 = activityBuilder.withActivityId(9).build();
    const otherActivity3 = activityBuilder.withActivityId(10).build();
    const athlete = athleteBuilder.withAthleteId(CURRENT_ATHLETE_ID).withActivities([activity2, activity]).build();
    const athlete2 = athleteBuilder.withAthleteId(123).withActivities([otherActivity1, otherActivity2]).build();
    const athlete3 = athleteBuilder.withAthleteId(123).withActivities([otherActivity3]).build();

    let receivedContext: PrefeedBuildContextModel;
    const buildFn: PrefeedBuildFn = (result, context) => {
      receivedContext = context;
      return result;
    };

    buildActivityAddedPrefeed([athlete2, athlete, athlete3], CURRENT_ATHLETE_ID, CURRENT_ACTIVITY_ID, [buildFn]);

    expect(receivedContext.otherAthleteActivitiesSorted).toHaveLength(3);
    expect(receivedContext.otherAthleteActivitiesSorted[0]).toBe(otherActivity1);
    expect(receivedContext.otherAthleteActivitiesSorted[1]).toBe(otherActivity2);
    expect(receivedContext.otherAthleteActivitiesSorted[2]).toBe(otherActivity3);
  });

  it('buildFn should receive sorted activities in otherActivitiesSorted in build fn args', () => {
    const activity = activityBuilder.withActivityId(CURRENT_ACTIVITY_ID).build();
    const activity2 = activityBuilder.withActivityId(7).build();
    const otherActivity1 = activityBuilder
      .createdAt(new Date(2024, 6, 7).toISOString())
      .withActivityId(8)
      .build();
    const otherActivity2 = activityBuilder
      .createdAt(new Date(2023, 5, 8).toISOString())
      .withActivityId(9)
      .build();
    const otherActivity3 = activityBuilder
      .createdAt(new Date(2023, 5, 7).toISOString())
      .withActivityId(10)
      .build();
    const athlete = athleteBuilder.withAthleteId(CURRENT_ATHLETE_ID).withActivities([activity2, activity]).build();
    const athlete2 = athleteBuilder.withAthleteId(123).withActivities([otherActivity1, otherActivity2]).build();
    const athlete3 = athleteBuilder.withAthleteId(123).withActivities([otherActivity3]).build();
    let receivedContext: PrefeedBuildContextModel;
    const buildFn: PrefeedBuildFn = (result, context) => {
      receivedContext = context;
      return result;
    };

    buildActivityAddedPrefeed([athlete, athlete2, athlete3], CURRENT_ATHLETE_ID, CURRENT_ACTIVITY_ID, [buildFn]);

    expect(receivedContext.otherAthleteActivitiesSorted).toHaveLength(3);
    expect(receivedContext.otherAthleteActivitiesSorted[0]).toBe(otherActivity3);
    expect(receivedContext.otherAthleteActivitiesSorted[1]).toBe(otherActivity2);
    expect(receivedContext.otherAthleteActivitiesSorted[2]).toBe(otherActivity1);
  });

  it('buildFn should join build fns results', () => {
    const activity = activityBuilder.withActivityId(CURRENT_ACTIVITY_ID).build();
    const athlete = athleteBuilder.withAthleteId(CURRENT_ATHLETE_ID).withActivities([activity]).build();
    const buildFn: PrefeedBuildFn = result => {
      return {
        ...result,
        highlights: [...result.highlights, { highlight: 'FirstActivity' }],
      };
    };
    const buildFn2: PrefeedBuildFn = result => {
      return {
        ...result,
        highlights: [...result.highlights, { highlight: 'FirstActivity' }],
      };
    };

    const result = buildActivityAddedPrefeed([athlete], CURRENT_ATHLETE_ID, CURRENT_ACTIVITY_ID, [buildFn, buildFn2]);

    expect(result.highlights).toHaveLength(2);
  });

  it('other athletes should contain only activities before current activity, when keepHistory config is true', () => {
    const CURRENT_DAY = new Date(2024, 6, 7).toISOString();
    const PREVIOUS_DAY = new Date(2024, 6, 6).toISOString();
    const NEXT_DAY = new Date(2024, 6, 8).toISOString();

    const activity = activityBuilder.endedAt(CURRENT_DAY).withActivityId(CURRENT_ACTIVITY_ID).build();
    const otherActivity1 = activityBuilder.endedAt(PREVIOUS_DAY).withActivityId(8).build();
    const otherActivity2 = activityBuilder.endedAt(NEXT_DAY).withActivityId(9).build();
    const athlete = athleteBuilder.withAthleteId(CURRENT_ATHLETE_ID).withActivities([activity]).build();
    const athlete2 = athleteBuilder.withAthleteId(123).withActivities([otherActivity1, otherActivity2]).build();
    let receivedContext: PrefeedBuildContextModel;
    const buildFn: PrefeedBuildFn = (result, context) => {
      receivedContext = context;
      return result;
    };

    buildActivityAddedPrefeed([athlete, athlete2], CURRENT_ATHLETE_ID, CURRENT_ACTIVITY_ID, [buildFn], {
      keepHistory: true,
    });

    expect(receivedContext.otherAthleteActivitiesSorted).toHaveLength(1);
    expect(receivedContext.otherAthleteActivitiesSorted[0]).toBe(otherActivity1);
    expect(receivedContext.otherAthletes[0].activities).toHaveLength(1);
    expect(receivedContext.otherAthletes[0].activities[0]).toBe(otherActivity1);
  });

  it('current athlete should contain only activities before current activity, when keepHistory config is true', () => {
    const CURRENT_DAY = new Date(2024, 6, 7).toISOString();
    const PREVIOUS_DAY = new Date(2024, 6, 6).toISOString();
    const NEXT_DAY = new Date(2024, 6, 8).toISOString();

    const activity = activityBuilder.endedAt(CURRENT_DAY).withActivityId(CURRENT_ACTIVITY_ID).build();
    const activity1 = activityBuilder.endedAt(PREVIOUS_DAY).withActivityId(8).build();
    const activity2 = activityBuilder.endedAt(NEXT_DAY).withActivityId(9).build();
    const athlete = athleteBuilder
      .withAthleteId(CURRENT_ATHLETE_ID)
      .withActivities([activity, activity1, activity2])
      .build();
    let receivedContext: PrefeedBuildContextModel;
    const buildFn: PrefeedBuildFn = (result, context) => {
      receivedContext = context;
      return result;
    };

    buildActivityAddedPrefeed([athlete], CURRENT_ATHLETE_ID, CURRENT_ACTIVITY_ID, [buildFn], { keepHistory: true });

    expect(receivedContext.currentAthleteOtherActivitiesSorted).toHaveLength(1);
    expect(receivedContext.currentAthleteOtherActivitiesSorted[0]).toBe(activity1);
    expect(receivedContext.currentAthlete.activities).toHaveLength(2);
    expect(receivedContext.currentAthlete.activities[0]).toBe(activity);
    expect(receivedContext.currentAthlete.activities[1]).toBe(activity1);
  });
});
