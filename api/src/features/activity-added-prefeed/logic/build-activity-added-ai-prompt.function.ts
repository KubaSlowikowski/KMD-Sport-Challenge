import { AiPromptModel, AiStyleType } from '../../chat-gpt/models';
import { getSportConfig } from '../../../config/rankingConfig';
import { AI_SYSTEM_PROMPT } from '../const/ai-system-prompt.const';
import {
  ActivityAddedPrefeedModel,
  PrefeedHighlightGlobalRecordPointsModel,
  PrefeedHighlightPersonalRecordPointsModel,
  PrefeedHighlightRankingAdvancementModel,
} from '../models';
import { ReferenceAthleteModel } from '../../prefeed/models';

/** Builds ai propt for ActivityAddedPrefeed model with given style.
 * @param model - activity added model with highlights. Must contains at least one highlight.
 * @param style - style in which prompt should be created.
 * @returns ai prompt to generate feed text by ai
 */
export function buildActivityAddedAiPrompt(model: ActivityAddedPrefeedModel, style: AiStyleType): AiPromptModel {
  if (!someHighlights(model)) {
    throw new Error('No highlights are given. To build ai prompt at least one type is required.');
  }

  const system = buildSystemPrompt();

  let userResult = getStylePrompt(style);
  if (isGlobalRecord(model)) {
    userResult += buildGlobalRecordPrompt(model);
  } else if (isPersonalRecord(model)) {
    userResult += buildPersonalRecordPrompt(model);
  } else {
    userResult += buildActivityPrompt(model);
  }

  if (isFirstActivity(model)) {
    userResult += ' ' + buildFirstActivityPrompt();
  }

  if (hasAdvancedInRanking(model)) {
    userResult += ' ' + buildRankingAdvancementPrompt(model);
  }

  return {
    system,
    user: userResult,
  };
}

function isFirstActivity(model: ActivityAddedPrefeedModel) {
  return model.highlights.some(h => h.highlight === 'FirstActivity');
}

function isPersonalRecord(model: ActivityAddedPrefeedModel) {
  return model.highlights.some(h => h.highlight === 'PersonalRecordPoints');
}

function isGlobalRecord(model: ActivityAddedPrefeedModel) {
  return model.highlights.some(h => h.highlight === 'GlobalRecordPoints');
}

function someHighlights(model: ActivityAddedPrefeedModel) {
  return model.highlights.length >= 0;
}

function hasAdvancedInRanking(model: ActivityAddedPrefeedModel) {
  return model.highlights.some(h => h.highlight === 'RankingAdvancement');
}

function buildSystemPrompt(): string {
  return AI_SYSTEM_PROMPT;
}

function buildFirstActivityPrompt(): string {
  return 'This is first activity';
}

function buildActivityPrompt(model: ActivityAddedPrefeedModel): string {
  const sportName = getSportName(model);
  return `${lookUpAthleteToken(model, model.athlete)} just finished ${sportName} training, and got ${model.activity.points} points.`;
}

function buildPersonalRecordPrompt(model: ActivityAddedPrefeedModel): string {
  const personalRecordHighlight = model.highlights.find(
    h => h.highlight === 'PersonalRecordPoints',
  ) as PrefeedHighlightPersonalRecordPointsModel;
  const sportName = getSportName(model);
  return `${lookUpAthleteToken(model, model.athlete)} just finished ${sportName} training, and beat personal record. Previous record is ${personalRecordHighlight.previousRecordInPoints} points and new is ${model.activity.points} points`;
}

function buildGlobalRecordPrompt(model: ActivityAddedPrefeedModel): string {
  const globalRecordHighlight = model.highlights.find(
    h => h.highlight === 'GlobalRecordPoints',
  ) as PrefeedHighlightGlobalRecordPointsModel;
  const sportName = getSportName(model);
  return `${lookUpAthleteToken(model, model.athlete)} just finished ${sportName} training, and beat challenge record. Previous record is ${globalRecordHighlight.previousRecordInPoints} points and belongs to ${lookUpAthleteToken(model, globalRecordHighlight.previousAthlete)} and new is ${model.activity.points} points`;
}

function buildRankingAdvancementPrompt(model: ActivityAddedPrefeedModel): string {
  const rankingAdvancement = model.highlights.find(
    h => h.highlight === 'RankingAdvancement',
  ) as PrefeedHighlightRankingAdvancementModel;
  const athletesJoined = rankingAdvancement.athletesBeaten
    .slice(0, 3)
    .map(a => lookUpAthleteToken(model, a))
    .join(', ');
  const andMoreSuffix = rankingAdvancement.athletesBeaten.length > 3;
  return ` And advanced from position ${rankingAdvancement.fromPosition + 1} to position: ${rankingAdvancement.toPosition + 1}, overtaking${athletesJoined} ${andMoreSuffix ? ' and more.' : ''}`;
}

function lookUpAthleteToken(model: ActivityAddedPrefeedModel, athlete: ReferenceAthleteModel): string {
  const athleteToken = model.athleteTokens.find(a => a.athleteId === athlete.athleteId);
  return athleteToken.tokenText;
}

function getStylePrompt(style: AiStyleType): string {
  switch (style) {
    case 'Funny':
      return 'Generate in funny mode: ';
    case 'Motivation':
      return 'Generate in motivation talk mode: ';
    default:
      throw new Error('Not mapped');
  }
}

function getSportName(model: ActivityAddedPrefeedModel): string {
  return getSportConfig(model.activity.sportType).name ?? model.activity.sportType;
}
