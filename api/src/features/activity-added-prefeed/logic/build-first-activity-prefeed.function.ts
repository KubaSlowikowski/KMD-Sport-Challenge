import { PrefeedBuildContextModel } from '../models';
import { ActivityAddedPrefeedModel } from '../models';

/**
 * Finds if activity was athlete's first activity in current challange.
 * @param result current result of add activity prefeed flow
 * @param context data from challenge aggregated into helpful fields.
 * @returns result with additional highlight if given activity was first
 */
export function buildFirstActivityPrefeed(
  result: ActivityAddedPrefeedModel,
  context: PrefeedBuildContextModel,
): ActivityAddedPrefeedModel {
  if (context.currentAthleteOtherActivitiesSorted.length > 0) {
    return result;
  }

  return {
    ...result,
    highlights: [...result.highlights, { highlight: 'FirstActivity' }],
  };
}
