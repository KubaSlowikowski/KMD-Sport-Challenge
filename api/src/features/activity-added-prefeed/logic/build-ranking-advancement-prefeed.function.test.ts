import { PrefeedBuildContextModel } from '../models';
import { ActivityAddedPrefeedModel, PrefeedHighlightRankingAdvancementModel } from '../models';
import { PrefeedRequestActivityModelBuilder } from '../test/prefeed-request-activity-model.builder';
import { PrefeedRequestAthleteModelBuilder } from '../test/prefeed-request-athlete-model.builder';
import { buildRankingAdvancementPrefeed } from './build-ranking-advancement-prefeed.function';

describe(buildRankingAdvancementPrefeed.name, () => {
  const BEFORE_CURRENT_ACTIVITY_DATE = new Date(2024, 1, 1).toISOString();
  const CURRENT_ACTIVITY_DATE = new Date(2024, 1, 2).toISOString();
  const AFTER_CURRENT_ACTIVITY_DATE = new Date(2024, 1, 3).toISOString();

  const athleteBuilder = new PrefeedRequestAthleteModelBuilder();
  const activityBuilder = new PrefeedRequestActivityModelBuilder();
  let buildResult: ActivityAddedPrefeedModel;

  beforeEach(() => {
    buildResult = {
      id: '',
      type: 'ActivityAdded',
      athlete: null,
      activity: null,
      highlights: [],
      created: new Date().toISOString(),
      athleteTokens: [],
    };
  });

  it('should not contains RankingAdvancements, when is only athlete', () => {
    const currentActivity = activityBuilder.build();
    const currentAthlete = athleteBuilder.withAthleteId(1).withActivities([currentActivity]).build();
    const context: PrefeedBuildContextModel = {
      currentActivity,
      currentAthleteOtherActivitiesSorted: [],
      currentAthlete,
      otherAthleteActivitiesSorted: [],
      otherAthletes: [],
    };

    const result = buildRankingAdvancementPrefeed(buildResult, context);

    expect(result.highlights).toHaveLength(0);
  });

  it('should not contains RankingAdvancements, when nothing changed', () => {
    const currentActivity = activityBuilder.createdAt(CURRENT_ACTIVITY_DATE).withPoints(20).build();
    const otherActivity = activityBuilder.createdAt(BEFORE_CURRENT_ACTIVITY_DATE).withPoints(30).build();
    const currentAthlete = athleteBuilder.withAthleteId(1).withActivities([currentActivity, otherActivity]).build();
    const otherAthleteActivity = activityBuilder.createdAt(BEFORE_CURRENT_ACTIVITY_DATE).withPoints(1000).build();
    const otherAthlete = athleteBuilder.withAthleteId(2).withActivities([otherAthleteActivity]).build();
    const context: PrefeedBuildContextModel = {
      currentActivity,
      currentAthleteOtherActivitiesSorted: [otherActivity],
      currentAthlete,
      otherAthleteActivitiesSorted: [otherAthleteActivity],
      otherAthletes: [otherAthlete],
    };

    const result = buildRankingAdvancementPrefeed(buildResult, context);

    expect(result.highlights).toHaveLength(0);
  });

  it('should contains RankingAdvancements, when athlete has beaten other athlete', () => {
    const currentActivity = activityBuilder.createdAt(CURRENT_ACTIVITY_DATE).withPoints(9000).build();
    const otherActivity = activityBuilder.createdAt(BEFORE_CURRENT_ACTIVITY_DATE).withPoints(30).build();
    const currentAthlete = athleteBuilder.withAthleteId(1).withActivities([currentActivity, otherActivity]).build();
    const otherAthleteActivity = activityBuilder.createdAt(BEFORE_CURRENT_ACTIVITY_DATE).withPoints(1000).build();
    const otherAthlete = athleteBuilder.withAthleteId(2).withActivities([otherAthleteActivity]).build();
    const context: PrefeedBuildContextModel = {
      currentActivity,
      currentAthleteOtherActivitiesSorted: [otherActivity],
      currentAthlete,
      otherAthleteActivitiesSorted: [otherAthleteActivity],
      otherAthletes: [otherAthlete],
    };

    const result = buildRankingAdvancementPrefeed(buildResult, context);

    expect(result.highlights).toHaveLength(1);
    expect(result.highlights[0].highlight).toBe('RankingAdvancement');
  });

  it('should contains valid from position', () => {
    const currentActivity = activityBuilder.createdAt(CURRENT_ACTIVITY_DATE).withPoints(9000).build();
    const otherActivity = activityBuilder.createdAt(BEFORE_CURRENT_ACTIVITY_DATE).withPoints(30).build();
    const currentAthlete = athleteBuilder.withAthleteId(1).withActivities([currentActivity, otherActivity]).build();
    const otherAthleteActivity = activityBuilder.createdAt(BEFORE_CURRENT_ACTIVITY_DATE).withPoints(1000).build();
    const otherAthlete = athleteBuilder.withAthleteId(2).withActivities([otherAthleteActivity]).build();
    const context: PrefeedBuildContextModel = {
      currentActivity,
      currentAthleteOtherActivitiesSorted: [otherActivity],
      currentAthlete,
      otherAthleteActivitiesSorted: [otherAthleteActivity],
      otherAthletes: [otherAthlete],
    };

    const result = buildRankingAdvancementPrefeed(buildResult, context);

    const type = result.highlights[0] as PrefeedHighlightRankingAdvancementModel;
    expect(type.fromPosition).toBe(1);
  });

  it('should contains valid to position', () => {
    const currentActivity = activityBuilder.createdAt(CURRENT_ACTIVITY_DATE).withPoints(9000).build();
    const otherActivity = activityBuilder.createdAt(BEFORE_CURRENT_ACTIVITY_DATE).withPoints(30).build();
    const currentAthlete = athleteBuilder.withAthleteId(1).withActivities([currentActivity, otherActivity]).build();
    const otherAthleteActivity = activityBuilder.createdAt(BEFORE_CURRENT_ACTIVITY_DATE).withPoints(1000).build();
    const otherAthlete = athleteBuilder.withAthleteId(2).withActivities([otherAthleteActivity]).build();
    const context: PrefeedBuildContextModel = {
      currentActivity,
      currentAthleteOtherActivitiesSorted: [otherActivity],
      currentAthlete,
      otherAthleteActivitiesSorted: [otherAthleteActivity],
      otherAthletes: [otherAthlete],
    };

    const result = buildRankingAdvancementPrefeed(buildResult, context);

    const type = result.highlights[0] as PrefeedHighlightRankingAdvancementModel;
    expect(type.toPosition).toBe(0);
  });

  it('should not contains RankingAdvancements, when position lost', () => {
    const currentActivity = activityBuilder.createdAt(CURRENT_ACTIVITY_DATE).withPoints(9000).build();
    const otherActivity = activityBuilder.createdAt(BEFORE_CURRENT_ACTIVITY_DATE).withPoints(30).build();
    const currentAthlete = athleteBuilder.withAthleteId(1).withActivities([currentActivity, otherActivity]).build();
    const otherAthleteActivity = activityBuilder.createdAt(BEFORE_CURRENT_ACTIVITY_DATE).withPoints(1000).build();
    const otherAthleteActivity2 = activityBuilder.createdAt(AFTER_CURRENT_ACTIVITY_DATE).withPoints(90000).build();
    const otherAthlete = athleteBuilder
      .withAthleteId(2)
      .withActivities([otherAthleteActivity, otherAthleteActivity2])
      .build();
    const context: PrefeedBuildContextModel = {
      currentActivity,
      currentAthleteOtherActivitiesSorted: [otherActivity],
      currentAthlete,
      otherAthleteActivitiesSorted: [otherAthleteActivity, otherAthleteActivity2],
      otherAthletes: [otherAthlete],
    };

    const result = buildRankingAdvancementPrefeed(buildResult, context);

    expect(result.highlights).toHaveLength(0);
  });

  it('should contains athletes beaten', () => {
    const OTHER_ATHLETE_ID = 123;
    const OTHER_ATHLETE_NAME = 'John';
    const currentActivity = activityBuilder.createdAt(CURRENT_ACTIVITY_DATE).withPoints(9000).build();
    const otherActivity = activityBuilder.createdAt(BEFORE_CURRENT_ACTIVITY_DATE).withPoints(30).build();
    const currentAthlete = athleteBuilder.withAthleteId(1).withActivities([currentActivity, otherActivity]).build();
    const otherAthleteActivity = activityBuilder.createdAt(BEFORE_CURRENT_ACTIVITY_DATE).withPoints(1000).build();
    const otherAthlete = athleteBuilder
      .withAthleteName(OTHER_ATHLETE_NAME)
      .withAthleteId(OTHER_ATHLETE_ID)
      .withActivities([otherAthleteActivity])
      .build();
    const context: PrefeedBuildContextModel = {
      currentActivity,
      currentAthleteOtherActivitiesSorted: [otherActivity],
      currentAthlete,
      otherAthleteActivitiesSorted: [otherAthleteActivity],
      otherAthletes: [otherAthlete],
    };

    const result = buildRankingAdvancementPrefeed(buildResult, context);

    const type = result.highlights[0] as PrefeedHighlightRankingAdvancementModel;
    expect(type.athletesBeaten).toHaveLength(1);
    expect(type.athletesBeaten[0].athleteId).toBe(OTHER_ATHLETE_ID);
    expect(type.athletesBeaten[0].athleteName).toBe(OTHER_ATHLETE_NAME);
  });

  it('should contains 2 athletes beaten', () => {
    const ATHLETE_2_ID = 123;
    const ATHLETE_2_NAME = 'John';
    const ATHLETE_3_ID = 124;
    const ATHLETE_3_NAME = 'Barbara';
    const currentActivity = activityBuilder.createdAt(CURRENT_ACTIVITY_DATE).withPoints(9000).build();
    const otherActivity = activityBuilder.createdAt(BEFORE_CURRENT_ACTIVITY_DATE).withPoints(30).build();
    const currentAthlete = athleteBuilder.withAthleteId(1).withActivities([currentActivity, otherActivity]).build();
    const athlete2Activity = activityBuilder.createdAt(BEFORE_CURRENT_ACTIVITY_DATE).withPoints(1000).build();
    const athlete3Activity = activityBuilder.createdAt(BEFORE_CURRENT_ACTIVITY_DATE).withPoints(1000).build();
    const athlete2 = athleteBuilder
      .withAthleteName(ATHLETE_2_NAME)
      .withAthleteId(ATHLETE_2_ID)
      .withActivities([athlete2Activity])
      .build();
    const athlete3 = athleteBuilder
      .withAthleteName(ATHLETE_3_NAME)
      .withAthleteId(ATHLETE_3_ID)
      .withActivities([athlete3Activity])
      .build();
    const context: PrefeedBuildContextModel = {
      currentActivity,
      currentAthleteOtherActivitiesSorted: [otherActivity],
      currentAthlete,
      otherAthleteActivitiesSorted: [athlete2Activity, athlete3Activity],
      otherAthletes: [athlete2, athlete3],
    };

    const result = buildRankingAdvancementPrefeed(buildResult, context);

    const type = result.highlights[0] as PrefeedHighlightRankingAdvancementModel;
    expect(type.athletesBeaten).toHaveLength(2);
    expect(type.athletesBeaten[0].athleteId).toBe(ATHLETE_2_ID);
    expect(type.athletesBeaten[0].athleteName).toBe(ATHLETE_2_NAME);
    expect(type.athletesBeaten[1].athleteId).toBe(ATHLETE_3_ID);
    expect(type.athletesBeaten[1].athleteName).toBe(ATHLETE_3_NAME);
  });

  it('athletes beaten should be sorted by position in current ranking', () => {
    const ATHLETE_2_ID = 123;
    const ATHLETE_2_NAME = 'John';
    const ATHLETE_3_ID = 124;
    const ATHLETE_3_NAME = 'Barbara';
    const currentActivity = activityBuilder.createdAt(CURRENT_ACTIVITY_DATE).withPoints(9000).build();
    const otherActivity = activityBuilder.createdAt(BEFORE_CURRENT_ACTIVITY_DATE).withPoints(30).build();
    const currentAthlete = athleteBuilder.withAthleteId(1).withActivities([currentActivity, otherActivity]).build();
    const athlete2Activity = activityBuilder.createdAt(BEFORE_CURRENT_ACTIVITY_DATE).withPoints(1000).build();
    const athlete3Activity = activityBuilder.createdAt(BEFORE_CURRENT_ACTIVITY_DATE).withPoints(1000).build();
    const athlete3Activity2 = activityBuilder.createdAt(AFTER_CURRENT_ACTIVITY_DATE).withPoints(1000).build();
    const athlete2 = athleteBuilder
      .withAthleteName(ATHLETE_2_NAME)
      .withAthleteId(ATHLETE_2_ID)
      .withActivities([athlete2Activity])
      .build();
    const athlete3 = athleteBuilder
      .withAthleteName(ATHLETE_3_NAME)
      .withAthleteId(ATHLETE_3_ID)
      .withActivities([athlete3Activity, athlete3Activity2])
      .build();
    const context: PrefeedBuildContextModel = {
      currentActivity,
      currentAthleteOtherActivitiesSorted: [otherActivity],
      currentAthlete,
      otherAthleteActivitiesSorted: [athlete2Activity, athlete3Activity2, athlete3Activity],
      otherAthletes: [athlete2, athlete3],
    };

    const result = buildRankingAdvancementPrefeed(buildResult, context);

    const type = result.highlights[0] as PrefeedHighlightRankingAdvancementModel;
    expect(type.athletesBeaten[0].athleteId).toBe(ATHLETE_3_ID);
    expect(type.athletesBeaten[0].athleteName).toBe(ATHLETE_3_NAME);
    expect(type.athletesBeaten[1].athleteId).toBe(ATHLETE_2_ID);
    expect(type.athletesBeaten[1].athleteName).toBe(ATHLETE_2_NAME);
  });

  it('should contains only 2 athletes beaten, not 1st athlete', () => {
    const ATHLETE_2_ID = 123;
    const ATHLETE_2_NAME = 'John';
    const ATHLETE_3_ID = 124;
    const ATHLETE_3_NAME = 'Barbara';
    const BEST_ATHLETE_ID = 125;
    const BEST_ATHLETE_NAME = 'Michal';
    const bestActivity = activityBuilder.createdAt(BEFORE_CURRENT_ACTIVITY_DATE).withPoints(1000000).build();
    const currentActivity = activityBuilder.createdAt(CURRENT_ACTIVITY_DATE).withPoints(9000).build();
    const otherActivity = activityBuilder.createdAt(BEFORE_CURRENT_ACTIVITY_DATE).withPoints(30).build();
    const currentAthlete = athleteBuilder.withAthleteId(1).withActivities([currentActivity, otherActivity]).build();
    const athlete2Activity = activityBuilder.createdAt(BEFORE_CURRENT_ACTIVITY_DATE).withPoints(1000).build();
    const athlete3Activity = activityBuilder.createdAt(BEFORE_CURRENT_ACTIVITY_DATE).withPoints(1000).build();
    const athlete2 = athleteBuilder
      .withAthleteName(ATHLETE_2_NAME)
      .withAthleteId(ATHLETE_2_ID)
      .withActivities([athlete2Activity])
      .build();
    const athlete3 = athleteBuilder
      .withAthleteName(ATHLETE_3_NAME)
      .withAthleteId(ATHLETE_3_ID)
      .withActivities([athlete3Activity])
      .build();
    const bestAthlete = athleteBuilder
      .withAthleteName(BEST_ATHLETE_NAME)
      .withAthleteId(BEST_ATHLETE_ID)
      .withActivities([bestActivity])
      .build();
    const context: PrefeedBuildContextModel = {
      currentActivity,
      currentAthleteOtherActivitiesSorted: [otherActivity],
      currentAthlete,
      otherAthleteActivitiesSorted: [bestActivity, athlete2Activity, athlete3Activity],
      otherAthletes: [athlete2, athlete3, bestAthlete],
    };

    const result = buildRankingAdvancementPrefeed(buildResult, context);

    const type = result.highlights[0] as PrefeedHighlightRankingAdvancementModel;
    expect(type.athletesBeaten).toHaveLength(2);
    expect(type.athletesBeaten[0].athleteId).toBe(ATHLETE_2_ID);
    expect(type.athletesBeaten[1].athleteId).toBe(ATHLETE_3_ID);
  });

  it('should contains valid from position when 2 athletes beaten, but not 1st athlete', () => {
    const ATHLETE_2_ID = 123;
    const ATHLETE_2_NAME = 'John';
    const ATHLETE_3_ID = 124;
    const ATHLETE_3_NAME = 'Barbara';
    const BEST_ATHLETE_ID = 125;
    const BEST_ATHLETE_NAME = 'Michal';
    const bestActivity = activityBuilder.createdAt(BEFORE_CURRENT_ACTIVITY_DATE).withPoints(1000000).build();
    const currentActivity = activityBuilder.createdAt(CURRENT_ACTIVITY_DATE).withPoints(9000).build();
    const otherActivity = activityBuilder.createdAt(BEFORE_CURRENT_ACTIVITY_DATE).withPoints(30).build();
    const currentAthlete = athleteBuilder.withAthleteId(1).withActivities([currentActivity, otherActivity]).build();
    const athlete2Activity = activityBuilder.createdAt(BEFORE_CURRENT_ACTIVITY_DATE).withPoints(1000).build();
    const athlete3Activity = activityBuilder.createdAt(BEFORE_CURRENT_ACTIVITY_DATE).withPoints(1000).build();
    const athlete2 = athleteBuilder
      .withAthleteName(ATHLETE_2_NAME)
      .withAthleteId(ATHLETE_2_ID)
      .withActivities([athlete2Activity])
      .build();
    const athlete3 = athleteBuilder
      .withAthleteName(ATHLETE_3_NAME)
      .withAthleteId(ATHLETE_3_ID)
      .withActivities([athlete3Activity])
      .build();
    const bestAthlete = athleteBuilder
      .withAthleteName(BEST_ATHLETE_NAME)
      .withAthleteId(BEST_ATHLETE_ID)
      .withActivities([bestActivity])
      .build();
    const context: PrefeedBuildContextModel = {
      currentActivity,
      currentAthleteOtherActivitiesSorted: [otherActivity],
      currentAthlete,
      otherAthleteActivitiesSorted: [bestActivity, athlete2Activity, athlete3Activity],
      otherAthletes: [athlete2, athlete3, bestAthlete],
    };

    const result = buildRankingAdvancementPrefeed(buildResult, context);

    const type = result.highlights[0] as PrefeedHighlightRankingAdvancementModel;
    expect(type.fromPosition).toBe(3);
  });

  it('should contains valid to position when 2 athletes beaten, but not 1st athlete', () => {
    const ATHLETE_2_ID = 123;
    const ATHLETE_2_NAME = 'John';
    const ATHLETE_3_ID = 124;
    const ATHLETE_3_NAME = 'Barbara';
    const BEST_ATHLETE_ID = 125;
    const BEST_ATHLETE_NAME = 'Michal';
    const bestActivity = activityBuilder.createdAt(BEFORE_CURRENT_ACTIVITY_DATE).withPoints(1000000).build();
    const currentActivity = activityBuilder.createdAt(CURRENT_ACTIVITY_DATE).withPoints(9000).build();
    const otherActivity = activityBuilder.createdAt(BEFORE_CURRENT_ACTIVITY_DATE).withPoints(30).build();
    const currentAthlete = athleteBuilder.withAthleteId(1).withActivities([currentActivity, otherActivity]).build();
    const athlete2Activity = activityBuilder.createdAt(BEFORE_CURRENT_ACTIVITY_DATE).withPoints(1000).build();
    const athlete3Activity = activityBuilder.createdAt(BEFORE_CURRENT_ACTIVITY_DATE).withPoints(1000).build();
    const athlete2 = athleteBuilder
      .withAthleteName(ATHLETE_2_NAME)
      .withAthleteId(ATHLETE_2_ID)
      .withActivities([athlete2Activity])
      .build();
    const athlete3 = athleteBuilder
      .withAthleteName(ATHLETE_3_NAME)
      .withAthleteId(ATHLETE_3_ID)
      .withActivities([athlete3Activity])
      .build();
    const bestAthlete = athleteBuilder
      .withAthleteName(BEST_ATHLETE_NAME)
      .withAthleteId(BEST_ATHLETE_ID)
      .withActivities([bestActivity])
      .build();
    const context: PrefeedBuildContextModel = {
      currentActivity,
      currentAthleteOtherActivitiesSorted: [otherActivity],
      currentAthlete,
      otherAthleteActivitiesSorted: [bestActivity, athlete2Activity, athlete3Activity],
      otherAthletes: [athlete2, athlete3, bestAthlete],
    };

    const result = buildRankingAdvancementPrefeed(buildResult, context);

    const type = result.highlights[0] as PrefeedHighlightRankingAdvancementModel;
    expect(type.toPosition).toBe(1);
  });

  it('should contains only 2 athletes beaten, not 1st athlete - which made enough points after current activity', () => {
    const ATHLETE_2_ID = 123;
    const ATHLETE_2_NAME = 'John';
    const ATHLETE_3_ID = 124;
    const ATHLETE_3_NAME = 'Barbara';
    const BEST_ATHLETE_ID = 125;
    const BEST_ATHLETE_NAME = 'Michal';
    const bestAthleteActivity1 = activityBuilder.createdAt(BEFORE_CURRENT_ACTIVITY_DATE).withPoints(2000).build();
    const bestAthleteActivity2 = activityBuilder.createdAt(AFTER_CURRENT_ACTIVITY_DATE).withPoints(9999999).build();
    const currentActivity = activityBuilder.createdAt(CURRENT_ACTIVITY_DATE).withPoints(9000).build();
    const otherActivity = activityBuilder.createdAt(BEFORE_CURRENT_ACTIVITY_DATE).withPoints(30).build();
    const currentAthlete = athleteBuilder.withAthleteId(1).withActivities([currentActivity, otherActivity]).build();
    const athlete2Activity = activityBuilder.createdAt(BEFORE_CURRENT_ACTIVITY_DATE).withPoints(1000).build();
    const athlete3Activity = activityBuilder.createdAt(BEFORE_CURRENT_ACTIVITY_DATE).withPoints(1000).build();
    const athlete2 = athleteBuilder
      .withAthleteName(ATHLETE_2_NAME)
      .withAthleteId(ATHLETE_2_ID)
      .withActivities([athlete2Activity])
      .build();
    const athlete3 = athleteBuilder
      .withAthleteName(ATHLETE_3_NAME)
      .withAthleteId(ATHLETE_3_ID)
      .withActivities([athlete3Activity])
      .build();
    const bestAthlete = athleteBuilder
      .withAthleteName(BEST_ATHLETE_NAME)
      .withAthleteId(BEST_ATHLETE_ID)
      .withActivities([bestAthleteActivity1, bestAthleteActivity2])
      .build();
    const context: PrefeedBuildContextModel = {
      currentActivity,
      currentAthleteOtherActivitiesSorted: [otherActivity],
      currentAthlete,
      otherAthleteActivitiesSorted: [bestAthleteActivity1, bestAthleteActivity2, athlete2Activity, athlete3Activity],
      otherAthletes: [athlete2, athlete3, bestAthlete],
    };

    const result = buildRankingAdvancementPrefeed(buildResult, context);

    const type = result.highlights[0] as PrefeedHighlightRankingAdvancementModel;
    expect(type.athletesBeaten).toHaveLength(2);
    expect(type.athletesBeaten[0].athleteId).toBe(ATHLETE_2_ID);
    expect(type.athletesBeaten[1].athleteId).toBe(ATHLETE_3_ID);
  });
});
