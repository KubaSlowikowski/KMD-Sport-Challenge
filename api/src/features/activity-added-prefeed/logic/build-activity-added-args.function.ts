import { ActivityAddedPrefeedConfigModel, PrefeedRequestAthleteModel } from '../models';
import { calculateActivityPoints, isActivitySupported } from '../../../services/rankingService';
import { Athlete } from '../../../models/domain/Athlete';
import { Activity } from '../../../models/domain/Activity';
import { athleteService } from '../../../services/athleteService';
import { activityService } from '../../../services/activityService';

/** Prepares all data for activityAddedPrefeed flow, so they can be used in easy way.
 * @param config - when cachedAthletes is set instead of getting entries from db it will use entries from cachedAthletes collection.
 * @returns data used by activityAddedPrefeed
 */
export async function buildActivityAddedArgs(
  config?: ActivityAddedPrefeedConfigModel,
): Promise<PrefeedRequestAthleteModel[]> {
  const athletes: Athlete[] = config?.cachedAthletes ?? (await athleteService.getAllAthletes());
  const activities: Activity[] = config?.cachedActivities ?? (await activityService.getAllActivities());

  return athletes.map(a => ({
    athleteId: a.athleteId,
    athleteName: a.name,
    sex: a.sex,
    activities: activities
      .filter(act => act.athleteId === a.athleteId)
      .map(a => ({
        activityId: a.id,
        sportType: a.sport_type,
        points: Math.floor(calculateActivityPoints(a)),
        supported: isActivitySupported(a),
        createdDate: new Date(a.start_date).toISOString(),
        endDate: new Date(new Date(a.start_date).getTime() + a.elapsed_time * 1000).toISOString(),
      })),
  }));
}
