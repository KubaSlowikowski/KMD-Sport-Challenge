import { PrefeedBuildContextModel } from '../models';
import { PrefeedBuildFn } from '../models/prefeed-build-fn.type';
import {
  ActivityAddedPrefeedConfigModel,
  ActivityAddedPrefeedModel,
  PrefeedRequestActivityModel,
  PrefeedRequestAthleteModel,
} from '../models';
import crypto from 'crypto';

/**
 * Invokes functions on activity, which are responsible for finding some highlights in activity. This function also prepares args for given functions, to simplify execution of logic.
 * @param allAthletes - data used by flow which contains athlete's and their's activities.
 * @param athleteId - Athlete's ids, who created activity
 * @param activityId - Activity id, which will be checked
 * @param buildFns - collection of functions which will be used to determine highlights from activity
 * @param config - additional configuration which changes flow execution. If keepHistory will be true, it will filter out all activities which end's after given activity end date.
 * It can be used to determine activity highlights in history, not in current context.
 * @returns - model which contains all highlights of activity (might be empty).
 * */
export function buildActivityAddedPrefeed(
  allAthletes: PrefeedRequestAthleteModel[],
  athleteId: number,
  activityId: number,
  buildFns: PrefeedBuildFn[],
  config?: ActivityAddedPrefeedConfigModel,
): ActivityAddedPrefeedModel {
  const currentAthlete = allAthletes.find(a => a.athleteId === athleteId);
  if (currentAthlete == null) {
    throw new Error(`Athlete with id '${athleteId}' not exists`);
  }

  const currentActivity = currentAthlete.activities.find(a => a.activityId === activityId);
  if (currentActivity == null) {
    throw new Error(`ActivityId '${activityId}' not exists for athlete with id '${currentAthlete.athleteId}'`);
  }

  const allAthletesWithAppliedConfig = allAthletes.map(a => applyConfigForAthlete(a, config, currentActivity));
  const currentAthleteWithAppliedConfig = applyConfigForAthlete(currentAthlete, config, currentActivity);
  const otherAthletesWithAppliedConfig = allAthletesWithAppliedConfig.filter(
    a => a.athleteId !== currentAthlete.athleteId,
  );
  const baseResult = createBasePrefeedModel(currentAthlete, currentActivity);
  const context: PrefeedBuildContextModel = {
    currentActivity,
    currentAthlete: currentAthleteWithAppliedConfig,
    otherAthletes: otherAthletesWithAppliedConfig,
    currentAthleteOtherActivitiesSorted: currentAthleteWithAppliedConfig.activities
      .filter(a => a.activityId !== currentActivity.activityId)
      .sort((a, b) => new Date(a.createdDate).getTime() - new Date(b.createdDate).getTime()),
    otherAthleteActivitiesSorted: otherAthletesWithAppliedConfig
      .flatMap(a => a.activities)
      .sort((a, b) => new Date(a.createdDate).getTime() - new Date(b.createdDate).getTime()),
  };
  const result = buildFns.reduce((previous, current) => current(previous, context), baseResult);
  return result;
}

function createBasePrefeedModel(
  athlete: PrefeedRequestAthleteModel,
  activity: PrefeedRequestActivityModel,
): ActivityAddedPrefeedModel {
  return {
    id: crypto.randomUUID(),
    type: 'ActivityAdded',
    athlete: {
      athleteId: athlete.athleteId,
      athleteName: athlete.athleteName,
      sex: athlete.sex,
    },
    highlights: [],
    activity,
    created: new Date().toISOString(),
    athleteTokens: [
      {
        id: 0,
        athleteId: athlete.athleteId,
        tokenText: `[ATHLETE_0](${athlete.sex === 'M' ? 'he/his' : athlete.sex === 'F' ? 'she/her' : 'they/them'})`,
        athleteName: athlete.athleteName,
      },
    ],
  };
}

function applyConfigForAthlete(
  athlete: PrefeedRequestAthleteModel,
  config: ActivityAddedPrefeedConfigModel,
  currentActivity: PrefeedRequestActivityModel,
): PrefeedRequestAthleteModel {
  const keepHistory = config?.keepHistory ?? false;

  return {
    ...athlete,
    activities: keepHistory ? athlete.activities.filter(a => a.endDate <= currentActivity.endDate) : athlete.activities,
  };
}
