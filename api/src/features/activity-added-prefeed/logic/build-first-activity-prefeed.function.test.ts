import { PrefeedBuildContextModel } from '../models';
import { ActivityAddedPrefeedModel } from '../models';
import { PrefeedRequestActivityModelBuilder } from '../test/prefeed-request-activity-model.builder';
import { PrefeedRequestAthleteModelBuilder } from '../test/prefeed-request-athlete-model.builder';
import { buildFirstActivityPrefeed } from './build-first-activity-prefeed.function';

describe(buildFirstActivityPrefeed.name, () => {
  const athleteBuilder = new PrefeedRequestAthleteModelBuilder();
  const activityBuilder = new PrefeedRequestActivityModelBuilder();
  let buildResult: ActivityAddedPrefeedModel;

  beforeEach(() => {
    buildResult = {
      id: '',
      type: 'ActivityAdded',
      athlete: null,
      activity: null,
      highlights: [],
      created: new Date().toISOString(),
      athleteTokens: [],
    };
  });

  it('should not contains FirstActivity type, when activity is not first', () => {
    const currentActivity = activityBuilder.build();
    const activity2 = activityBuilder.build();
    const currentAthlete = athleteBuilder.withActivities([activity2, currentActivity]).build();
    const otherAthleteActivity = activityBuilder.build();
    const athlete2 = athleteBuilder.withActivities([otherAthleteActivity]).build();
    const context: PrefeedBuildContextModel = {
      currentActivity,
      currentAthleteOtherActivitiesSorted: [activity2],
      currentAthlete,
      otherAthleteActivitiesSorted: [],
      otherAthletes: [athlete2],
    };

    const result = buildFirstActivityPrefeed(buildResult, context);

    expect(result.highlights).toHaveLength(0);
  });

  it('should contains FirstActivity type, when activity is first', () => {
    const currentActivity = activityBuilder.build();
    const currentAthlete = athleteBuilder.withActivities([currentActivity]).build();
    const context: PrefeedBuildContextModel = {
      currentActivity,
      currentAthleteOtherActivitiesSorted: [],
      currentAthlete,
      otherAthleteActivitiesSorted: [],
      otherAthletes: [],
    };

    const result = buildFirstActivityPrefeed(buildResult, context);

    expect(result.highlights).toHaveLength(1);
    expect(result.highlights[0].highlight).toBe('FirstActivity');
  });

  it("should contains FirstActivity type, when activity is first (should ignore other athlete's activities", () => {
    const currentActivity = activityBuilder.build();
    const currentAthlete = athleteBuilder.withActivities([currentActivity]).build();
    const otherAthleteActivity = activityBuilder.build();
    const athlete2 = athleteBuilder.withActivities([otherAthleteActivity]).build();
    const context: PrefeedBuildContextModel = {
      currentActivity,
      currentAthleteOtherActivitiesSorted: [],
      currentAthlete,
      otherAthleteActivitiesSorted: [otherAthleteActivity],
      otherAthletes: [athlete2],
    };

    const result = buildFirstActivityPrefeed(buildResult, context);

    expect(result.highlights).toHaveLength(1);
    expect(result.highlights[0].highlight).toBe('FirstActivity');
  });

  it('should copy previous types', () => {
    buildResult.highlights = [{ highlight: 'FirstActivity' }];
    const currentActivity = activityBuilder.build();
    const currentAthlete = athleteBuilder.withActivities([currentActivity]).build();
    const context: PrefeedBuildContextModel = {
      currentActivity,
      currentAthleteOtherActivitiesSorted: [],
      currentAthlete,
      otherAthleteActivitiesSorted: [],
      otherAthletes: [],
    };

    const result = buildFirstActivityPrefeed(buildResult, context);

    expect(result.highlights).toHaveLength(2);
  });
});
