import { PrefeedBuildContextModel } from '../models';
import { ActivityAddedPrefeedModel } from '../models';
import { appendAthleteTokens } from './append-athlete-tokens.function';

/**
 * Finds if activity beat current challenge points for one activity record.
 * @param result current result of add activity prefeed flow
 * @param context data from challenge aggregated into helpful fields.
 * @returns result with additional highlight if given activity beat current record
 */
export function buildGlobalRecordPointsPrefeed(
  result: ActivityAddedPrefeedModel,
  context: PrefeedBuildContextModel,
): ActivityAddedPrefeedModel {
  const otherActivities = [
    ...context.otherAthleteActivitiesSorted,
    ...context.currentAthleteOtherActivitiesSorted,
  ].sort((a, b) => new Date(a.createdDate).getTime() - new Date(b.createdDate).getTime());
  if (otherActivities.length === 0) {
    return result;
  }

  const currentGlobalRecordActivity = otherActivities.reduce(
    (p, c) => (p == null ? c : c.points > p.points ? c : p),
    null,
  );
  const currentRecordAthleteHolder = [...context.otherAthletes, context.currentAthlete].find(a =>
    a.activities.some(a => a.activityId === currentGlobalRecordActivity.activityId),
  );
  if (currentGlobalRecordActivity.points >= context.currentActivity.points) {
    return result;
  }

  return {
    ...result,
    highlights: [
      ...result.highlights,
      {
        highlight: 'GlobalRecordPoints',
        previousRecordInPoints: currentGlobalRecordActivity.points,
        previousAthlete: {
          athleteId: currentRecordAthleteHolder.athleteId,
          athleteName: currentRecordAthleteHolder.athleteName,
          sex: currentRecordAthleteHolder.sex,
        },
      },
    ],
    athleteTokens: appendAthleteTokens(result.athleteTokens, currentRecordAthleteHolder),
  };
}
