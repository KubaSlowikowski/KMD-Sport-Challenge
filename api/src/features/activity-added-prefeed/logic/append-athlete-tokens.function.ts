import { ReferenceAthleteModel } from 'src/features/prefeed/models';
import { AthleteTokenModel } from '../models/athlete-token.model';

export function appendAthleteTokens(
  athleteTokens: AthleteTokenModel[],
  ...athletes: ReferenceAthleteModel[]
): AthleteTokenModel[] {
  const maxTokenId = Math.max(...athleteTokens.flatMap(a => a.id));
  return [
    ...athleteTokens,
    ...athletes
      .filter(a => !athleteTokens.some(t => t.athleteId === a.athleteId))
      .map((a, index) => ({
        id: maxTokenId + index + 1,
        tokenText: `[ATHLETE_${maxTokenId + index + 1}](${a.sex === 'M' ? 'he/his' : a.sex === 'F' ? 'she/her' : 'they/them'})`,
        athleteId: a.athleteId,
        athleteName: a.athleteName,
      })),
  ];
}
