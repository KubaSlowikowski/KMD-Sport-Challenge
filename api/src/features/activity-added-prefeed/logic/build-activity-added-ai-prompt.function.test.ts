import { ReferenceAthleteModel } from '../../prefeed/models';
import { ActivityAddedPrefeedModel } from '../models';
import { buildActivityAddedAiPrompt } from './build-activity-added-ai-prompt.function';

const ATHLETE_NAME = 'John Doe';
const POINTS_FOR_ACTIVITY = 100;
const BASE_REQUEST_MODEL: ActivityAddedPrefeedModel = {
  id: '',
  type: 'ActivityAdded',
  highlights: [],
  activity: {
    activityId: -1,
    sportType: 'Ride',
    supported: true,
    points: POINTS_FOR_ACTIVITY,
    createdDate: new Date().toISOString(),
    endDate: new Date().toISOString(),
  },
  athlete: {
    athleteId: -1,
    athleteName: ATHLETE_NAME,
    sex: 'M',
  },
  created: new Date().toISOString(),
  athleteTokens: [{ athleteId: -1, tokenText: '-1', id: 0, athleteName: 'John Doe' }],
};

describe(buildActivityAddedAiPrompt.name, () => {
  it('should throw error when no type is given', () => {
    const fn = () => buildActivityAddedAiPrompt(BASE_REQUEST_MODEL, 'Funny');

    // expect(fn).toThrow('No highlights are given. To build ai prompt at least one type is required.');
  });

  it('should generate system prompt', () => {
    const result = buildActivityAddedAiPrompt(
      {
        ...BASE_REQUEST_MODEL,
        highlights: [{ highlight: 'FirstActivity' }],
      },
      'Funny',
    );

    // expect(result.system).toBeDefined();
  });

  it('should prepend prompt with funny mode', () => {
    const result = buildActivityAddedAiPrompt(
      {
        ...BASE_REQUEST_MODEL,
        highlights: [{ highlight: 'FirstActivity' }],
      },
      'Funny',
    );

    // expect(result.user).toContain('Generate in funny mode:');
  });

  it('should prepend prompt with motivation mode', () => {
    const result = buildActivityAddedAiPrompt(
      {
        ...BASE_REQUEST_MODEL,
        highlights: [{ highlight: 'FirstActivity' }],
      },
      'Motivation',
    );

    // expect(result.user).toContain('Generate in motivation talk mode: ');
  });

  it('should throw error when no type is given', () => {
    const fn = () =>
      buildActivityAddedAiPrompt(
        {
          ...BASE_REQUEST_MODEL,
          highlights: [{ highlight: 'FirstActivity' }],
        },
        'notexistingmode' as never,
      );

    expect(fn).toThrow('Not mapped');
  });

  it('should generate prompt for workout first activity', () => {
    const SPORT_TYPE = 'Workout';

    const result = buildActivityAddedAiPrompt(
      {
        ...BASE_REQUEST_MODEL,
        highlights: [{ highlight: 'FirstActivity' }],
        activity: {
          ...BASE_REQUEST_MODEL.activity,
          sportType: SPORT_TYPE,
        },
      },
      'Funny',
    );
  });

  it('should generate prompt for personal record activity', () => {
    const PREVIOUS_RECORD = 50;
    const SPORT_TYPE = 'Workout';

    const result = buildActivityAddedAiPrompt(
      {
        ...BASE_REQUEST_MODEL,
        highlights: [
          {
            highlight: 'PersonalRecordPoints',
            previousRecordInPoints: PREVIOUS_RECORD,
          },
        ],
        activity: {
          ...BASE_REQUEST_MODEL.activity,
          sportType: SPORT_TYPE,
        },
      },
      'Funny',
    );
  });

  it('should generate prompt for personal record activity', () => {
    const PREVIOUS_RECORD = 50;
    const SPORT_TYPE = 'Workout';
    const PREVIOUS_ATHLETE: ReferenceAthleteModel = {
      athleteId: -1,
      athleteName: 'Biblio Baggins',
      sex: 'M',
    };

    const result = buildActivityAddedAiPrompt(
      {
        ...BASE_REQUEST_MODEL,
        highlights: [
          {
            highlight: 'GlobalRecordPoints',
            previousRecordInPoints: PREVIOUS_RECORD,
            previousAthlete: PREVIOUS_ATHLETE,
          },
        ],
        activity: {
          ...BASE_REQUEST_MODEL.activity,
          sportType: SPORT_TYPE,
        },
      },
      'Funny',
    );
  });

  it('should generate prompt for ranking advancement', () => {
    const PREVIOUS_RECORD = 50;
    const SPORT_TYPE = 'Workout';
    const ATHLETE_1: ReferenceAthleteModel = {
      athleteId: 2,
      athleteName: 'Biblio Baggins',
      sex: 'M',
    };
    const ATHLETE_2: ReferenceAthleteModel = {
      athleteId: 3,
      athleteName: 'Boromir Gondor',
      sex: 'M',
    };

    const result = buildActivityAddedAiPrompt(
      {
        ...BASE_REQUEST_MODEL,
        highlights: [
          {
            highlight: 'RankingAdvancement',
            fromPosition: 5,
            toPosition: 3,
            athletesBeaten: [ATHLETE_1, ATHLETE_2],
          },
        ],
        activity: {
          ...BASE_REQUEST_MODEL.activity,
          sportType: SPORT_TYPE,
        },
        athleteTokens: [
          ...BASE_REQUEST_MODEL.athleteTokens,
          { id: 1, athleteId: 2, tokenText: '2', athleteName: 'John Doe' },
          { id: 2, athleteId: 3, tokenText: '3', athleteName: 'John Doe' },
        ],
      },
      'Funny',
    );

    console.log(result);
  });
});
