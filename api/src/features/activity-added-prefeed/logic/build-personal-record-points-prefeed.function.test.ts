import { PrefeedBuildContextModel } from '../models';
import { ActivityAddedPrefeedModel, PrefeedHighlightPersonalRecordPointsModel } from '../models';
import { PrefeedRequestActivityModelBuilder } from '../test/prefeed-request-activity-model.builder';
import { PrefeedRequestAthleteModelBuilder } from '../test/prefeed-request-athlete-model.builder';
import { buildPersonalRecordPointsPrefeed } from './build-personal-record-points-prefeed.function';

describe(buildPersonalRecordPointsPrefeed.name, () => {
  const athleteBuilder = new PrefeedRequestAthleteModelBuilder();
  const activityBuilder = new PrefeedRequestActivityModelBuilder();
  let buildResult: ActivityAddedPrefeedModel;

  beforeEach(() => {
    buildResult = {
      id: '',
      type: 'ActivityAdded',
      athlete: null,
      activity: null,
      highlights: [],
      created: new Date().toISOString(),
      athleteTokens: [],
    };
  });

  it('should not contains PersonalRecordPoints type, when activity is first', () => {
    const currentActivity = activityBuilder.build();
    const currentAthlete = athleteBuilder.withActivities([currentActivity]).build();
    const context: PrefeedBuildContextModel = {
      currentActivity,
      currentAthleteOtherActivitiesSorted: [],
      currentAthlete,
      otherAthleteActivitiesSorted: [],
      otherAthletes: [],
    };

    const result = buildPersonalRecordPointsPrefeed(buildResult, context);

    expect(result.highlights).toHaveLength(0);
  });

  it('should contains PersonalRecordPoints type, when activity is best', () => {
    const currentActivity = activityBuilder.withPoints(40).build();
    const previousRecord = activityBuilder.withPoints(10).build();
    const currentAthlete = athleteBuilder.withActivities([previousRecord, currentActivity]).build();
    const context: PrefeedBuildContextModel = {
      currentActivity,
      currentAthleteOtherActivitiesSorted: [previousRecord],
      currentAthlete,
      otherAthleteActivitiesSorted: [],
      otherAthletes: [],
    };

    const result = buildPersonalRecordPointsPrefeed(buildResult, context);

    expect(result.highlights).toHaveLength(1);
    expect(result.highlights[0].highlight).toBe('PersonalRecordPoints');
  });

  it('should not contains PersonalRecordPoints type, when activity has same amount of points', () => {
    const currentActivity = activityBuilder.withPoints(40).build();
    const previousRecord = activityBuilder.withPoints(40).build();
    const currentAthlete = athleteBuilder.withActivities([previousRecord, currentActivity]).build();
    const context: PrefeedBuildContextModel = {
      currentActivity,
      currentAthleteOtherActivitiesSorted: [previousRecord],
      currentAthlete,
      otherAthleteActivitiesSorted: [],
      otherAthletes: [],
    };

    const result = buildPersonalRecordPointsPrefeed(buildResult, context);

    expect(result.highlights).toHaveLength(0);
  });

  it('fromPoints should contains previous record', () => {
    const PREVIOUS_RECORD_POINTS = 40;
    const currentActivity = activityBuilder.withPoints(50).build();
    const previousRecord = activityBuilder.withPoints(PREVIOUS_RECORD_POINTS).build();
    const activity2 = activityBuilder.withPoints(30).build();
    const activity3 = activityBuilder.withPoints(30).build();
    const currentAthlete = athleteBuilder
      .withActivities([activity3, previousRecord, activity2, currentActivity])
      .build();
    const context: PrefeedBuildContextModel = {
      currentActivity,
      currentAthleteOtherActivitiesSorted: [activity3, previousRecord, activity2],
      currentAthlete,
      otherAthleteActivitiesSorted: [],
      otherAthletes: [],
    };

    const result = buildPersonalRecordPointsPrefeed(buildResult, context);

    const type = result.highlights[0] as PrefeedHighlightPersonalRecordPointsModel;
    expect(type.previousRecordInPoints).toBe(PREVIOUS_RECORD_POINTS);
  });
});
