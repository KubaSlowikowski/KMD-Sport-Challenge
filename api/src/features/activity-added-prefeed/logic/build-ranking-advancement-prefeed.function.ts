import { PrefeedBuildContextModel } from '../models';
import { ActivityAddedPrefeedModel, PrefeedRequestActivityModel, PrefeedRequestAthleteModel } from '../models';
import { appendAthleteTokens } from './append-athlete-tokens.function';

// TODO: add some snapshot for ranking
/**
 * Finds if thank's to that activity, athlete advanced in ranking.
 * @param result current result of add activity prefeed flow
 * @param context data from challenge aggregated into helpful fields.
 * @returns result with additional highlight if athlete advanced in ranking to higher position.
 */
export function buildRankingAdvancementPrefeed(
  result: ActivityAddedPrefeedModel,
  context: PrefeedBuildContextModel,
): ActivityAddedPrefeedModel {
  const allAthletes = [context.currentAthlete, ...context.otherAthletes];
  const currentRanking = getRanking(allAthletes);
  const previousRanking = getRanking(allAthletes, a => a.createdDate < context.currentActivity.createdDate);
  const currentPosition = currentRanking.findIndex(c => c.athleteId === context.currentAthlete.athleteId);
  const previousPosition = previousRanking.findIndex(c => c.athleteId === context.currentAthlete.athleteId);

  if (currentPosition >= previousPosition) {
    return result;
  }

  const currentAthletePreviousPoints = previousRanking.find(
    c => c.athleteId === context.currentAthlete.athleteId,
  ).points;
  const currentAthleteCurrentPoints = currentRanking.find(c => c.athleteId === context.currentAthlete.athleteId).points;
  const previousAthletesWithHigherPositions = previousRanking
    .filter(r => r.points > currentAthletePreviousPoints)
    .map(a => ({
      athleteId: a.athleteId,
      athleteName: a.athleteName,
      sex: a.sex,
      previousPoints: a.points,
      currentPoints: currentRanking.find(r => r.athleteId === a.athleteId).points,
    }));
  const athletesBeaten = previousAthletesWithHigherPositions
    .filter(p => p.currentPoints < currentAthleteCurrentPoints)
    .sort((a, b) => b.currentPoints - a.currentPoints)
    .map(a => ({
      athleteName: a.athleteName,
      athleteId: a.athleteId,
      sex: a.sex,
    }));

  return {
    ...result,
    highlights: [
      ...result.highlights,
      {
        highlight: 'RankingAdvancement',
        fromPosition: previousPosition,
        toPosition: currentPosition,
        athletesBeaten,
      },
    ],
    athleteTokens: appendAthleteTokens(result.athleteTokens, ...athletesBeaten),
  };
}

function getRanking(
  athletes: PrefeedRequestAthleteModel[],
  activityCondition: (activity: PrefeedRequestActivityModel) => boolean = () => true,
): {
  athleteId: number;
  athleteName: string;
  sex: 'M' | 'F';
  points: number;
}[] {
  return athletes
    .map(a => ({
      athleteId: a.athleteId,
      athleteName: a.athleteName,
      sex: a.sex,
      points: a.activities.filter(a => activityCondition(a)).reduce((p, c) => p + c.points, 0),
    }))
    .sort((a, b) => b.points - a.points);
}
