import { PrefeedBuildContextModel } from '../models';
import { ActivityAddedPrefeedModel } from '../models';
import { PrefeedHighlightGlobalRecordPointsModel } from '../models/highlights/prefeed-highlight-global-record-points.model';
import { PrefeedRequestActivityModelBuilder } from '../test/prefeed-request-activity-model.builder';
import { PrefeedRequestAthleteModelBuilder } from '../test/prefeed-request-athlete-model.builder';
import { buildGlobalRecordPointsPrefeed } from './build-global-record-points-prefeed.function';

describe(buildGlobalRecordPointsPrefeed.name, () => {
  const athleteBuilder = new PrefeedRequestAthleteModelBuilder();
  const activityBuilder = new PrefeedRequestActivityModelBuilder();
  let buildResult: ActivityAddedPrefeedModel;

  beforeEach(() => {
    buildResult = {
      id: '',
      type: 'ActivityAdded',
      athlete: null,
      activity: null,
      highlights: [],
      created: new Date().toISOString(),
      athleteTokens: [],
    };
  });

  it('should not contains GlobalRecordPoints type, when activity is first', () => {
    const currentActivity = activityBuilder.withActivityId(1).build();
    const currentAthlete = athleteBuilder.withAthleteId(100).withActivities([currentActivity]).build();
    const context: PrefeedBuildContextModel = {
      currentActivity,
      currentAthleteOtherActivitiesSorted: [],
      currentAthlete,
      otherAthleteActivitiesSorted: [],
      otherAthletes: [],
    };

    const result = buildGlobalRecordPointsPrefeed(buildResult, context);

    expect(result.highlights).toHaveLength(0);
  });

  it('should not contains GlobalRecordPoints type, when record has not been beaten', () => {
    const currentActivity = activityBuilder.withActivityId(1).withPoints(30).build();
    const previousActivity = activityBuilder.withActivityId(2).withPoints(40).build();
    const previousActivity2 = activityBuilder.withActivityId(3).withPoints(20).build();
    const currentAthlete = athleteBuilder
      .withAthleteId(100)
      .withActivities([previousActivity2, currentActivity, previousActivity])
      .build();
    const context: PrefeedBuildContextModel = {
      currentActivity,
      currentAthleteOtherActivitiesSorted: [previousActivity, previousActivity2],
      currentAthlete,
      otherAthleteActivitiesSorted: [],
      otherAthletes: [],
    };

    const result = buildGlobalRecordPointsPrefeed(buildResult, context);

    expect(result.highlights).toHaveLength(0);
  });

  it("should not contains GlobalRecordPoints type, when other's athlete record has not been beaten", () => {
    const currentActivity = activityBuilder.withActivityId(1).withPoints(30).build();
    const previousActivity = activityBuilder.withActivityId(2).withPoints(40).build();
    const previousActivity2 = activityBuilder.withActivityId(3).withPoints(20).build();
    const currentAthlete = athleteBuilder.withAthleteId(100).withActivities([currentActivity]).build();
    const athlete2 = athleteBuilder.withAthleteId(100).withActivities([previousActivity, previousActivity2]).build();
    const context: PrefeedBuildContextModel = {
      currentActivity,
      currentAthleteOtherActivitiesSorted: [],
      currentAthlete,
      otherAthleteActivitiesSorted: [previousActivity, previousActivity2],
      otherAthletes: [athlete2],
    };

    const result = buildGlobalRecordPointsPrefeed(buildResult, context);

    expect(result.highlights).toHaveLength(0);
  });

  it("should not contains GlobalRecordPoints type, when other's athlete record has not been beaten (different activity order)", () => {
    const currentActivity = activityBuilder.withActivityId(1).withPoints(30).build();
    const previousActivity = activityBuilder.withActivityId(2).withPoints(40).build();
    const previousActivity2 = activityBuilder.withActivityId(3).withPoints(20).build();
    const currentAthlete = athleteBuilder.withAthleteId(100).withActivities([currentActivity]).build();
    const athlete2 = athleteBuilder.withAthleteId(100).withActivities([previousActivity2, previousActivity]).build();
    const context: PrefeedBuildContextModel = {
      currentActivity,
      currentAthleteOtherActivitiesSorted: [],
      currentAthlete,
      otherAthleteActivitiesSorted: [previousActivity2, previousActivity],
      otherAthletes: [athlete2],
    };

    const result = buildGlobalRecordPointsPrefeed(buildResult, context);

    expect(result.highlights).toHaveLength(0);
  });

  it('should contains GlobalRecordPoints type, when athlete beat his own record', () => {
    const currentActivity = activityBuilder.withActivityId(1).withPoints(50).build();
    const previousActivity = activityBuilder.withActivityId(2).withPoints(40).build();
    const currentAthlete = athleteBuilder
      .withAthleteId(100)
      .withActivities([previousActivity, currentActivity])
      .build();
    const context: PrefeedBuildContextModel = {
      currentActivity,
      currentAthleteOtherActivitiesSorted: [previousActivity],
      currentAthlete,
      otherAthleteActivitiesSorted: [],
      otherAthletes: [],
    };

    const result = buildGlobalRecordPointsPrefeed(buildResult, context);

    expect(result.highlights).toHaveLength(1);
    expect(result.highlights[0].highlight).toBe('GlobalRecordPoints');
  });

  it("should contains GlobalRecordPoints type, when athlete beat other athlete's record", () => {
    const currentActivity = activityBuilder.withActivityId(1).withPoints(50).build();
    const previousActivity = activityBuilder.withActivityId(2).withPoints(40).build();
    const currentAthlete = athleteBuilder.withAthleteId(100).withActivities([currentActivity]).build();
    const athlete2 = athleteBuilder.withAthleteId(101).withActivities([previousActivity]).build();
    const context: PrefeedBuildContextModel = {
      currentActivity,
      currentAthleteOtherActivitiesSorted: [],
      currentAthlete,
      otherAthleteActivitiesSorted: [previousActivity],
      otherAthletes: [athlete2],
    };

    const result = buildGlobalRecordPointsPrefeed(buildResult, context);

    expect(result.highlights).toHaveLength(1);
    expect(result.highlights[0].highlight).toBe('GlobalRecordPoints');
  });

  it('should set previousRecord to previous record', () => {
    const PREVIOUS_RECORD_POINTS = 40;
    const currentActivity = activityBuilder.withActivityId(1).withPoints(50).build();
    const previousActivity = activityBuilder.withActivityId(2).withPoints(PREVIOUS_RECORD_POINTS).build();
    const currentAthlete = athleteBuilder.withAthleteId(100).withActivities([currentActivity]).build();
    const athlete2 = athleteBuilder.withAthleteId(101).withActivities([previousActivity]).build();
    const context: PrefeedBuildContextModel = {
      currentActivity,
      currentAthleteOtherActivitiesSorted: [],
      currentAthlete,
      otherAthleteActivitiesSorted: [previousActivity],
      otherAthletes: [athlete2],
    };

    const result = buildGlobalRecordPointsPrefeed(buildResult, context);

    const type = result.highlights[0] as PrefeedHighlightGlobalRecordPointsModel;
    expect(type.previousRecordInPoints).toBe(PREVIOUS_RECORD_POINTS);
  });

  it('should set previousAthlete to athlete who has previous record', () => {
    const PREVIOUS_ATHLETE_ACTIVITY_ID = 101;
    const PREVIOUS_ATHLETE_NAME = 'John';
    const currentActivity = activityBuilder.withActivityId(1).withPoints(50).build();
    const previousActivity = activityBuilder.withActivityId(2).withPoints(40).build();
    const currentAthlete = athleteBuilder.withAthleteId(100).withActivities([currentActivity]).build();
    const athlete2 = athleteBuilder
      .withAthleteId(PREVIOUS_ATHLETE_ACTIVITY_ID)
      .withAthleteName(PREVIOUS_ATHLETE_NAME)
      .withActivities([previousActivity])
      .build();
    const context: PrefeedBuildContextModel = {
      currentActivity,
      currentAthleteOtherActivitiesSorted: [],
      currentAthlete,
      otherAthleteActivitiesSorted: [previousActivity],
      otherAthletes: [athlete2],
    };

    const result = buildGlobalRecordPointsPrefeed(buildResult, context);

    const type = result.highlights[0] as PrefeedHighlightGlobalRecordPointsModel;
    expect(type.previousAthlete).toBeDefined();
    expect(type.previousAthlete.athleteId).toBe(PREVIOUS_ATHLETE_ACTIVITY_ID);
    expect(type.previousAthlete.athleteName).toBe(PREVIOUS_ATHLETE_NAME);
  });

  it('should set previousAthlete to current athlete if he beat his own record', () => {
    const CURRENT_ATHLETE_ACTIVITY_ID = 101;
    const CURRENT_ATHLETE_NAME = 'John';
    const currentActivity = activityBuilder.withActivityId(1).withPoints(50).build();
    const previousActivity = activityBuilder.withActivityId(2).withPoints(40).build();
    const currentAthlete = athleteBuilder
      .withAthleteId(CURRENT_ATHLETE_ACTIVITY_ID)
      .withAthleteName(CURRENT_ATHLETE_NAME)
      .withActivities([currentActivity, previousActivity])
      .build();
    const context: PrefeedBuildContextModel = {
      currentActivity,
      currentAthleteOtherActivitiesSorted: [previousActivity],
      currentAthlete,
      otherAthleteActivitiesSorted: [],
      otherAthletes: [],
    };

    const result = buildGlobalRecordPointsPrefeed(buildResult, context);

    const type = result.highlights[0] as PrefeedHighlightGlobalRecordPointsModel;
    expect(type.previousAthlete).toBeDefined();
    expect(type.previousAthlete.athleteId).toBe(CURRENT_ATHLETE_ACTIVITY_ID);
    expect(type.previousAthlete.athleteName).toBe(CURRENT_ATHLETE_NAME);
  });
});
