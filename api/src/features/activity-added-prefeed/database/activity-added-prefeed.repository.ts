import { PrefeedRepository } from '../.././../features/prefeed/database';
import { ActivityAddedPrefeedModel } from '../models';
import { dbClient } from '../../../db/dynamoDB/dynamoDb';

export class ActivityAddedPrefeedRepository extends PrefeedRepository<ActivityAddedPrefeedModel> {
  protected parse(prefeed: ActivityAddedPrefeedModel): unknown {
    return {
      ...prefeed,
      created: prefeed.created,
      activity:
        prefeed.activity != null
          ? {
              ...prefeed.activity,
              createdDate: prefeed.activity.createdDate,
              endDate: prefeed.activity.endDate,
            }
          : null,
    };
  }
}

export const activityAddedPrefeedRepository: ActivityAddedPrefeedRepository = new ActivityAddedPrefeedRepository(
  dbClient,
);
