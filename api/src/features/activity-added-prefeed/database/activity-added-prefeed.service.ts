import { ActivityAddedPrefeedModel } from '../models';
import { ActivityAddedPrefeedRepository, activityAddedPrefeedRepository } from './activity-added-prefeed.repository';

export class ActivityAddedPrefeedService {
  private readonly repository: ActivityAddedPrefeedRepository;

  constructor(repository: ActivityAddedPrefeedRepository) {
    this.repository = repository;
  }

  async save(prefeedModel: ActivityAddedPrefeedModel): Promise<boolean> {
    if (!prefeedModel) throw new Error(`save(): Given 'prefeedModel' is null or undefined.`);

    return this.repository.save(prefeedModel);
  }
}

export const activityAddedPrefeedService: ActivityAddedPrefeedService = new ActivityAddedPrefeedService(
  activityAddedPrefeedRepository,
);
