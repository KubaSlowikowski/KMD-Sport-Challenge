import { ActivityType } from '../../../strava/types/activity.type';
import { PrefeedRequestActivityModel } from '../models';

export class PrefeedRequestActivityModelBuilder {
  constructor(
    private currentFns: ((model: PrefeedRequestActivityModel) => PrefeedRequestActivityModel)[] = [],
    private model: PrefeedRequestActivityModel | null = null,
  ) {}

  public updateModel(model: PrefeedRequestActivityModel): PrefeedRequestActivityModelBuilder {
    return new PrefeedRequestActivityModelBuilder([], model);
  }

  public build(): PrefeedRequestActivityModel {
    const model: PrefeedRequestActivityModel = this.model ?? this.createDefaultModel();
    return this.currentFns.reduce((model, fn) => fn(model), model);
  }

  public withActivityId(activityId: number): PrefeedRequestActivityModelBuilder {
    const fns: ((model: PrefeedRequestActivityModel) => PrefeedRequestActivityModel)[] = [
      ...this.currentFns,
      model => ({
        ...model,
        activityId,
      }),
    ];
    return new PrefeedRequestActivityModelBuilder(fns, this.model);
  }

  public withSportType(sportType: ActivityType): PrefeedRequestActivityModelBuilder {
    const fns: ((model: PrefeedRequestActivityModel) => PrefeedRequestActivityModel)[] = [
      ...this.currentFns,
      model => ({
        ...model,
        sportType,
      }),
    ];
    return new PrefeedRequestActivityModelBuilder(fns, this.model);
  }

  public isSupported(supported: boolean): PrefeedRequestActivityModelBuilder {
    const fns: ((model: PrefeedRequestActivityModel) => PrefeedRequestActivityModel)[] = [
      ...this.currentFns,
      model => ({
        ...model,
        supported,
      }),
    ];
    return new PrefeedRequestActivityModelBuilder(fns, this.model);
  }

  public withPoints(points: number): PrefeedRequestActivityModelBuilder {
    const fns: ((model: PrefeedRequestActivityModel) => PrefeedRequestActivityModel)[] = [
      ...this.currentFns,
      model => ({
        ...model,
        points,
      }),
    ];
    return new PrefeedRequestActivityModelBuilder(fns, this.model);
  }

  public createdAt(createdDate: string): PrefeedRequestActivityModelBuilder {
    const fns: ((model: PrefeedRequestActivityModel) => PrefeedRequestActivityModel)[] = [
      ...this.currentFns,
      model => ({
        ...model,
        createdDate,
      }),
    ];
    return new PrefeedRequestActivityModelBuilder(fns, this.model);
  }

  public endedAt(endDate: string): PrefeedRequestActivityModelBuilder {
    const fns: ((model: PrefeedRequestActivityModel) => PrefeedRequestActivityModel)[] = [
      ...this.currentFns,
      model => ({
        ...model,
        endDate,
      }),
    ];
    return new PrefeedRequestActivityModelBuilder(fns, this.model);
  }

  private createDefaultModel(): PrefeedRequestActivityModel {
    return {
      activityId: 1,
      sportType: 'Ride',
      supported: true,
      points: 300,
      createdDate: new Date(2024, 5, 22).toISOString(),
      endDate: new Date(2024, 5, 23).toISOString(),
    };
  }
}
