import { PrefeedRequestActivityModel, PrefeedRequestAthleteModel } from '../models';

export class PrefeedRequestAthleteModelBuilder {
  constructor(
    private currentFns: ((model: PrefeedRequestAthleteModel) => PrefeedRequestAthleteModel)[] = [],
    private model: PrefeedRequestAthleteModel | null = null,
  ) {}

  public updateModel(model: PrefeedRequestAthleteModel): PrefeedRequestAthleteModelBuilder {
    return new PrefeedRequestAthleteModelBuilder([], model);
  }

  public build(): PrefeedRequestAthleteModel {
    const model: PrefeedRequestAthleteModel = this.model ?? this.createDefaultModel();
    return this.currentFns.reduce((model, fn) => fn(model), model);
  }

  public withNonActivities(): PrefeedRequestAthleteModelBuilder {
    return this.withActivities([]);
  }

  public withAthleteId(athleteId: number): PrefeedRequestAthleteModelBuilder {
    const fns: ((model: PrefeedRequestAthleteModel) => PrefeedRequestAthleteModel)[] = [
      ...this.currentFns,
      model => ({
        ...model,
        athleteId,
      }),
    ];
    return new PrefeedRequestAthleteModelBuilder(fns, this.model);
  }

  public withAthleteName(athleteName: string): PrefeedRequestAthleteModelBuilder {
    const fns: ((model: PrefeedRequestAthleteModel) => PrefeedRequestAthleteModel)[] = [
      ...this.currentFns,
      model => ({
        ...model,
        athleteName,
      }),
    ];
    return new PrefeedRequestAthleteModelBuilder(fns, this.model);
  }

  public withActivities(activities: PrefeedRequestActivityModel[]): PrefeedRequestAthleteModelBuilder {
    const fns: ((model: PrefeedRequestAthleteModel) => PrefeedRequestAthleteModel)[] = [
      ...this.currentFns,
      model => ({
        ...model,
        activities,
      }),
    ];
    return new PrefeedRequestAthleteModelBuilder(fns, this.model);
  }

  private createDefaultModel(): PrefeedRequestAthleteModel {
    return {
      athleteId: 2,
      athleteName: 'Bilbo Baggins',
      activities: [],
      sex: 'M',
    };
  }
}
