import { EventStatus, StravaEventItem } from '../models/domain/StravaWebhookEvents';
import { stravaEventsProcessorFunctionName } from './stravaEventsProcessor';
import { APPLICATION_NAME_PREFIX, sendJsonError, sendJsonResponse } from '../utils/awsUtils';
import Lambda from 'aws-sdk/clients/lambda';
import { Context, Handler } from 'aws-lambda';
import { stravaWebhookEventService } from '../services/stravaWebhookEventService';

const lambda = new Lambda();

/**
 * AWS Lambda that processes all the strava-webhook-events that have status 'failed' or 'not processed'.
 * For each event that needs to be processed, a 'stravaEventsProcessor' AWS Lambda function is invoked.
 *
 * Deprecated events are being filtered out and marked as 'skipped'.
 */
export const processEventsHandler: Handler = async (event: any, context: Context) => {
  console.log('Strava events scheduled processor started.');

  const events: StravaEventItem[] = await stravaWebhookEventService.getFailedOrNotProcessed();
  const eventsToProcess: StravaEventItem[] = stravaWebhookEventService.filterOutDeprecatedEvents(events);
  const eventsToSkip: StravaEventItem[] = events.filter(event => !eventsToProcess.some(etp => etp.id === event.id));

  // Process events asynchronously.
  const eventsToProcessPromises: Promise<any>[] = eventsToProcess.map(event => processEvent(event));
  const eventsToSkipPromises: Promise<any>[] = eventsToSkip.map(event =>
    stravaWebhookEventService.setEventStatus(
      event.id,
      EventStatus.SKIPPED,
      `Event is deprecated - there are newer events that override this one.`,
    ),
  );

  // Wait for async calls finished.
  const results = await Promise.allSettled([...eventsToProcessPromises, ...eventsToSkipPromises]);

  // Filter out the rejected promises
  const errors = results.filter(result => result.status === 'rejected');

  if (errors.length > 0) {
    const message = 'An error occurred during strava webhooks events processing.';
    console.error(message, errors);
    return sendJsonError(message, 500);
  }

  return sendJsonResponse(204);
};

/**
 * Invokes AWS Lambda that processes StravaEventItem.
 *
 * The function DOES NOT wait for the lambda to complete!!! It only makes a request to AWS Lambda API and wait for the 'invoke' API call to complete.
 * @param event Strava webhooks event document to process.
 */
async function processEvent(event: StravaEventItem) {
  const params = {
    FunctionName: `${APPLICATION_NAME_PREFIX}-${stravaEventsProcessorFunctionName}`,
    InvocationType: 'Event',
    Payload: JSON.stringify({ stravaEventDocument: event }),
  };

  try {
    await lambda.invoke(params).promise();
    console.log(`${stravaEventsProcessorFunctionName} Lambda invocation started for event with _id='${event.id}'.`);
  } catch (error) {
    const message = `Error invoking ${stravaEventsProcessorFunctionName} Lambda.`;
    console.error(message, error);
    return Promise.reject(`${message} Error message: ${error.message}`);
  }
}
