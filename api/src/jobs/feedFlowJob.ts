import { APIGatewayProxyEvent, Context, Handler } from 'aws-lambda';
import { sendJsonError, sendJsonResponse } from '../utils/awsUtils';
import { invokeActivityAddedFeedFlow } from '../features/feed-builder/logic/invoke-activity-added-feed-flow.function';
import { FeedModel } from '../features/feed/models';

export const feedFlowInvokerFunctionName = 'feedFlowInvoker';
interface InvokeFeedFlowLambdaEvent {
  athleteId: number;
  activityId: number;
}

/**
 * AWS Lambda that invokes a feed flow process.
 *
 * @param event Event that contains an athleteId and activityId. Invoked from other AWS Lambda via AWS Lambda API or API Gateway (using HTTP POST request).
 * @param context Context object, which contains information about the invocation, function, and execution environment.
 */
export const feedFlowInvocationHandler: Handler = async (
  event: InvokeFeedFlowLambdaEvent | APIGatewayProxyEvent,
  context: Context,
) => {
  let athleteId: number = null;
  let activityId: number = null;

  try {
    if (isInvokedFromApiGateway(event)) {
      console.log('Received an event from API Gateway.');
      event = event as APIGatewayProxyEvent;

      if (event.body) {
        const body = JSON.parse(event.body);
        athleteId = body.athleteId;
        activityId = body.activityId;
      }
    } else if (isInvokedFromOtherLambda(event)) {
      console.log('Received an event from other AWS Lambda.');
      event = event as InvokeFeedFlowLambdaEvent;

      athleteId = event.athleteId;
      activityId = event.activityId;
    } else {
      const message: string = 'Unknown type of event received.';
      console.log(message);
      return sendJsonError(message, 400);
    }

    if (!athleteId || !activityId) {
      const message: string = `athleteId or activityId is null or undefined.`;
      console.log(message);
      return sendJsonError(message, 400);
    }

    console.log(`Feed flow invocation started for an athlete with id='${athleteId}' and activityId='${activityId}'`);

    const feed: FeedModel = await invokeActivityAddedFeedFlow(athleteId, activityId);

    if (feed) {
      console.log(`Feed created with id='${feed.id}'.`);
    } else {
      console.log('Nothing interesting...');
    }

    return sendJsonResponse(204);
  } catch (error) {
    const message: string = 'An error occurred during feed flow invoking.';
    console.error(message, error);
    console.log('Event: ', event);
    return sendJsonError(message, 500);
  }

  function isInvokedFromApiGateway(event: InvokeFeedFlowLambdaEvent | APIGatewayProxyEvent): boolean {
    return 'version' in event;
  }

  function isInvokedFromOtherLambda(event: InvokeFeedFlowLambdaEvent | APIGatewayProxyEvent): boolean {
    return 'athleteId' in event && 'activityId' in event;
  }
};
