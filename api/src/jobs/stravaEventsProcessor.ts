import { AspectType, EventStatus, ObjectType, StravaEventItem } from '../models/domain/StravaWebhookEvents';
import { Athlete } from '../models/domain/Athlete';
import { fetchActivityById, StravaServiceResponse } from '../services/stravaService';
import { dateTimeStringToTimestamp, getChallengeEndTimestamp, getChallengeStartTimestamp } from '../utils/utils';
import { APPLICATION_NAME_PREFIX, sendJsonError, sendJsonResponse } from '../utils/awsUtils';
import { Context, Handler } from 'aws-lambda';
import Lambda from 'aws-sdk/clients/lambda';
import { feedFlowInvokerFunctionName } from './feedFlowJob';
import { stravaWebhookEventService } from '../services/stravaWebhookEventService';
import { Activity } from '../models/domain/Activity';
import { activityService } from '../services/activityService';
import { athleteService } from '../services/athleteService';

export const stravaEventsProcessorFunctionName = 'stravaEventsProcessor';
const lambda = new Lambda();

/**
 * AWS Lambda that processes StravaEventDocument given in the 'event' input parameter.
 *
 * @param event Strava webhook event document that was saved in DB and needs to be processed.
 * @param context Context object, which contains information about the invocation, function, and execution environment.
 */
export const processEventHandler: Handler = async (event: { stravaEventItem: StravaEventItem }, context: Context) => {
  console.log(`Started processing an event`, event);
  const stravaEvent = event.stravaEventItem;

  try {
    if (stravaEvent.object_type === ObjectType.ACTIVITY) {
      await processActivityEvent(stravaEvent);
    } else if (stravaEvent.object_type === ObjectType.ATHLETE) {
      await processAthleteEvent(stravaEvent);
    }

    console.log(`Finished event processing.`);
    return sendJsonResponse(204);
  } catch (error) {
    const message = 'An error occurred during event processing.';
    console.error(message, error);
    return sendJsonError(message, 500);
  }
};

export async function processAthleteEvent(event: StravaEventItem): Promise<void> {
  // TODO handle case when user revoked an access to our app
  await stravaWebhookEventService.setEventStatus(
    event.id,
    EventStatus.SKIPPED,
    'Athlete events processing not implemented.',
  );
}

export async function processActivityEvent(event: StravaEventItem): Promise<void> {
  if (!(await stravaWebhookEventService.eventDocumentExists(event.id))) {
    throw new Error(`Event with _id: '${event.id}' does not exist in the DB.`);
  }

  if (!(await athleteService.athleteExists(event.athlete_id))) {
    const message = `Unable to find an athlete with id='${event.athlete_id}' in current challenge.`;
    await stravaWebhookEventService.setEventStatus(event.id, EventStatus.PROCESSED, message);
    throw new Error(message);
  }

  if (event.status === EventStatus.PROCESSING) {
    console.log(`Cannot process an event with id='${event.id}' because it is already processing.`);
    return;
  }

  switch (event.aspect_type) {
    case AspectType.CREATE: {
      /**
       *  - create  -> mark event status as processing
       *            -> check if athlete is taking part in our challenge - moved to the top of the function
       *            -> check that activity creation is not duplicated (activity already exists in DB).
       *            -> fetch activity details by athleteId and objectID. Handle 4XX response (make status failed)
       *            -> if activity is in current challenge - save it to DB
       *              -> success - mark as processed
       *              -> fail - mark as failed
       */
      await stravaWebhookEventService.setEventStatus(event.id, EventStatus.PROCESSING);

      if (await hasUserActivityWithId(event.athlete_id, event.object_id)) {
        await stravaWebhookEventService.setEventStatus(
          event.id,
          EventStatus.PROCESSED,
          `Athlete with id='${event.athlete_id}' already have an activity with id='${event.object_id}' in the database.`,
        );
        return;
      }

      const athlete: Athlete = await athleteService.findAthleteById(event.athlete_id);
      let activity: Activity | StravaServiceResponse = await fetchActivityById(event.object_id, athlete);

      if (activity === StravaServiceResponse.NOT_FOUND) {
        await stravaWebhookEventService.setEventStatus(
          event.id,
          EventStatus.PROCESSED,
          `The activity with id='${event.object_id}' does not exists in Strava.`,
        );
        return;
      } else if (
        activity === null ||
        Object.values(StravaServiceResponse).includes(activity as StravaServiceResponse)
      ) {
        await stravaWebhookEventService.setEventStatus(
          event.id,
          EventStatus.FAILED,
          `An error occurred. The activity with id='${event.object_id}' returned from StravaService is null. Check CloudWatch logs for more details.`,
        );
        return;
      }

      activity = activity as Activity;

      if (!isActivityCreatedDuringCurrentChallenge(activity.start_date)) {
        await stravaWebhookEventService.setEventStatus(
          event.id,
          EventStatus.PROCESSED,
          `The activity wasn't created during the current challenge.`,
        );
        return;
      }

      const success: boolean = await activityService.saveActivity(activity);
      if (success) {
        await stravaWebhookEventService.setEventStatus(
          event.id,
          EventStatus.PROCESSED,
          'Activity created successfully.',
        );
        await invokeFeedFlowLambda(event.athlete_id, event.object_id);
      } else {
        await stravaWebhookEventService.setEventStatus(event.id, EventStatus.FAILED, 'Failed to create the activity.');
      }

      break;
    }
    case AspectType.UPDATE: {
      /**
       *  - update  -> mark event status as processing
       *            -> check if athlete is taking part in our challenge - moved to the top of the function
       *            -> check if user has (in mongo) the given activity by activityID
       *              -> if no - skip and mark as processed
       *            -> fetch activity details. Handle 4XX response (make status failed)
       *            -> if activity is in current challenge - save it to DB by doing upsert on activityID
       *              -> success - mark as processed
       *              -> fail - mark as failed
       */
      await stravaWebhookEventService.setEventStatus(event.id, EventStatus.PROCESSING);

      if (!(await hasUserActivityWithId(event.athlete_id, event.object_id))) {
        await stravaWebhookEventService.setEventStatus(
          event.id,
          EventStatus.PROCESSED,
          `Athlete with id='${event.athlete_id}' doesn't have an activity with id='${event.object_id}'.`,
        );
        return;
      }

      const athlete: Athlete = await athleteService.findAthleteById(event.athlete_id);
      let activity: Activity | StravaServiceResponse = await fetchActivityById(event.object_id, athlete);

      if (activity === StravaServiceResponse.NOT_FOUND) {
        await stravaWebhookEventService.setEventStatus(
          event.id,
          EventStatus.PROCESSED,
          `The activity with id='${event.object_id}' does not exists in Strava.`,
        );
        return;
      } else if (
        activity === null ||
        Object.values(StravaServiceResponse).includes(activity as StravaServiceResponse)
      ) {
        await stravaWebhookEventService.setEventStatus(
          event.id,
          EventStatus.FAILED,
          `An error occurred. The activity with id='${event.object_id}' returned from StravaService is null. Check CloudWatch logs for more details.`,
        );
        return;
      }

      activity = activity as Activity;

      if (!isActivityCreatedDuringCurrentChallenge(activity.start_date)) {
        await stravaWebhookEventService.setEventStatus(
          event.id,
          EventStatus.PROCESSED,
          `The activity wasn't created during the current challenge.`,
        );
        return;
      }

      const success: boolean = await activityService.saveActivity(activity);
      if (success) {
        await stravaWebhookEventService.setEventStatus(
          event.id,
          EventStatus.PROCESSED,
          'Activity updated successfully.',
        );
      } else {
        await stravaWebhookEventService.setEventStatus(event.id, EventStatus.FAILED, 'Failed to updated the activity.');
      }

      break;
    }
    case AspectType.DELETE: {
      /**
       *  - delete  -> mark event status as processing
       *            -> check if athlete is taking part in our challenge - moved to the top of the function
       *            -> check if we have this activity (by athleteId) in DB and delete it. Handle MongoDB result acknowledgement.
       *              -> if not - mark as processed
       *            -> mark as processed
       */
      await stravaWebhookEventService.setEventStatus(event.id, EventStatus.PROCESSING);

      if (!(await hasUserActivityWithId(event.athlete_id, event.object_id))) {
        await stravaWebhookEventService.setEventStatus(
          event.id,
          EventStatus.PROCESSED,
          `Athlete with id='${event.athlete_id}' doesn't have an activity with id='${event.object_id}'.`,
        );
        return;
      }

      const deletedSuccessfully: boolean = await activityService.deleteActivityById(event.object_id);
      if (deletedSuccessfully) {
        await stravaWebhookEventService.setEventStatus(
          event.id,
          EventStatus.PROCESSED,
          'Activity deleted successfully.',
        );
      } else {
        await stravaWebhookEventService.setEventStatus(event.id, EventStatus.FAILED, 'Failed to delete the activity.');
      }

      break;
    }
  }

  function isActivityCreatedDuringCurrentChallenge(activityStartDate: string): boolean {
    const timestamp = dateTimeStringToTimestamp(activityStartDate);
    return timestamp > getChallengeStartTimestamp() && timestamp < getChallengeEndTimestamp();
  }

  /**
   * Determines whether the user has activity in a database.
   * If not, it may mean that modified activity is out of the challenge.
   */
  async function hasUserActivityWithId(athleteId: number, activityId: number): Promise<boolean> {
    const activity: Activity = await activityService.getById(activityId);
    return activity && activity.athleteId === athleteId;
  }

  /**
   * Invokes AWS Lambda that starts feed invocation.
   *
   * The function DOES NOT wait for the lambda to complete!!! It only makes a request to AWS Lambda API and wait for the 'invoke' API call to complete.
   * @param athleteId Athlete ID.
   * @param activityId Activity ID.
   */
  async function invokeFeedFlowLambda(athleteId: number, activityId: number) {
    const params = {
      FunctionName: `${APPLICATION_NAME_PREFIX}-${feedFlowInvokerFunctionName}`,
      InvocationType: 'Event',
      Payload: JSON.stringify({ athleteId: athleteId, activityId: activityId }),
    };

    try {
      await lambda.invoke(params).promise();
      console.log(`${feedFlowInvokerFunctionName} Lambda invocation started.`);
    } catch (error) {
      console.log(`Error invoking ${feedFlowInvokerFunctionName} Lambda:`, error);
    }
  }
}
