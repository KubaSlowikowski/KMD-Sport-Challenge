/* eslint-disable @typescript-eslint/no-explicit-any */
// mipa: TODO: add types to this file

import { Athlete } from '../models/domain/Athlete';

/**
 * Currently we don't need this job, but in the future it may be used as a template code for activities fetching from a given period.
 */

async function fetchAthletesNewestData() {
  // console.log(`Job fetchAthletesNewestData() started.`);
  //
  // const athletes: Athlete[] = await getAllAthletes();
  //
  // const promises = athletes.map(athlete => fetchNewActivities(athlete).catch(error => ({ error })));
  //
  // const results = await Promise.allSettled(promises);
  // const errors = results.filter(result => result.status === 'rejected');
  //
  // if (errors.length > 0) {
  //     return sendJsonError((errors[0] as any).reason.message, 500);
  // }
  //
  // return sendJsonResponse(204);
}

async function fetchNewActivities(athlete: Athlete): Promise<void> {
  // const newActivities: Activity[] = await stravaService.getAthleteActivities(athlete, athlete.updatedAt);
  //
  // if (!newActivities) {
  //     console.error(`Error in activities-job. Activities updating for athlete '${athlete.name}' failed. Reason: new activities array returned from Strava service is undefined or null.`);
  //     throw new Error(`Error in activities-job. Activities updating for athlete '${athlete.name}' failed. Reason: new activities array returned from Strava service is undefined or null.`);
  // }
  // if (newActivities.length === 0) {
  //     console.log(`No new activities for athlete '${athlete.name}'.`);
  //     return;
  // }
  // console.log(`Fetched ${newActivities.length} new activities for athlete '${athlete.name}'.`);
  //
  // await saveActivities(athlete.athleteId, newActivities);
}

// async function syncActivities() {
//     console.log(`Job syncActivities() started.`);
//
//     const database = await connectToDatabase();
//     const athletesCollection = database.collection("athletes");
//
//     const athletesCursor = await athletesCollection.find({athleteId: {$ne: 109707293}});
//     for await (const athlete of athletesCursor) {
//         let i = 0;
//         for (const activity of athlete.activities) {
//             if (!activity.synced) {
//                 try {
//                     const detailedActivity = await stravaService.getDetailedActivity(athlete, activity);
//                     if (detailedActivity === undefined || detailedActivity === null) continue;
//
//                     await athletesCollection.updateOne(
//                         { athleteId: athlete.athleteId },
//                         { $set: { 'activities.$[act]': detailedActivity } },
//                         { arrayFilters: [{ 'act.id': detailedActivity.id }] }
//                     );
//
//                     console.log(`Synced activity '${activity.name}' for athlete '${athlete.name}'.`);
//                 } catch (error) {
//
//                 }
//             }
//         }
//     }
//
//     console.log(`Job syncActivities() finished.`);
// }

module.exports.updateActivities = fetchAthletesNewestData;
