import { StravaEventItem } from '../models/domain/StravaWebhookEvents';
import { APPLICATION_NAME_PREFIX, sendJsonError, sendJsonResponse } from '../utils/awsUtils';
import { Context, DynamoDBRecord, DynamoDBStreamEvent, Handler } from 'aws-lambda';
import Lambda from 'aws-sdk/clients/lambda';
import { Converter } from 'aws-sdk/clients/dynamodb';
import { stravaEventsProcessorFunctionName } from './stravaEventsProcessor';

const lambda = new Lambda();
export const dynamoDbEventsProcessorFunctionName = 'dynamoDbEventsProcessor';

/**
 * AWS Lambda that receives an event sent by DynamoDB trigger and delegates it to appropriate lambdas depending on event object_type.
 *
 * @param event Event that DynamoDB created.
 * @param context Context object, which contains information about the invocation, function, and execution environment.
 */
export const processDynamoDbTriggerEventHandler: Handler = async (event: DynamoDBStreamEvent, context: Context) => {
  const records: DynamoDBRecord[] = event.Records;
  console.log(`Received ${records.length} DynamoDB records`);
  console.log(records);

  for (let record of records) {
    if (record.eventName !== 'INSERT') continue; // we only want to process the new events

    const dbItem = record.dynamodb.NewImage;
    const stravaEvent: StravaEventItem = Converter.unmarshall(dbItem) as StravaEventItem;

    try {
      await invokeEventProcessLambda(stravaEvent);
      return sendJsonResponse(204);
    } catch (error) {
      const message = 'An error occurred during DynamoDB-trigger-event processing.';
      console.error(message, error);
      return sendJsonError(message, 500);
    }
  }

  sendJsonResponse(204);
};

/**
 * Invokes AWS Lambda that processes StravaEventDocument.
 *
 * The function DOES NOT wait for the lambda to complete!!! It only makes a request to AWS Lambda API and wait for the 'invoke' API call to complete.
 * @param eventItem Strava webhooks event document to process.
 */
async function invokeEventProcessLambda(eventItem: StravaEventItem) {
  const params = {
    FunctionName: `${APPLICATION_NAME_PREFIX}-${stravaEventsProcessorFunctionName}`,
    InvocationType: 'Event',
    Payload: JSON.stringify({ stravaEventItem: eventItem }),
  };

  try {
    await lambda.invoke(params).promise();
    console.log(`${stravaEventsProcessorFunctionName} Lambda invocation started.`);
  } catch (error) {
    console.log(`Error invoking ${stravaEventsProcessorFunctionName} Lambda:`, error);
  }
}
