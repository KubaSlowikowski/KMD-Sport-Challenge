/* eslint-disable @typescript-eslint/no-explicit-any */
// TODO: mipa enable types

import jwt from 'jsonwebtoken';
import { Athlete } from '../models/domain/Athlete';
import { athleteService } from '../services/athleteService';
import { securityService } from '../services/securityService';

export const securityMiddleware = [jwt_Middleware, oAuth2_TokenRefreshingMiddleware];

function jwt_Middleware(req, res, next) {
  const token = req.headers['authorization']?.split(' ')[1];
  if (!token) {
    return res.status(403).json({ error: 'JWT token not found.' });
  }

  jwt.verify(token, process.env.JWT_PRIVATE_KEY, (err, decoded_Data) => {
    if (err) {
      return res.status(403).json({ error: 'Invalid token' });
    }
    res.locals.athleteId = decoded_Data.athleteId;

    next();
  });
}

async function oAuth2_TokenRefreshingMiddleware(req, res, next) {
  const athleteId = res.locals.athleteId;

  if (!athleteId) {
    return res.status(400).send('Athlete ID not found.');
  }

  // get access tokens from db
  const athlete: Athlete = await athleteService.findAthleteById(athleteId);
  if (!athlete) {
    return res.status(404).send(`Athlete with id='${athleteId}' not found`);
  }

  if (securityService.isTokenExpired((athlete as any).expires_at)) {
    try {
      await securityService.refreshToken((athlete as any).refresh_token, athleteId);
    } catch (err) {
      if (err.response.status === 400) {
        res.redirect('/login');
      } else {
        res.send('Error retrieving access token.');
      }
    }
  }

  next();
}
