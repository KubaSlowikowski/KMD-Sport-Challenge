import { DetailedActivity } from '../models/detailed-activity';
import { RIDE_FIXTURE } from './ride.fixture';
import { WORKOUT_FIXTURE } from './workout.fixture';

export type PossibleBuilderSportTypes = 'Workout' | 'Ride';

export class DetailedActivityBuilder {
  constructor(
    private currentFns: ((model: DetailedActivity) => DetailedActivity)[] = [],
    private model: DetailedActivity | null = null,
  ) {}

  public updateModel(model: DetailedActivity): DetailedActivityBuilder {
    return new DetailedActivityBuilder([], model);
  }

  public build(): DetailedActivity {
    const model: DetailedActivity = this.model ?? this.createDefaultModel();
    return this.currentFns.reduce((model, fn) => fn(model), model);
  }

  public ofType(type: PossibleBuilderSportTypes): DetailedActivityBuilder {
    const fns: ((model: DetailedActivity) => DetailedActivity)[] = [
      ...this.currentFns,
      model => ({
        ...this.getBaseModel(type),
      }),
    ];
    return new DetailedActivityBuilder(fns, this.model);
  }

  private createDefaultModel(): DetailedActivity {
    return this.getBaseModel('Workout');
  }

  private getBaseModel(type: PossibleBuilderSportTypes): DetailedActivity {
    switch (type) {
      case 'Workout':
        return WORKOUT_FIXTURE;
      case 'Ride':
        return RIDE_FIXTURE;
      default:
        throw new Error('Not mapped');
    }
  }
}
