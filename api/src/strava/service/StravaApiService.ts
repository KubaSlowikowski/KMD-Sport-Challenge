import { SummaryActivity } from '../models/summary-activity.dto';
import axios, { AxiosResponse } from 'axios';
import { ENDPOINTS } from '../../utils/utils';
import { DetailedActivity } from '../models/detailed-activity';
import { DetailedAthlete } from '../models/detailed-athlete';

class StravaApiService {
  public async listAthleteActivities(args: {
    after: number;
    before: number;
    accessToken: string;
  }): Promise<AxiosResponse<SummaryActivity[]>> {
    return await axios.get(ENDPOINTS.STRAVA_ATHLETE_ACTIVITIES, {
      params: { after: args.after, before: args.before, per_page: 100 },
      headers: { Authorization: `Bearer ${args.accessToken}` },
    });
  }

  public async getActivityById(activityId: number, accessToken: string): Promise<AxiosResponse<DetailedActivity>> {
    return await axios.get(`${ENDPOINTS.STRAVA_ACTIVITY}/${activityId}`, {
      headers: { Authorization: `Bearer ${accessToken}` },
    });
  }

  public async getLoggedInAthlete(accessToken: string): Promise<AxiosResponse<DetailedAthlete>> {
    return await axios.get(ENDPOINTS.STRAVA_GET_LOGGEDIN_ATHLETE, {
      headers: { Authorization: `Bearer ${accessToken}` },
    });
  }
}

export const stravaApiService = new StravaApiService();
