export interface Split {
  /** The average speed of this split, in meters per second */
  average_speed: number;
  /** The distance of this split, in meters */
  distance: number;
  /** The elapsed time of this split, in seconds */
  elapsed_time: number;
  /** The elevation difference of this split, in meters */
  elevation_difference: number;
  /** The pacing zone of this split */
  pace_zone: number;
  /** The moving time of this split, in seconds */
  moving_time: number;
  /** N/A */
  split: number;

  average_grade_adjusted_speed: number;
}
