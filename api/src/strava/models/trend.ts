export interface Trend {
  speeds: number[];
  current_activity_index: number;
  min_speed: number;
  mid_speed: number;
  max_speed: number;
  direction: number;
}
