import { Achievement } from './achievment';
import { MetaActivity } from './meta-activity';
import { MetaAthlete } from './meta-athlete';
import { SummarySegment } from './summary-segment';

export interface DetailedSegmentEffort {
  /** The unique identifier of this effort */
  id: number;
  /** The unique identifier of the activity related to this effort */
  activity_id: number;
  /** The effort's elapsed time */
  elapsed_time: number;
  /** The time at which the effort was started. */
  start_date: string;
  /** The time at which the effort was started in the local timezone. */
  start_date_local: string;
  /** The effort's distance in meters */
  distance: number;
  /** Whether this effort is the current best on the leaderboard */
  is_kom: boolean;
  /** The name of the segment on which this effort was performed */
  name: string;
  /** An instance of MetaActivity. */
  activity: MetaActivity;
  /** An instance of MetaAthlete. */
  athlete: MetaAthlete;
  /** The effort's moving time */
  moving_time: number;
  /** The start index of this effort in its activity's stream */
  start_index: number;
  /** The end index of this effort in its activity's stream */
  end_index: number;
  /** The effort's average cadence */
  average_cadence: number;
  /** The average wattage of this effort */
  average_watts?: number;
  /** For riding efforts, whether the wattage was reported by a dedicated recording device */
  device_watts: boolean;
  /** The heart heart rate of the athlete during this effort */
  average_heartrate?: number;
  /** The maximum heart rate of the athlete during this effort */
  max_heartrate?: number;
  /** An instance of SummarySegment. */
  segment: SummarySegment;
  /** The rank of the effort on the global leaderboard if it belongs in the top 10 at the time of upload */
  kom_rank?: number;
  /** The rank of the effort on the athlete's leaderboard if it belongs in the top 3 at the time of upload */
  pr_rank?: number;
  /** Whether this effort should be hidden when viewed within an activity */
  hidden: boolean;

  resource_state: number;
  achievements: Achievement[];
  visibility: string;
}
