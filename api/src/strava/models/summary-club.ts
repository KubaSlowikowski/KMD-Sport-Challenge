import { ActivityType } from '../types/activity.type';

export interface SummaryClub {
  /** The club's unique identifier. */
  id: number;
  /** Resource state, indicates level of detail. Possible values: 1 -> "meta", 2 -> "summary", 3 -> "detail" */
  resource_state: number;
  /** The club's name. */
  name: string;
  /** URL to a 60x60 pixel profile picture. */
  profile_medium: string;
  /** URL to a ~1185x580 pixel cover photo. */
  cover_photo: string;
  /** URL to a ~360x176 pixel cover photo. */
  cover_photo_small: string;
  /** @deprecated. Prefer to use activity_types. May take one of the following values: cycling, running, triathlon, other */
  sport_type: 'cycling' | 'running' | 'triathlon' | 'other';
  /** The activity types that count for a club. This takes precedence over sport_type. */
  activity_types: ActivityType[];
  /** The club's city. */
  city: string;
  /** The club's state or geographical region. */
  state: string;
  /** The club's country. */
  country: string;
  /** Whether the club is private. */
  private: boolean;
  /** The club's member count. */
  member_count: number;
  /** Whether the club is featured or not. */
  featured: boolean;
  /** Whether the club is verified or not. */
  verified: boolean;
  /** The club's vanity URL. */
  url: string;

  profile: string;
  activity_types_icon: string;
  dimensions: string[];
  localized_sport_type: string;
}
