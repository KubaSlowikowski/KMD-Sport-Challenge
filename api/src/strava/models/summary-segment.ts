import { SummaryPRSegmentEffort } from './summary-pr-segment-effort';
import { SummarySegmentEffort } from './summary-segment-effort';

export interface SummarySegment {
  /** The unique identifier of this segment */
  id: number;
  /** The name of this segment */
  name: string;
  /** May take one of the following values: Ride, Run */
  activity_type: 'Ride' | 'Run';
  /** The segment's distance, in meters */
  distance: number;
  /** The segment's average grade, in percents */
  average_grade: number;
  /** The segments's maximum grade, in percents */
  maximum_grade: number;
  /** The segments's highest elevation, in meters */
  elevation_high: number;
  /** The segments's lowest elevation, in meters */
  elevation_low: number;
  /** An instance of LatLng. */
  start_latlng: number[];
  /** An instance of LatLng. */
  end_latlng: number[];
  /** The category of the climb [0, 5]. Higher is harder ie. 5 is Hors catégorie, 0 is uncategorized in climb_category. */
  climb_category: number;
  /** The segments's city. */
  city: string;
  /** The segments's state or geographical region. */
  state: string;
  /** The segment's country. */
  country: string;
  /** Whether this segment is private. */
  private: boolean;
  /** Whether this segment is considered hazardous */
  hazardous: boolean;
  /** Whether this route is starred by the logged-in athlete */
  starred: boolean;
  /** An instance of SummaryPRSegmentEffort. */
  athlete_pr_effort?: SummaryPRSegmentEffort;
  /** An instance of SummarySegmentEffort. */
  athlete_segment_stats?: SummarySegmentEffort;

  resource_state: number;
  elevation_profile?: unknown;
}
