import { Trend } from './trend';

export interface SimilarActivities {
  /** Number of efforts by the authenticated athlete on this segment. */
  effort_count: number;
  average_speed: number;
  min_average_speed: number;
  mid_average_speed: number;
  max_average_speed: number;
  /** The rank of the effort on the athlete's leaderboard if it belongs in the top 3 at the time of upload */
  pr_rank?: number;
  frequency_milestone?: unknown;
  trend: Trend;
  /** Resource state, indicates level of detail. Possible values: 1 -> "meta", 2 -> "summary", 3 -> "detail" */
  resource_state: number;
}
