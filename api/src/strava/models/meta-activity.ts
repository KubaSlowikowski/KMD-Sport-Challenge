export interface MetaActivity {
  /** The unique identifier of the activity */
  id: number;
  visibility: string;
  resource_state: number;
}
