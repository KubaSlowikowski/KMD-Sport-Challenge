import { ActivityType } from '../types/activity.type';
import { PolylineMap } from './polyline-map.dto';
import { MetaAthlete } from './meta-athlete';
import { DetailedSegmentEffort } from './detailed-segment-effort';
import { Split } from './split';
import { Lap } from './lap';
import { SimilarActivities } from './similar-activities';
import { StatsVisibility } from './stats-visibility';
import { PhotosSummary } from './photos-summary';
import { SummaryGear } from './summary-gear';

export interface DetailedActivity {
  /** The unique identifier of the activity */
  id: number;
  /** The identifier provided at upload time */
  external_id: string;
  /** The identifier of the upload that resulted in this activity */
  upload_id: number;
  /** An instance of MetaAthlete */
  athlete: MetaAthlete;
  /** The name of the activity */
  name: string;
  /** The name of the activity */
  distance: number;
  /** The activity's moving time, in seconds */
  moving_time: number;
  /** The activity's elapsed time, in seconds */
  elapsed_time: number;
  /** The activity's total elevation gain. */
  total_elevation_gain: number;
  /** The activity's highest elevation, in meters */
  elev_high?: number;
  /** The activity's lowest elevation, in meters */
  elev_low?: number;
  /** @deprecated Deprecated. Prefer to use sport_type */
  type: string;
  /** An instance of SportType. */
  sport_type: ActivityType;
  /** The time at which the activity was started. */
  start_date: string;
  /** The time at which the activity was started in the local timezone. */
  start_date_local: string;
  /** The timezone of the activity */
  timezone: string;
  /** An instance of LatLng. */
  start_latlng: number[];
  /** An instance of LatLng. */
  end_latlng: number[];
  /** The number of achievements gained during this activity */
  achievement_count: number;
  /** The number of kudos given for this activity */
  kudos_count: number;
  /** The number of comments for this activity */
  comment_count: number;
  /** The number of athletes for taking part in a group activity */
  athlete_count: number;
  /** The number of Instagram photos for this activity */
  photo_count: number;
  /** The number of Instagram and Strava photos for this activity */
  total_photo_count: number;
  /** An instance of PolylineMap. */
  map: PolylineMap;
  /** Whether this activity was recorded on a training machine */
  trainer: boolean;
  /** Whether this activity is a commute */
  commute: boolean;
  /** Whether this activity was created manually */
  manual: boolean;
  /** Whether this activity is private */
  private: boolean;
  /** Whether this activity is flagged */
  flagged: boolean;
  /** The activity's workout type */
  workout_type?: number;
  /** The unique identifier of the upload in string format */
  upload_id_str?: string;
  /** The activity's average speed, in meters per second */
  average_speed: number;
  /** The activity's max speed, in meters per second */
  max_speed: number;
  /** Whether the logged-in athlete has kudoed this activity */
  has_kudoed: boolean;
  /** Whether the activity is muted */
  hide_from_home: boolean;
  /** The id of the gear for the activity */
  gear_id?: string;
  /** The total work done in kilojoules during this activity. Rides only */
  kilojoules?: number;
  /** Average power output in watts during this activity. Rides only */
  average_watts?: number;
  /** Whether the watts are from a power meter, false if estimated */
  device_watts?: boolean;
  /** Rides with power meter data only */
  max_watts?: number;
  /** Similar to Normalized Power. Rides with power meter data only */
  weighted_average_watts?: number;
  /** The description of the activity */
  description?: string;
  /** An instance of PhotosSummary */
  photos: PhotosSummary;
  /** An instance of SummaryGear. */
  gear?: SummaryGear;
  /** The number of kilocalories consumed during this activity */
  calories: number;
  /** A collection of DetailedSegmentEffort objects. */
  segment_efforts: DetailedSegmentEffort[];
  /** The name of the device used to record the activity */
  device_name?: string;
  /** The token used to embed a Strava activity */
  embed_token: string;
  /** The splits of this activity in metric units (for runs) */
  splits_metric?: Split[];
  /** The splits of this activity in metric units (for runs) */
  splits_standard?: Split[];
  /** A collection of Lap objects */
  laps?: Lap[];
  /** A collection of DetailedSegmentEffort objects. */
  best_efforts?: DetailedSegmentEffort[];

  resource_state: number;
  utc_offset: number;
  location_city?: string;
  location_state?: string;
  location_country?: string;
  visibility: string;
  average_cadence?: number;
  has_heartrate: boolean;
  heartrate_opt_out: boolean;
  display_hide_heartrate_option: boolean;
  from_accepted_tag: boolean;
  pr_count: number;
  perceived_exertion?: number;
  prefer_perceived_exertion: boolean;
  stats_visibility: StatsVisibility[];
  similar_activities?: SimilarActivities;
  available_zones: string[];
}
