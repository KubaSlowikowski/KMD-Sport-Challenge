export interface PhotosSummaryPrimary {
  /** An instance of long. */
  id: number;
  /** An instance of integer. */
  source: number;
  /** An instance of string. */
  unique_id: string;
  /** An instance of string. */
  urls: string;
}
