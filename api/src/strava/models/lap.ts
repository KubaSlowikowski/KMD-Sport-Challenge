import { MetaActivity } from './meta-activity';
import { MetaAthlete } from './meta-athlete';

export interface Lap {
  /** The unique identifier of this lap */
  id: number;
  /** An instance of MetaActivity. */
  activity: MetaActivity;
  /** An instance of MetaAthlete. */
  athlete: MetaAthlete;
  /** The lap's average cadence */
  average_cadence?: number;
  /** The lap's average speed */
  average_speed: number;
  /** The lap's distance, in meters */
  distance: number;
  /** The lap's elapsed time, in seconds */
  elapsed_time: number;
  /** The start index of this effort in its activity's stream */
  start_index: number;
  /** The end index of this effort in its activity's stream */
  end_index: number;
  /** The index of this lap in the activity it belongs to */
  lap_index: number;
  /** The maximum speed of this lat, in meters per second */
  max_speed: number;
  /** The lap's moving time, in seconds */
  moving_time: number;
  /** The name of the lap */
  name: string;
  /** The athlete's pace zone during this lap */
  pace_zone?: number;
  /** An instance of integer. */
  split: number;
  /** The time at which the lap was started. */
  start_date: string;
  /** The time at which the lap was started in the local timezone. */
  start_date_local: string;
  /** The elevation gain of this lap, in meters */
  total_elevation_gain: number;

  resource_state: number;
  device_watts: boolean;
  average_watts: number;
}
