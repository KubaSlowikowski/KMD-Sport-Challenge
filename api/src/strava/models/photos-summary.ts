import { PhotosSummaryPrimary } from './photos-summary-primary';

export interface PhotosSummary {
  /** An instance of PhotosSummary_primary */
  primary?: PhotosSummaryPrimary;
  /** The number of photos */
  count: number;
}
