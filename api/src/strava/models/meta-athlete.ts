export interface MetaAthlete {
  /** The unique identifier of the athlete */
  id: number;
  resource_state: number;
}
