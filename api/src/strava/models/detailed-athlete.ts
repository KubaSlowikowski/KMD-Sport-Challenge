import { SummaryClub } from './summary-club';
import { SummaryGear } from './summary-gear';

export interface DetailedAthlete {
  /** The unique identifier of the athlete */
  id: number;
  /** Resource state, indicates level of detail. Possible values: 1 -> "meta", 2 -> "summary", 3 -> "detail" */
  resource_state: number;
  /** The athlete's first name. */
  firstname: string;
  /** The athlete's last name. */
  lastname: string;
  /** URL to a 62x62 pixel profile picture. */
  profile_medium: string;
  /** URL to a 124x124 pixel profile picture. */
  profile: string;
  /** The athlete's city. */
  city: string;
  /** The athlete's state or geographical region. */
  state: string;
  /** The athlete's country. */
  country?: string;
  /** The athlete's sex. May take one of the following values: M, F */
  sex: 'M' | 'F';
  /** @deprecated. Use summit field instead. Whether the athlete has any Summit subscription. */
  premium: boolean;
  /** Whether the athlete has any Summit subscription. */
  summit: boolean;
  /** The time at which the athlete was created. */
  created_at: string;
  /** The time at which the athlete was last updated. */
  updated_at: string;
  /** The athlete's follower count. */
  follower_count?: number;
  /** The athlete's friend count. */
  friend_count?: number;
  /** The athlete's preferred unit system. May take one of the following values: feet, meters */
  measurement_preference?: 'feet' | 'meters';
  /** The athlete's FTP (Functional Threshold Power). */
  ftp?: number;
  /** The athlete's weight. */
  weight: number;
  /** The athlete's clubs */
  clubs: SummaryClub;
  /** The athlete's bikes. */
  bikes: SummaryGear;
  /** The athlete's shoes. */
  shoes: SummaryGear;

  follower?: number;
  username: string;
  bio: string;
  badge_type_id: number;
  friend?: unknown;
}
