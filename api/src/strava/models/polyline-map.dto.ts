export interface PolylineMap {
  /** The identifier of the map */
  id: string;
  /** The polyline of the map, only returned on detailed representation of an object */
  polyline?: string;
  /** The summary polyline of the map */
  summary_polyline: string;
  resource_state: number;
}
