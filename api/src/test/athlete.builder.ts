import { Athlete } from '../models/domain/Athlete';

export class AthleteBuilder {
  constructor(
    private currentFns: ((model: Athlete) => Athlete)[] = [],
    private model: Athlete | null = null,
  ) {}

  public updateModel(model: Athlete): AthleteBuilder {
    return new AthleteBuilder([], model);
  }

  public build(): Athlete {
    const model: Athlete = this.model ?? this.createDefaultModel();
    return this.currentFns.reduce((model, fn) => fn(model), model);
  }

  private createDefaultModel(): Athlete {
    return {
      athleteId: 2,
      access_token: 'f505cc08a5d1f4a3f18a31f23371bac86e54bf7b',
      expires_at: 1715969284,
      expires_in: 12137,
      imageUrl: 'https://graph.facebook.com/5484752968299250/picture?height=256&width=256',
      name: 'Bilbo Baggins',
      refresh_token: 'b949c362ac53588af59a2cd6c8285891b0477c74',
      token_type: 'Bearer',
      sex: 'M',
    };
  }
}
