import { Activity } from '../models/domain/Activity';
import { ActivityRepository, activityRepository } from '../repository/activityRepository';
import { isActivitySupportExertionLevel } from './rankingService';
import { Athlete } from '../models/domain/Athlete';
import { AthleteService, athleteService } from './athleteService';
import { DetailedActivity } from '../strava/models/detailed-activity';
import { getDetailedActivity } from './stravaService';
import { DetailedActivityMapper, detailedActivityMapper } from '../mapper/detailedActivityMapper';
import { activityOverrideService, ActivityOverrideService } from './activityOverrideService';

class ActivityService {
  private readonly repository: ActivityRepository;
  private readonly athleteService: AthleteService;
  private readonly detailedActivityMapper: DetailedActivityMapper;
  private readonly activityOverrideService: ActivityOverrideService;

  constructor(
    repository: ActivityRepository,
    athleteService: AthleteService,
    detailedActivityMapper: DetailedActivityMapper,
    activityOverrideService: ActivityOverrideService,
  ) {
    this.repository = repository;
    this.athleteService = athleteService;
    this.detailedActivityMapper = detailedActivityMapper;
    this.activityOverrideService = activityOverrideService;
  }

  /**
   * Get all athlete's activities from the database.
   * @param athleteId Athlete's ID,
   * @return Athlete's activities array.
   */
  async getAthleteActivities(athleteId: number): Promise<Activity[]> {
    if (!athleteId) throw new Error(`getAthleteActivities(): Given 'athleteId' is null or undefined.`);

    // FIXME joining activity-overrides is inefficient. We should consider to change our data model and get rid off override table.
    const [activities, activityOverrides] = await Promise.all([
      this.repository.getAthleteActivities(athleteId),
      this.activityOverrideService.getAll(),
    ]);

    return activities.map(act => ({
      ...act,
      ...activityOverrides.find(o => o.activityId === act.id),
    }));
  }

  /**
   * Find particular activity by its ID.
   * @param activityId Activity unique ID.
   */
  async getById(activityId: number): Promise<Activity> {
    if (!activityId) throw new Error(`getById(): Given 'activityId' is null or undefined.`);

    // FIXME joining activity-overrides is inefficient. We should consider to change our data model and get rid off override table.
    const [activity, activityOverride] = await Promise.all([
      this.repository.getById(activityId),
      this.activityOverrideService.getByActivityId(activityId),
    ]);

    return {
      ...activityOverride,
      ...activity,
    };
  }

  /**
   * Saves an activity item in the database.
   * @param activity Activity object to be saved in DB.
   * @return Returns 'true' if save was successful, false otherwise.
   */
  async saveActivity(activity: Activity): Promise<boolean> {
    if (!activity) throw new Error(`saveActivity(): Given 'activity' is null or undefined.`);

    activity.perceived_exertion = null; // Removes perceived_exertion property from activity from Strava, since we want to set it on our side.
    return this.repository.saveActivity(activity);
  }

  /**
   * Saves multiple activities items in the database.
   * @param activities Activity objects array to be saved in DB.
   * @return Returns 'true' if operation was successful, false otherwise.
   */
  async saveActivities(activities: Activity[]): Promise<boolean> {
    if (activities == null) throw new Error(`saveActivities(): Given 'activities' is null or undefined.`);

    activities = activities.map(act => ({
      // Removes perceived_exertion property from activity from Strava, since we want to set it on our side.
      ...act,
      perceived_exertion: null,
    }));

    return this.repository.saveActivities(activities);
  }

  async deleteActivityById(activityId: number): Promise<boolean> {
    if (!activityId) throw new Error(`deleteActivityById(): activityID is null or undefined.`);

    return this.repository.deleteActivityById(activityId);
  }

  async getAllActivities(): Promise<Activity[]> {
    const [activities, activityOverrides] = await Promise.all([
      this.repository.getAllActivities(),
      this.activityOverrideService.getAll(),
    ]);

    return activities.map(act => ({
      ...act,
      ...activityOverrides.find(o => o.activityId === act.id),
    }));
  }

  /**
   * Refresh athlete's activity and save the newest data in DB.
   * @param athleteId
   * @param activityId
   */
  async refreshActivity(athleteId: number, activityId: number): Promise<boolean> {
    const athlete: Athlete = await this.athleteService.findAthleteById(athleteId);
    const detailedActivity: DetailedActivity = await getDetailedActivity(athlete, activityId);
    const activity: Activity = this.detailedActivityMapper.mapDetailedActivity(detailedActivity);

    return await this.saveActivity(activity);
  }

  /**
   * Returns activities with missing exertion level.
   * @param athleteId
   */
  async getActivitiesWithMissingEffortByAthleteId(athleteId: number): Promise<Activity[]> {
    if (!athleteId)
      throw new Error(`getActivitiesWithMissingEffortByAthleteId(): given 'athleteId' is null or undefined.`);

    const activities: Activity[] = await this.getAthleteActivities(athleteId);
    return this.filterActivitiesWithMissingExertionLevel(activities);
  }

  private filterActivitiesWithMissingExertionLevel(activities: Activity[]): Activity[] {
    const toReturn = [];

    activities.forEach(activity => {
      if (!isActivitySupportExertionLevel(activity)) {
        return;
      }

      if (!activity.perceived_exertion) {
        toReturn.push(activity);
      }
    });

    return toReturn;
  }
}

export const activityService = new ActivityService(
  activityRepository,
  athleteService,
  detailedActivityMapper,
  activityOverrideService,
);
