import { getChallengeEndTimestamp, getChallengeStartTimestamp } from '../utils/utils';
import { stravaApiService } from '../strava/service/StravaApiService';
import { Athlete } from '../models/domain/Athlete';
import { Activity } from '../models/domain/Activity';
import { SummaryActivity } from '../strava/models/summary-activity.dto';
import { DetailedActivity } from '../strava/models/detailed-activity';
import { securityService } from './securityService';
import { detailedActivityMapper } from '../mapper/detailedActivityMapper';

export enum StravaServiceResponse {
  NOT_FOUND = 'not_found',
  STRAVA_API_LIMIT_EXCEEDED = 'strava_api_limit_exceeded',
  OTHER = 'other',
}

export async function fetchActivityById(
  activityId: number,
  athlete: Athlete,
): Promise<Activity | StravaServiceResponse> {
  // TODO unify all those functions, because currently logic is duplicated
  let accessToken = athlete.access_token;
  if (securityService.isTokenExpired(athlete.expires_at)) {
    try {
      accessToken = await securityService.refreshToken(athlete.refresh_token, athlete.athleteId);
    } catch (error) {
      return null;
    }
  }

  try {
    const response = await stravaApiService.getActivityById(activityId, accessToken);
    const activity: DetailedActivity = response.data;

    return detailedActivityMapper.mapDetailedActivity(activity);
  } catch (error) {
    if (error.response.status === 404) {
      console.error(
        `Unable to fetch activity with id='${activityId}' from Strava API. Reason: activity does not exist.`,
      );
      return StravaServiceResponse.NOT_FOUND;
    } else if (error.response.status === 429) {
      console.error(
        `Strava API reached maximum number of requests. Unable to fetch details for activity with id='${activityId}'`,
      );
      return StravaServiceResponse.STRAVA_API_LIMIT_EXCEEDED;
    } else {
      console.error(
        `Strava API responded with error in stravaService.getActivityDetails() for athlete '${athlete.name}' (id='${athlete.athleteId}').`,
        {
          status: error.response.status,
          message: error.response.statusText,
        },
      );
      return StravaServiceResponse.OTHER;
    }
  }
}

/**
 * Fetch athlete's activities from Strava API.
 * @param athlete Athlete object (from a database),
 * @param afterTimestamp (Optional) The timestamp after which the data should be fetched.
 * @param beforeTimestamp (Optional) The timestamp after which the data should not be fetched.
 * @return An array of athlete activities.
 */
export async function getAthleteActivities(
  athlete: Athlete,
  afterTimestamp: number = null,
  beforeTimestamp: number = null,
): Promise<Activity[] | null> {
  let accessToken = athlete.access_token;
  if (securityService.isTokenExpired(athlete.expires_at)) {
    try {
      accessToken = await securityService.refreshToken(athlete.refresh_token, athlete.athleteId);
    } catch (error) {
      return null; // FIXME
    }
  }

  try {
    const response = await stravaApiService.listAthleteActivities({
      after: afterTimestamp ?? getChallengeStartTimestamp(),
      before: beforeTimestamp ?? getChallengeEndTimestamp(),
      accessToken: accessToken,
    });
    const activities: SummaryActivity[] = response.data;
    const activityIds: number[] = activities.map(activity => activity.id);
    const activitiesWithDetails: DetailedActivity[] = await fetchDetailedActivities(activityIds, athlete, false);

    return activitiesWithDetails.map(detailedActivityMapper.mapDetailedActivity);
  } catch (error) {
    if (error.response.status === 429) {
      console.error(
        `Strava API reached maximum number of requests. Athlete '${athlete.name}' new activities data will not be fetched now.`,
      );
    } else {
      console.error(
        `Strava API responded with error in stravaService.getAthleteActivities() for athlete '${athlete.name}' (id='${athlete.athleteId}').`,
        {
          status: error.response.status,
          message: error.response.statusText,
        },
      );
    }
    return null;
  }
}

/**
 * Fetches details of the given activities. Fetching operation is performed asynchronously.
 * @param activityIds IDs of the activities for which the details should be fetched.
 * @param athlete Athlete object (from database).
 * @param shouldRefreshToken Determines whether the OAuth2.0 token should be refreshed in case of expiration or not.
 * If the token is not expired set this flag to 'false' to avoid Strava API limits consumption.
 * @return Returns an array of activities with detailed data.
 */
async function fetchDetailedActivities(
  activityIds: number[],
  athlete: Athlete,
  shouldRefreshToken = true,
): Promise<DetailedActivity[]> {
  const settlementDescriptors = await Promise.allSettled(
    activityIds.map(id => getDetailedActivity(athlete, id, shouldRefreshToken)),
  );
  const detailedActivities: DetailedActivity[] = settlementDescriptors
    .filter(descriptor => descriptor.status === 'fulfilled')
    .map(descriptor => (descriptor as { value }).value); // TODO mipa add types

  return detailedActivities;
}

/**
 * Fetch activity details from Strava API.
 * @param athlete Athlete object (from a database),
 * @param activityId The activity ID for which the details data should be fetched.
 * @param shouldRefreshToken Determines whether the OAuth2 access token should be refreshed or not.
 * @return detailedActivity The activity's detailed representation.
 */
export async function getDetailedActivity(
  athlete: Athlete,
  activityId: number,
  shouldRefreshToken = true,
): Promise<DetailedActivity | null> {
  if (!activityId) {
    console.error('Unable to fetch activity details. Given activityId is undefined or null.');
    return;
  }
  if (!athlete) {
    console.error('Unable to fetch activity details. Given athlete is undefined or null.');
    return;
  }

  let access_token = athlete.access_token;
  if (shouldRefreshToken && securityService.isTokenExpired(athlete.expires_at)) {
    try {
      access_token = await securityService.refreshToken(athlete.refresh_token, athlete.athleteId);
    } catch (error) {
      return null;
    }
  }

  try {
    const response = await stravaApiService.getActivityById(activityId, access_token);

    return response.data;
  } catch (error) {
    if (error.response.status === 429) {
      console.error(
        `Strava API reached maximum number of requests. Unable to fetch details for activity with id='${activityId}'`,
      );
    } else {
      console.error(
        `Strava API responded with error in stravaService.getActivityDetails() for athlete '${athlete.name}' (id='${athlete.athleteId}').`,
        {
          status: error.response.status,
          message: error.response.statusText,
        },
      );
    }
  }
}
