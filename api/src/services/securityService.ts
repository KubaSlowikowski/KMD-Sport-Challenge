import axios from 'axios';
import { AthleteAuthTokens, AthleteService, athleteService } from './athleteService';
import { ENDPOINTS, GRANT_TYPES } from '../utils/utils';

class SecurityService {
  private readonly athleteService: AthleteService;

  constructor(athleteService: AthleteService) {
    this.athleteService = athleteService;
  }

  /**
   * Refresh athlete's OAuth2.0 token and save it to a database.
   * @param existing_refresh_token Existing athlete's Strava OAuth2 refresh token,
   * @param athlete_id Athlete's ID,
   * @return {Promise<*>} New refresh token.
   */
  async refreshToken(existing_refresh_token: string, athlete_id: number): Promise<any> {
    const tokenEndpoint = ENDPOINTS.STRAVA_TOKEN; // TODO move this code to Strava related services.
    const requestBody = {
      client_id: process.env.CLIENT_ID,
      client_secret: process.env.CLIENT_SECRET,
      grant_type: GRANT_TYPES.REFRESH_TOKEN,
      refresh_token: existing_refresh_token,
    };

    try {
      const response = await axios.post(tokenEndpoint, requestBody);
      const { token_type, access_token, refresh_token, expires_at, expires_in } = response.data;

      const data: AthleteAuthTokens = {
        token_type: token_type,
        access_token: access_token,
        refresh_token: refresh_token,
        expires_at: expires_at,
        expires_in: expires_in,
      };

      const updateSuccessful: boolean = await this.athleteService.updateAuthTokensForAthlete(athlete_id, data);
      if (updateSuccessful) {
        console.log(`OAuth2 Strava tokens refreshed for athlete with id=${athlete_id}.`);
      } else {
        console.log(`OAuth2 Strava tokens refresh failed for athlete with id=${athlete_id}.`);
      }

      return access_token;
    } catch (error) {
      console.error(`Couldn't refresh OAuth2 Strava tokens for athlete with id='${athlete_id}'.`, {
        status: error.response.status,
        errors: error.response.data.errors,
      });
      throw error;
    }
  }

  isTokenExpired(expires_at: number): boolean {
    const currentTimestampInSeconds = Math.floor(new Date().getTime() / 1000);
    return expires_at < currentTimestampInSeconds + 300;
  }
}

export const securityService = new SecurityService(athleteService);
