import { Athlete } from '../models/domain/Athlete';
import { athleteRepository, AthleteRepository } from '../repository/athleteRepository';

export type AthleteAuthTokens = {
  token_type: string;
  access_token: string;
  refresh_token: string;
  expires_at: number;
  expires_in: number;
};

export class AthleteService {
  private readonly repository: AthleteRepository;

  constructor(repository: AthleteRepository) {
    this.repository = repository;
  }

  /**
   * Find an athlete from database by his ID.
   * @param athleteId Athlete ID.
   * @return Returns promise with athlete (if exists), otherwise null.
   */
  async findAthleteById(athleteId: number): Promise<Athlete> {
    if (!athleteId) throw new Error(`findAthleteById(): Given 'athleteId' is null or undefined.`);

    return this.repository.findAthleteById(athleteId);
  }

  /**
   * Checks whether the athlete with given ID takes part in the challenge (is in a database) or not.
   * @param athleteId Athlete's ID.
   * @return Promise of 'true' if user takes part in the challenge, 'false' otherwise.
   */
  async athleteExists(athleteId: number): Promise<boolean> {
    if (!athleteId) throw new Error(`athleteExists(): Given 'athleteId' is null or undefined.`);

    return this.repository.athleteExists(athleteId);
  }

  /**
   * Fetch all athletes from the database.
   * @return An array of athletes.
   */
  async getAllAthletes(): Promise<Athlete[]> {
    return this.repository.getAllAthletes();
  }

  /**
   * Saves an athlete item in the database.
   * @param athlete Athlete object to be saved in DB.
   * @return Returns 'true' if update was successful, false otherwise.
   */
  async saveAthlete(athlete: Athlete): Promise<boolean> {
    if (!athlete) throw new Error(`saveAthlete(): Given 'athlete' is null or undefined.`);

    return this.repository.saveAthlete(athlete);
  }

  /**
   * Updates OAuth2.0 Strava security tokens in DB for a given athlete.
   * @return Returns 'true' if update was successful, false otherwise.
   */
  async updateAuthTokensForAthlete(athleteId: number, authTokens: AthleteAuthTokens): Promise<boolean> {
    if (!athleteId || !authTokens)
      throw new Error(`updateAuthTokensForAthlete(): Given 'athleteId' or 'authTokens' is null or undefined.`);

    return this.repository.updateAuthTokensForAthlete(athleteId, authTokens);
  }

  /**
   * Deletes athlete by his ID.
   * @param athleteId ID of an athlete.
   * @return Returns 'true' if deletion was successful, false otherwise.
   */
  async deleteAthleteById(athleteId: number): Promise<boolean> {
    if (!athleteId) throw new Error(`deleteAthleteById(): Given 'athleteId' or 'authTokens' is null or undefined.`);

    return this.repository.deleteAthleteById(athleteId);
  }
}

export const athleteService = new AthleteService(athleteRepository);
