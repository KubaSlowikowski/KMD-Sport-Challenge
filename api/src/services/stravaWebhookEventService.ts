import { AspectType, EventStatus, StravaEventItem, WebhookEventFromStrava } from '../models/domain/StravaWebhookEvents';
import { StravaWebhookEventRepository, stravaWebhookEventRepository } from '../repository/stravaWebhookEventRepository';

class StravaWebhookEventService {
  private readonly repository: StravaWebhookEventRepository;

  constructor(repository: StravaWebhookEventRepository) {
    this.repository = repository;
  }

  async save(event: WebhookEventFromStrava): Promise<void> {
    return this.repository.save(event);
  }

  async eventDocumentExists(eventId: string): Promise<boolean> {
    return this.repository.exists(eventId);
  }

  async setEventStatus(eventId: string, status: EventStatus, message?: string): Promise<void> {
    await this.repository.setStatus(eventId, status, message ?? '');
  }

  async getFailedOrNotProcessed(): Promise<StravaEventItem[]> {
    return this.repository.getFailedOrNotProcessed();
  }

  filterOutDeprecatedEvents(events: StravaEventItem[]): StravaEventItem[] {
    type GroupedEvents = { [athlete_id: number]: StravaEventItem[] };

    const groupedEvents = groupByAthleteId(events);

    let newestEvents = getNewestEvents(groupedEvents);

    return newestEvents;

    function groupByAthleteId(events: StravaEventItem[]): GroupedEvents {
      let grouped = {};
      for (let event of events) {
        if (!grouped[event.athlete_id]) {
          grouped[event.athlete_id] = [];
        }
        grouped[event.athlete_id].push(event);
      }

      return grouped;
    }

    // Find the newest event for each object_id for each athlete and object_type
    function getNewestEvents(groupedEvents: GroupedEvents): StravaEventItem[] {
      let newestEvents: StravaEventItem[] = [];

      for (let athlete_id in groupedEvents) {
        const sortedEvents = groupedEvents[athlete_id].sort(
          (a, b) => new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime(),
        );

        const newestForObject: { [athlete_id: number]: StravaEventItem } = {};

        for (let event of sortedEvents) {
          if (deleteEventExists(newestEvents, event)) {
            continue;
          }

          if (updateEventExists(newestEvents, event) && event.aspect_type === AspectType.UPDATE) {
            continue;
          }

          // If the event is a create event, always include it in the returned array
          if (event.aspect_type === AspectType.CREATE || event.aspect_type === AspectType.DELETE) {
            newestEvents = newestEvents.filter(
              e =>
                !(
                  e.object_id === event.object_id &&
                  e.athlete_id === event.athlete_id &&
                  e.object_type === event.object_type
                ),
            );
            newestEvents.push(event);
          }
          // If the event is not a create event, only include it if it's the newest event for its object_id
          else if (!newestForObject[event.object_id]) {
            newestForObject[event.object_id] = event;
            newestEvents.push(event);
          }
        }
      }

      return newestEvents;

      function deleteEventExists(sortedEvents: StravaEventItem[], event: StravaEventItem): boolean {
        return sortedEvents.some(
          e => e.object_id === event.object_id && e.object_type === event.object_type && e.aspect_type === 'delete',
        );
      }

      function updateEventExists(sortedEvents: StravaEventItem[], event: StravaEventItem): boolean {
        return sortedEvents.some(
          e => e.object_id === event.object_id && e.object_type === event.object_type && e.aspect_type === 'update',
        );
      }
    }
  }
}

export const stravaWebhookEventService = new StravaWebhookEventService(stravaWebhookEventRepository);
