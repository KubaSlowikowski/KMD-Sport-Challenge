/* eslint-disable @typescript-eslint/no-explicit-any */
// TODO: mipa add types to this file

import { MET_MAX, MET_MIN, EXERTION_CONFIG, getSportConfig } from '../config/rankingConfig';
import { Activity } from '../models/domain/Activity';
import { activityService } from './activityService';
import { athleteService } from './athleteService';

export type RankingActivity = {
  type: string;
  points: number;
  latterlyPoints: number;
};

export type RankingPosition = {
  athleteId: number;
  athleteName: string;
  imageUrl: string;
  activities: RankingActivity[];
};

export type Ranking = RankingPosition[];

export async function getRanking(): Promise<Ranking> {
  // TODO --- TO BE REFACTORED --- we want to introduce CQRS design pattern where ranking data will be stored in read-only tables.
  // It will speed up application init.
  const [activities, allAthletes] = await Promise.all([
    activityService.getAllActivities(),
    athleteService.getAllAthletes(),
  ]);
  const athletes = allAthletes.map(a => ({
    athleteId: a.athleteId,
    athleteName: a.name,
    imageUrl: a.imageUrl,
    activities: activities.filter(activity => activity.athleteId === a.athleteId),
  }));

  const athletesWithPoints = athletes.map(a => ({
    ...a,
    activitiesWithPoints: a.activities.map(activity => {
      const points: number = calculateActivityPoints(activity);
      return {
        type: activity.sport_type,
        points: points,
        latterlyPoints: isActivityLatterly(activity) ? points : 0,
      };
    }),
  }));

  const ranking: Ranking = athletesWithPoints.map(a => ({
    athleteId: a.athleteId,
    athleteName: a.athleteName,
    imageUrl: a.imageUrl,
    activities: groupActivities(a.activitiesWithPoints),
  }));

  return ranking;
}

export function sumPoints(activities: RankingActivity[]): number {
  return activities.reduce((p, c) => p + c.points, 0);
}

/**
 * Determines whether the given activity has been recently added or not.
 * This information will be useful to determine the athlete's points from last X hours and check if athlete has leveled up in the ranking.
 * @return 'true' if activity was added more than X hours ago, 'false' otherwise.
 */
function isActivityLatterly(activity: Activity): boolean {
  const startDate: Date = new Date(activity.start_date);
  return startDate < getLatterlyDate(24);
}

function groupActivities(activities: RankingActivity[]): RankingActivity[] {
  return activities
    .reduce((p, c) => {
      let group: RankingActivity = p.find(g => g.type === c.type);
      if (group == null) {
        group = {
          type: c.type,
          points: 0,
          latterlyPoints: 0,
        };
        p.push(group);
      }

      group.points += c.points;
      group.latterlyPoints += c.latterlyPoints;
      return p;
    }, [])
    .sort((a: RankingActivity, b: RankingActivity) => b.points - a.points);
}

export function isActivitySupported(activity: Activity): boolean {
  return getSportConfig(activity.sport_type).sport_type != null;
}

export function isActivitySupportExertionLevel(activity: Activity): boolean {
  return getSportConfig(activity.sport_type).type === 'timeExertionEffort';
}

export function calculateActivityPoints(activity: Activity) {
  const configEntry = getSportConfig(activity.sport_type);
  switch (configEntry.type) {
    case 'timePaceHillPowerFnEffort':
      return Math.max(0, calculateDistanceHorizontalHillPacePowerFnEffort(activity, configEntry as any));
    case 'timePacePowerFnEffort':
      return Math.max(0, calculateDistanceHorizontalPacePowerFnEffort(activity, configEntry as any));
    case 'timeExertionEffort':
      return Math.max(0, calculateTimeExertionEffort(activity, configEntry as any));
    case 'timeEffort':
      return Math.max(0, calculateTimeEffort(activity, configEntry as any));
    default:
      throw new Error('Not mapped');
  }
}

export function getCalculationParams(activity: Activity) {
  const configEntry = getSportConfig(activity.sport_type);
  switch (configEntry.type) {
    case 'timePaceHillPowerFnEffort':
      return getDistanceHorizontalHillPacePowerFnEffortCalculationParams(activity, configEntry as any);
    case 'timePacePowerFnEffort':
      return getDistanceHorizontalPacePowerFnEffortCalculationParams(activity, configEntry as any);
    case 'timeExertionEffort':
      return getTimeExertionEffortCalculationParams(activity, configEntry as any);
    case 'timeEffort':
      return getTimeEffortCalculationParams(activity, configEntry as any);
    default:
      throw new Error('Not mapped');
  }
}

function calculateDistanceHorizontalHillPacePowerFnEffort(
  activity: Activity,
  { metPowerFnA, metPowerFnB, metPowerFnN, tuningRatio },
) {
  const distance = activity.distance ?? 0;
  const movingTime = activity.moving_time ?? 0;

  const movingTimeInMin = movingTime / 60;
  const averageSpeedKmH = activity.average_speed * 3.6;

  // dividing distance by 2 based on the assumption that in the average starting and finishing point are on the same elevation
  const avgGradient = (activity.total_elevation_gain / (distance / 2)) * 100;
  const adjustmentGAP = stravaGAPEqualHeartRateModelLookup(avgGradient);
  const paceMinKm = 60 / averageSpeedKmH;
  const adjustedPace = paceMinKm / adjustmentGAP;
  const adjustedAverageSpeedKmH = 60 / adjustedPace;
  return calculatePointsForVelocityAndTimePowerFn({
    speedKmH: adjustedAverageSpeedKmH,
    movingTimeInMin,
    metPowerFnA,
    metPowerFnB,
    metPowerFnN,
    tuningRatio,
  });
}

export function getActivityEffort(activity: Activity) {
  const configEntry = getSportConfig(activity.sport_type);
  return configEntry.type === 'timeExertionEffort' ? activity.perceived_exertion : null;
}

function getDistanceHorizontalHillPacePowerFnEffortCalculationParams(
  activity: Activity,
  { type, metPowerFnA, metPowerFnB, metPowerFnN, tuningRatio, baseSpeedKmH },
) {
  const movingTime = activity.moving_time ?? 0;
  const distance = activity.distance ?? 0;
  const averageSpeed = distance / movingTime;

  const averageSpeedKmH = averageSpeed * 3.6;
  const movingTimeInMin = movingTime / 60;
  const pointsForBaseSpeedPerMinute = calculatePointsForVelocityAndTimePowerFn({
    speedKmH: baseSpeedKmH,
    movingTimeInMin: 1,
    metPowerFnA,
    metPowerFnB,
    metPowerFnN,
    tuningRatio,
  });
  const pointsForBaseSpeed = calculatePointsForVelocityAndTimePowerFn({
    speedKmH: baseSpeedKmH,
    movingTimeInMin,
    metPowerFnA,
    metPowerFnB,
    metPowerFnN,
    tuningRatio,
  });
  const pointsForAverageSpeed = calculatePointsForVelocityAndTimePowerFn({
    speedKmH: averageSpeedKmH,
    movingTimeInMin,
    metPowerFnA,
    metPowerFnB,
    metPowerFnN,
    tuningRatio,
  });
  const pointsForAverageSpeedPlusElevation = calculateDistanceHorizontalHillPacePowerFnEffort(activity, {
    metPowerFnA,
    metPowerFnB,
    metPowerFnN,
    tuningRatio,
  });

  return {
    calculationType: type,
    movingTime: movingTime,
    totalPoints: pointsForAverageSpeedPlusElevation,

    defaultPace: baseSpeedKmH,
    defaultPacePoints: pointsForBaseSpeedPerMinute,
    pointsForMovingTime: pointsForBaseSpeed,
    pointsForAverageSpeed: pointsForAverageSpeed - pointsForBaseSpeed,
    pointsForElevationGain: pointsForAverageSpeedPlusElevation - pointsForAverageSpeed,
  };
}

function getDistanceHorizontalPacePowerFnEffortCalculationParams(
  activity: Activity,
  { type, metPowerFnA, metPowerFnB, metPowerFnN, tuningRatio, baseSpeedKmH },
) {
  const movingTime = activity.moving_time;
  const distance = activity.distance ?? 0;
  const averageSpeed = distance / movingTime;

  const averageSpeedKmH = averageSpeed * 3.6;
  const movingTimeInMin = movingTime / 60;
  const pointsForBaseSpeedPerMinute = calculatePointsForVelocityAndTimePowerFn({
    speedKmH: baseSpeedKmH,
    movingTimeInMin: 1,
    metPowerFnA,
    metPowerFnB,
    metPowerFnN,
    tuningRatio,
  });
  const pointsForBaseSpeed = calculatePointsForVelocityAndTimePowerFn({
    speedKmH: baseSpeedKmH,
    movingTimeInMin,
    metPowerFnA,
    metPowerFnB,
    metPowerFnN,
    tuningRatio,
  });
  const pointsForAverageSpeed = calculatePointsForVelocityAndTimePowerFn({
    speedKmH: averageSpeedKmH,
    movingTimeInMin,
    metPowerFnA,
    metPowerFnB,
    metPowerFnN,
    tuningRatio,
  });

  return {
    calculationType: type,
    movingTime: movingTime,
    totalPoints: pointsForAverageSpeed,

    defaultPace: baseSpeedKmH,
    defaultPacePoints: pointsForBaseSpeedPerMinute,
    pointsForMovingTime: pointsForBaseSpeed,
    pointsForAverageSpeed: pointsForAverageSpeed - pointsForBaseSpeed,
  };
}

function getTimeExertionEffortCalculationParams(activity: Activity, { type, tuningRatio }) {
  const movingTime = activity.moving_time ?? 0;

  const pointsForMovingTime = (movingTime / 60) * getTimeExertionMets(1) * tuningRatio;
  const effortPoints = calculateTimeExertionEffort(activity, { tuningRatio });

  return {
    calculationType: type,
    movingTime: movingTime,
    totalPoints: effortPoints,

    defaultPoints: getTimeExertionMets(activity.perceived_exertion) * tuningRatio,
    effort: activity.perceived_exertion,
    pointsForMovingTime,
    pointsForEffort: effortPoints - pointsForMovingTime,
  };
}

function getTimeEffortCalculationParams(activity: Activity, { type, timeRatio, tuningRatio }) {
  const movingTime = activity.moving_time ?? 0;

  return {
    calculationType: type,
    movingTime: movingTime,
    totalPoints: calculateTimeEffort(activity, { timeRatio, tuningRatio }),

    defaultPoints: timeRatio * tuningRatio,
  };
}

function calculateDistanceHorizontalPacePowerFnEffort(
  activity: Activity,
  { metPowerFnA, metPowerFnB, metPowerFnN, tuningRatio },
) {
  const movingTime = activity.moving_time ?? 0;
  const distance = activity.distance ?? 0;
  const averageSpeed = distance / movingTime;

  const movingTimeInMin = movingTime / 60;
  const averageSpeedKmH = averageSpeed * 3.6;
  return calculatePointsForVelocityAndTimePowerFn({
    speedKmH: averageSpeedKmH,
    movingTimeInMin,
    metPowerFnA,
    metPowerFnB,
    metPowerFnN,
    tuningRatio,
  });
}

function calculatePointsForVelocityAndTimePowerFn({
  speedKmH,
  movingTimeInMin,
  metPowerFnA,
  metPowerFnB,
  metPowerFnN,
  tuningRatio,
}) {
  const met = calculateMetPerMinuteForVelocityPowerFn({
    speedKmH,
    metPowerFnA,
    metPowerFnB,
    metPowerFnN,
    tuningRatio,
  });
  const points = met * movingTimeInMin;
  return points;
}

function calculateMetPerMinuteForVelocityPowerFn({ speedKmH, metPowerFnA, metPowerFnB, metPowerFnN, tuningRatio }) {
  let met = metPowerFnA * Math.pow(speedKmH, metPowerFnN) + metPowerFnB;
  met = met * tuningRatio;
  met = Math.min(Math.max(met, MET_MIN), MET_MAX);
  return met;
}

function calculateTimeExertionEffort(activity: Activity, { tuningRatio }) {
  const movingTime = activity.moving_time ?? 0;

  const mets = getTimeExertionMets(activity.perceived_exertion) * tuningRatio;
  const movingTimeInMin = movingTime / 60;
  const points = movingTimeInMin * mets;
  return points;
}

function getTimeExertionMets(perceived_exertion: number): number {
  const perceivedExertion = perceived_exertion ?? 1;
  return EXERTION_CONFIG.a * Math.pow(perceivedExertion, EXERTION_CONFIG.n) + EXERTION_CONFIG.b;
}

function calculateTimeEffort(activity: Activity, { timeRatio, tuningRatio }): number {
  const movingTimeInMin = activity.moving_time / 60;
  return movingTimeInMin * timeRatio * tuningRatio;
}

// https://medium.com/strava-engineering/an-improved-gap-model-8b07ae8886c3
function stravaGAPEqualHeartRateModelLookup(gradient: number): number {
  const modelDataPoints = [
    { x1: -99999, x2: -32.18421084, y1: 3400.473, y2: 1.6 },
    { x1: -32.18421084, x2: -27.76315839, y1: 1.6, y2: 1.384615385 },
    { x1: -27.76315839, x2: -23.78947353, y1: 1.384615385, y2: 1.225641104 },
    { x1: -23.78947353, x2: -20.31578946, y1: 1.225641104, y2: 1.094871873 },
    { x1: -20.31578946, x2: -17.28947299, y1: 1.094871873, y2: 1 },
    { x1: -17.28947299, x2: -14.65789575, y1: 1, y2: 0.930768996 },
    { x1: -14.65789575, x2: -12.31578898, y1: 0.930768996, y2: 0.894871873 },
    { x1: -12.31578898, x2: -10.28947166, y1: 0.894871873, y2: 0.876922842 },
    { x1: -10.28947166, x2: -8.526315447, y1: 0.876922842, y2: 0.882051204 },
    { x1: -8.526315447, x2: -7.07894869, y1: 0.882051204, y2: 0.88461515 },
    { x1: -7.07894869, x2: -5.684210298, y1: 0.88461515, y2: 0.897435819 },
    { x1: -5.684210298, x2: -4.631580368, y1: 0.897435819, y2: 0.907692073 },
    { x1: -4.631580368, x2: -3.657892984, y1: 0.907692073, y2: 0.92820505 },
    { x1: -3.657892984, x2: -2.815790967, y1: 0.92820505, y2: 0.941025719 },
    { x1: -2.815790967, x2: -2.078945679, y1: 0.941025719, y2: 0.964102642 },
    { x1: -2.078945679, x2: -1.473685758, y1: 0.964102642, y2: 0.976923077 },
    { x1: -1.473685758, x2: -0.973682565, y1: 0.976923077, y2: 0.992307692 },
    { x1: -0.973682565, x2: -0.552631557, y1: 0.992307692, y2: 0.997435819 },
    { x1: -0.552631557, x2: 0, y1: 0.997435819, y2: 1 },
    { x1: 0, x2: 0.526317375, y1: 1, y2: 1.010256489 },
    { x1: 0.526317375, x2: 0.947368383, y1: 1.010256489, y2: 1.020512742 },
    { x1: 0.947368383, x2: 1.473685758, y1: 1.020512742, y2: 1.041025719 },
    { x1: 1.473685758, x2: 2.052631497, y1: 1.041025719, y2: 1.061538462 },
    { x1: 2.052631497, x2: 2.763157784, y1: 1.061538462, y2: 1.092307692 },
    { x1: 2.763157784, x2: 3.631578802, y1: 1.092307692, y2: 1.120512742 },
    { x1: 3.631578802, x2: 4.578947185, y1: 1.120512742, y2: 1.164102642 },
    { x1: 4.578947185, x2: 5.71052448, y1: 1.164102642, y2: 1.217948796 },
    { x1: 5.71052448, x2: 7.000001325, y1: 1.217948796, y2: 1.279487258 },
    { x1: 7.000001325, x2: 8.500001265, y1: 1.279487258, y2: 1.366666588 },
    { x1: 8.500001265, x2: 10.31579067, y1: 1.366666588, y2: 1.479487258 },
    { x1: 10.31579067, x2: 12.31578898, y1: 1.479487258, y2: 1.623076923 },
    { x1: 12.31578898, x2: 14.60526257, y1: 1.623076923, y2: 1.807692308 },
    { x1: 14.60526257, x2: 17.28947299, y1: 1.807692308, y2: 2.048718027 },
    { x1: 17.28947299, x2: 20.28947287, y1: 2.048718027, y2: 2.317948796 },
    { x1: 20.28947287, x2: 23.73684276, y1: 2.317948796, y2: 2.620512742 },
    { x1: 23.73684276, x2: 27.7105252, y1: 2.620512742, y2: 2.961538462 },
    { x1: 27.7105252, x2: 32.18421084, y1: 2.961538462, y2: 3.333333412 },
    { x1: 32.18421084, x2: 99999, y1: 3.333333412, y2: 7700.778 },
  ];

  const singleRange = modelDataPoints.find(el => gradient >= el.x1 && gradient < el.x2);

  if (singleRange === undefined) {
    // return ZERO so it will be easily spotted when gradient is outside of range that makes sense
    return 0;
  }

  // just a linear equation for two points
  const valueInRange =
    ((singleRange.y2 - singleRange.y1) / (singleRange.x2 - singleRange.x1)) * gradient +
    singleRange.y1 -
    ((singleRange.y2 - singleRange.y1) / (singleRange.x2 - singleRange.x1)) * singleRange.x1;
  return valueInRange;
}

function getLatterlyDate(numberOfHours: number): Date {
  const latteryHours = 1 * numberOfHours;
  let latterlyDate = new Date();
  latterlyDate.setUTCHours(0, 0, 0, 0);
  latterlyDate = new Date(latterlyDate.getTime() - latteryHours * 60 * 60 * 1000);

  return latterlyDate;
}
