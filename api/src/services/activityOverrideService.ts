import { ActivityOverrideRepository, activityOverrideRepository } from '../repository/activityOverrideRepository';
import { ActivityOverride } from '../models/domain/ActivityOverride';

export class ActivityOverrideService {
  private readonly repository: ActivityOverrideRepository;

  constructor(repository: ActivityOverrideRepository) {
    this.repository = repository;
  }

  /**
   * Get all activity-overrides from the DB.
   */
  async getAll(): Promise<ActivityOverride[]> {
    return this.repository.getAll();
  }

  /**
   * Get activity-override by activityId.
   * @param activityId Activity ID.
   */
  async getByActivityId(activityId: number): Promise<ActivityOverride> {
    if (!activityId) throw new Error(`getByActivityId(): Given 'activityId' is null or undefined.`);

    return this.repository.getByActivityId(activityId);
  }

  /**
   * Updates activity override properties that override a Strava activity.
   * @param activityOverride
   */
  async update(activityOverride: ActivityOverride): Promise<boolean> {
    if (!activityOverride.activityId) return false;

    return this.repository.update(activityOverride);
  }
}

export const activityOverrideService = new ActivityOverrideService(activityOverrideRepository);
