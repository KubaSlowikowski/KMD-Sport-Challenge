import { UserSetting } from '../models/domain/UserSetting';
import { Collection } from 'mongodb';
import { getUserSettingsCollection } from '../db/mongoDBAdapter';

class UserSettingsService {
  async getAllByAthleteId(athleteId: number): Promise<UserSetting[]> {
    if (!athleteId) return null;

    const collection: Collection<UserSetting> = await getUserSettingsCollection();
    const settings: UserSetting[] = await collection.find({ athleteId: athleteId }).toArray();

    return settings;
  }

  async getByNameAndAthleteId(settingName: string, athleteId: number): Promise<UserSetting> {
    if (!athleteId || !settingName) return null;

    const collection: Collection<UserSetting> = await getUserSettingsCollection();
    const setting: UserSetting = await collection.findOne({
      athleteId: athleteId,
      name: settingName,
    });

    return setting;
  }

  async save(userSetting: UserSetting): Promise<boolean> {
    if (!this.isValid(userSetting)) return false;

    const collection: Collection<UserSetting> = await getUserSettingsCollection();
    const result = await collection.updateOne(
      { athleteId: userSetting.athleteId },
      { $set: userSetting },
      { upsert: true },
    );

    if (result.acknowledged) {
      console.log(
        `User setting with name='${userSetting.name}' successfully saved for athlete with ID='${userSetting.athleteId}'.`,
      );
    } else {
      console.log(
        `Failed to save setting with name='${userSetting.name}' for athlete with ID='${userSetting.athleteId}'.`,
      );
    }

    return result.acknowledged;
  }

  async delete(settingName: string, athleteId: number): Promise<boolean> {
    if (!athleteId || !settingName) return null;

    const collection: Collection<UserSetting> = await getUserSettingsCollection();
    const result = await collection.deleteOne({
      name: settingName,
      athleteId: athleteId,
    });

    if (result.acknowledged) {
      console.log(`User setting with name='${settingName}' successfully deleted for athlete with ID='${athleteId}'.`);
    } else {
      console.log(`Failed to delete setting with name='${settingName}' for athlete with ID='${athleteId}'.`);
    }

    return result.acknowledged;
  }

  isValid(userSetting: UserSetting): boolean {
    if (!userSetting || !userSetting.athleteId || !userSetting.name || !('value' in userSetting)) {
      return false;
    }

    return true;
  }
}

export const userSettingsService = new UserSettingsService();
