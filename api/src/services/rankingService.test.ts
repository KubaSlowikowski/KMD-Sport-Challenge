import { DetailedActivityBuilder } from '../strava/test/detailed-activity.builder';
import { calculateActivityPoints } from './rankingService';
import { detailedActivityMapper } from '../mapper/detailedActivityMapper';
import { DetailedActivity } from '../strava/models/detailed-activity';

describe('rankingService', () => {
  const builder = new DetailedActivityBuilder();

  it('should calculate bike ride', () => {
    const EXPECTED_POINTS_FOR_BIKE = 46.5;
    const bikeRide: DetailedActivity = builder.ofType('Ride').build();

    const points = calculateActivityPoints(detailedActivityMapper.mapDetailedActivity(bikeRide));

    expect(Math.abs(EXPECTED_POINTS_FOR_BIKE - points)).toBeLessThan(0.5);
  });

  it('should calculate workout', () => {
    const EXPECTED_POINTS_FOR_WORKOUT = 110;
    const workout: DetailedActivity = builder.ofType('Workout').build();

    const points = calculateActivityPoints(detailedActivityMapper.mapDetailedActivity(workout));

    expect(Math.abs(EXPECTED_POINTS_FOR_WORKOUT - points)).toBeLessThan(0.5);
  });
});
