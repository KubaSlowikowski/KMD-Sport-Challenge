import { ObjectId } from 'mongodb';

// Webhooks event from Strava API.
export interface WebhookEventFromStrava {
  // Always either "activity" or "athlete."
  object_type: ObjectType;

  // Always "create," "update," or "delete."
  aspect_type: AspectType;

  // For activity events, the activity's ID. For athlete events, the athlete's ID.
  object_id: number;

  // The athlete's ID.
  owner_id: number;

  /**
   * For activity update events, keys can contain "title," "type," and "private,"
   * which is always "true" (activity visibility set to Only You) or "false" (activity visibility set to Followers Only or Everyone).
   * For app deauthorization events, there is always an "authorized" : "false" key-value pair.
   */
  updates: { title: string; type: string; private: boolean } | { authorized: 'false' }; // TODO add array

  // The push subscription ID that is receiving this event.
  subscription_id: number;

  // The time that the event occurred.
  event_time: number;
}

// An entity that is being saved to 'StravaWebhookEvent' table.
export interface StravaEventItem {
  id: string;

  // Describes when was the event document created.
  createdAt: string;

  // Always either "activity" or "athlete."
  object_type: ObjectType;

  // Always "create," "update," or "delete."
  aspect_type: AspectType;

  // For activity events, the activity's ID. For athlete events, the athlete's ID.
  object_id: number;

  // The athlete's ID.
  athlete_id: number;

  // Current status of the event.
  status: EventStatus;

  // Additional information about the status.
  statusDetails?: string;

  // When was the event document recently updated.
  updatedAt?: string;
}

// A type that is being saved to 'strava-webhook-events' collection.
export interface StravaEventDocument {
  _id: ObjectId;

  // Describes when was the event document created.
  createdAt: Date;

  // Always either "activity" or "athlete."
  object_type: ObjectType;

  // Always "create," "update," or "delete."
  aspect_type: AspectType;

  // For activity events, the activity's ID. For athlete events, the athlete's ID.
  object_id: number;

  // The athlete's ID.
  athlete_id: number;

  // Current status of the event.
  status: EventStatus;

  // Additional information about the status.
  statusDetails?: string;

  // When was the event document recently updated.
  updatedAt?: Date;
}

export enum ObjectType {
  ACTIVITY = 'activity',
  ATHLETE = 'athlete',
}

export enum AspectType {
  CREATE = 'create',
  UPDATE = 'update',
  DELETE = 'delete',
}

export enum EventStatus {
  NOT_PROCESSED = 'not processed',
  PROCESSED = 'processed',
  PROCESSING = 'processing',
  FAILED = 'failed',
  SKIPPED = 'skipped',
}
