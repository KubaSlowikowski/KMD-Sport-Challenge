export interface Athlete {
  athleteId: number;
  name: string;
  imageUrl: string;
  token_type: string;
  access_token: string;
  refresh_token: string;
  expires_at: number;
  expires_in: number;
  /** The athlete's sex. May take one of the following values: M, F */
  sex: 'M' | 'F';
}
