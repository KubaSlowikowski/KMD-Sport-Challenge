export interface ActivityOverride {
  // ID of the related athlete
  athleteId: number;

  // ID of the related Activity
  activityId: number;

  perceived_exertion: number;
}
