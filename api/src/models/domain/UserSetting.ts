import { ObjectId } from 'mongodb';

export interface UserSetting {
  _id?: ObjectId;

  // ID of strava athlete - to be removed and replaced by unique user UUID (should not be Strava related).
  athleteId: number;

  name: string;

  value: any;
}
