import { ActivityType } from '../../strava/types/activity.type';
import { PhotosSummary } from '../../strava/models/photos-summary';

export interface Activity {
  /** The unique identifier of the activity */
  id: number;
  /** The unique identifier of the athlete */
  athleteId: number;
  /** The name of the activity */
  name: string;
  /** The name of the activity */
  distance: number;
  /** The activity's moving time, in seconds */
  moving_time: number;
  /** The activity's elapsed time, in seconds */
  elapsed_time: number;
  /** The activity's total elevation gain. */
  total_elevation_gain: number;
  /** An instance of SportType. */
  sport_type: ActivityType;
  /** The time at which the activity was started. */
  start_date: string;
  /** The activity's average speed, in meters per second */
  average_speed: number;
  /** The description of the activity */
  description?: string;
  /** An instance of PhotosSummary */
  photos?: PhotosSummary;
  perceived_exertion?: number; // TODO to be removed since we don't want to set effort levels on activities
  /** Whether this activity is private */
  private: boolean;
}
