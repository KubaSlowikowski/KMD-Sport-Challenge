import { ActivityOverride } from '../models/domain/ActivityOverride';
import { DynamoDBClient } from '@aws-sdk/client-dynamodb';
import { dbClient, TableNames } from '../db/dynamoDB/dynamoDb';
import { GetCommand, GetCommandOutput, ScanCommandInput } from '@aws-sdk/lib-dynamodb';
import { isCommandOutput2xxSuccessful } from './utils';
import { dynamoDbHelper, DynamoDbHelper } from '../db/dynamoDB/dynamoDbHelper';

export class ActivityOverrideRepository {
  private readonly dbClient: DynamoDBClient;
  private readonly dynamoDbHelper: DynamoDbHelper;

  constructor(dbClient: DynamoDBClient, dynamoDbHelper: DynamoDbHelper) {
    this.dbClient = dbClient;
    this.dynamoDbHelper = dynamoDbHelper;
  }

  /**
   * Get all activity-overrides from the DB.
   */
  async getAll(): Promise<ActivityOverride[]> {
    const params: ScanCommandInput = {
      TableName: TableNames.ActivityOverride,
    };

    return this.dynamoDbHelper.getAllItems<ActivityOverride>(params);
  }

  /**
   * Get activity-override by activityId.
   * @param activityId Activity ID.
   */
  async getByActivityId(activityId: number): Promise<ActivityOverride> {
    const command = new GetCommand({
      TableName: TableNames.ActivityOverride,
      Key: {
        activityId: activityId,
      },
    });

    const response: GetCommandOutput = await this.dbClient.send(command);

    const success: boolean = isCommandOutput2xxSuccessful(response);
    if (!success) {
      console.log(`Failed to query an activity-override with id=${activityId}.`);
      console.log(response);
    }

    return response.Item as ActivityOverride;
  }

  /**
   * Updates activity override properties that override a Strava activity.
   * @param activityOverride
   */
  async update(activityOverride: ActivityOverride): Promise<boolean> {
    return this.dynamoDbHelper.save(activityOverride, TableNames.ActivityOverride);
  }
}

export const activityOverrideRepository = new ActivityOverrideRepository(dbClient, dynamoDbHelper);
