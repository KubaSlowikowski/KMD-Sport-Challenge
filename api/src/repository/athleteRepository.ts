import { Athlete } from '../models/domain/Athlete';
import { AthleteAuthTokens } from '../services/athleteService';
import { DynamoDBClient } from '@aws-sdk/client-dynamodb';
import { dbClient, TableNames } from '../db/dynamoDB/dynamoDb';
import {
  DeleteCommand,
  DeleteCommandOutput,
  GetCommand,
  GetCommandOutput,
  ScanCommandInput,
  UpdateCommand,
  UpdateCommandOutput,
} from '@aws-sdk/lib-dynamodb';
import { isCommandOutput2xxSuccessful } from './utils';
import { DynamoDbHelper, dynamoDbHelper } from '../db/dynamoDB/dynamoDbHelper';

export class AthleteRepository {
  private readonly dbClient: DynamoDBClient;
  private readonly dynamoDbHelper: DynamoDbHelper;

  constructor(dbClient: DynamoDBClient, dynamoDbHelper: DynamoDbHelper) {
    this.dbClient = dbClient;
    this.dynamoDbHelper = dynamoDbHelper;
  }

  /**
   * Find an athlete from database by his ID.
   * @param athleteId Athlete ID.
   * @return Returns promise with athlete (if exists), otherwise null.
   */
  async findAthleteById(athleteId: number): Promise<Athlete> {
    const command = new GetCommand({
      TableName: TableNames.Athlete,
      Key: {
        athleteId: athleteId,
      },
    });

    const response: GetCommandOutput = await this.dbClient.send(command);

    const success: boolean = isCommandOutput2xxSuccessful(response);
    if (!success) {
      console.log(`Failed to query an athlete with id=${athleteId}.`);
      console.log(response);
    }

    return response.Item as Athlete;
  }

  /**
   * Checks whether the athlete with given ID takes part in the challenge (is in a database) or not.
   * @param athleteId Athlete's ID.
   * @return Promise of 'true' if user takes part in the challenge, 'false' otherwise.
   */
  async athleteExists(athleteId: number): Promise<boolean> {
    const athlete: Athlete = await this.findAthleteById(athleteId);
    return !!athlete; // boom boom, you're boolean!
  }

  /**
   * Fetch all athletes from the database.
   * @return An array of athletes.
   */
  async getAllAthletes(): Promise<Athlete[]> {
    const params: ScanCommandInput = {
      TableName: TableNames.Athlete,
    };

    return this.dynamoDbHelper.getAllItems<Athlete>(params);
  }

  /**
   * Saves an athlete item in the database.
   * @param athlete Athlete object to be saved in DB.
   * @return Returns 'true' if update was successful, false otherwise.
   */
  async saveAthlete(athlete: Athlete): Promise<boolean> {
    return this.dynamoDbHelper.save(athlete, TableNames.Athlete);
  }

  /**
   * Updates OAuth2.0 Strava security tokens in DB for a given athlete.
   * @return Returns 'true' if update was successful, false otherwise.
   */
  async updateAuthTokensForAthlete(athleteId: number, authTokens: AthleteAuthTokens): Promise<boolean> {
    const command = new UpdateCommand({
      TableName: TableNames.Athlete,
      Key: {
        athleteId: athleteId,
      },
      UpdateExpression: `set 
        token_type = :token_type, 
        access_token = :access_token, 
        refresh_token = :refresh_token, 
        expires_at = :expires_at, 
        expires_in = :expires_in`,
      ConditionExpression: 'attribute_exists(athleteId)',
      ExpressionAttributeValues: {
        ':token_type': authTokens.token_type,
        ':access_token': authTokens.access_token,
        ':refresh_token': authTokens.refresh_token,
        ':expires_at': authTokens.expires_at,
        ':expires_in': authTokens.expires_in,
      },
      ReturnValues: 'NONE',
    });

    try {
      const response: UpdateCommandOutput = await this.dbClient.send(command);
      if (!isCommandOutput2xxSuccessful(response)) {
        throw new Error();
      }
      console.log(`Successfully set a new OAuth2.0 Strava tokens for an athlete with id='${athleteId}'.`);
      return true;
    } catch (error) {
      if (error.name === 'ConditionalCheckFailedException') {
        console.log(`No athlete item found with id='${athleteId}'.`);
      } else {
        console.error(`Failed set a new OAuth2.0 Strava tokens for an athlete with id='${athleteId}'.`);
        console.error(error);
        throw error;
      }
    }
  }

  /**
   * Deletes athlete by his ID.
   * @param athleteId ID of an athlete.
   * @return Returns 'true' if deletion was successful, false otherwise.
   */
  async deleteAthleteById(athleteId: number): Promise<boolean> {
    const command = new DeleteCommand({
      TableName: TableNames.Athlete,
      Key: {
        athleteId: athleteId,
      },
    });

    const response: DeleteCommandOutput = await this.dbClient.send(command);

    const success: boolean = isCommandOutput2xxSuccessful(response);
    if (!success) {
      console.log(response);
    }

    return success;
  }
}

export const athleteRepository = new AthleteRepository(dbClient, dynamoDbHelper);
