import { Activity } from '../models/domain/Activity';
import { DynamoDBClient } from '@aws-sdk/client-dynamodb';
import { dbClient, IndexNames, TableNames } from '../db/dynamoDB/dynamoDb';
import {
  DeleteCommand,
  GetCommand,
  GetCommandOutput,
  QueryCommand,
  QueryCommandOutput,
  ScanCommandInput,
} from '@aws-sdk/lib-dynamodb';
import { isCommandOutput2xxSuccessful } from './utils';
import { DynamoDbHelper, dynamoDbHelper } from '../db/dynamoDB/dynamoDbHelper';

export class ActivityRepository {
  private readonly dbClient: DynamoDBClient;
  private readonly dynamoDbHelper: DynamoDbHelper;

  constructor(dbClient: DynamoDBClient, dynamoDbHelper: DynamoDbHelper) {
    this.dbClient = dbClient;
    this.dynamoDbHelper = dynamoDbHelper;
  }

  /**
   * Get all athlete's activities from the database.
   * @param athleteId Athlete's ID,
   * @return Athlete's activities array.
   */
  async getAthleteActivities(athleteId: number): Promise<Activity[]> {
    const command = new QueryCommand({
      TableName: TableNames.Activity,
      IndexName: IndexNames.GSI_Activity_AthleteId,
      KeyConditionExpression: 'athleteId = :athleteId',
      ExpressionAttributeValues: { ':athleteId': athleteId },
    });

    const response: QueryCommandOutput = await this.dbClient.send(command);

    const success: boolean = isCommandOutput2xxSuccessful(response);
    if (!success) {
      console.log(`Failed to query activities for athlete with id=${athleteId}.`);
      console.log(response);
    }

    return response.Items as Activity[];
  }

  async getAllActivities(): Promise<Activity[]> {
    const params: ScanCommandInput = {
      TableName: TableNames.Activity,
    };

    return this.dynamoDbHelper.getAllItems<Activity>(params);
  }

  /**
   * Find particular activity by its ID.
   * @param activityId Activity unique ID.
   */
  async getById(activityId: number): Promise<Activity> {
    const command = new GetCommand({
      TableName: TableNames.Activity,
      Key: {
        id: activityId,
      },
    });

    const response: GetCommandOutput = await this.dbClient.send(command);

    const success: boolean = isCommandOutput2xxSuccessful(response);
    if (!success) {
      console.log(`Failed to query an activity with id=${activityId}.`);
      console.log(response);
    }

    return response.Item as Activity;
  }

  /**
   * Saves an activity item in the database.
   * @param activity Activity object to be saved in DB.
   * @return Returns 'true' if save was successful, false otherwise.
   */
  async saveActivity(activity: Activity): Promise<boolean> {
    const success: boolean = await this.dynamoDbHelper.save(activity, TableNames.Activity);
    if (success) {
      console.log(`Successfully saved an activity with id='${activity.id}'.`);
    } else {
      console.log(`Failed to save an activity with id='${activity.id}'.`);
    }

    return success;
  }

  /**
   * Saves multiple activities items in the database.
   * @param activities Activity objects array to be saved in DB.
   * @return Returns 'true' if operation was successful, false otherwise.
   */
  async saveActivities(activities: Activity[]): Promise<boolean> {
    let success = true;

    // Save activities asynchronously
    await Promise.allSettled(
      activities.map(async activity => {
        const result = await this.saveActivity(activity);
        if (!result) success = false;
      }),
    );

    return success;
  }

  async deleteActivityById(activityId: number): Promise<boolean> {
    const command = new DeleteCommand({
      TableName: TableNames.Activity,
      Key: {
        id: activityId,
      },
    });

    const response = await this.dbClient.send(command);

    const success: boolean = isCommandOutput2xxSuccessful(response);
    if (!success) {
      console.log(response);
    }

    return success;
  }
}

export const activityRepository = new ActivityRepository(dbClient, dynamoDbHelper);
