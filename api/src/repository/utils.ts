import { MetadataBearer } from '@smithy/types/dist-types/response';

export function isCommandOutput2xxSuccessful<OutputCommand extends MetadataBearer>(output: OutputCommand): boolean {
  if (!output) return false;

  const statusCode = output.$metadata.httpStatusCode;
  if (!statusCode) return false;

  return statusCode >= 200 && statusCode < 300;
}
