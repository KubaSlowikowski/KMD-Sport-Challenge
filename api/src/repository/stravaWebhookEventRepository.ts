import { DynamoDBClient } from '@aws-sdk/client-dynamodb';
import { dbClient, IndexNames, TableNames } from '../db/dynamoDB/dynamoDb';
import {
  PutCommand,
  PutCommandOutput,
  QueryCommand,
  QueryCommandOutput,
  UpdateCommand,
  UpdateCommandOutput,
} from '@aws-sdk/lib-dynamodb';
import { isCommandOutput2xxSuccessful } from './utils';
import { EventStatus, StravaEventItem, WebhookEventFromStrava } from '../models/domain/StravaWebhookEvents';
import crypto from 'crypto';

export class StravaWebhookEventRepository {
  private readonly dbClient: DynamoDBClient;

  constructor(dbClient: DynamoDBClient) {
    this.dbClient = dbClient;
  }

  async save(event: WebhookEventFromStrava): Promise<void> {
    const toSave: StravaEventItem = {
      id: crypto.randomUUID(),
      createdAt: new Date().toISOString(),
      object_type: event.object_type,
      aspect_type: event.aspect_type,
      object_id: event.object_id,
      athlete_id: event.owner_id,
      status: EventStatus.NOT_PROCESSED,
    };

    const command = new PutCommand({
      TableName: TableNames.StravaWebhookEvent,
      Item: toSave,
    });

    const response: PutCommandOutput = await this.dbClient.send(command);

    if (isCommandOutput2xxSuccessful(response)) {
      console.log(
        `Strava webhook event was saved successfully for athlete_id='${toSave.athlete_id}' and object_id='${toSave.object_id}'.`,
      );
    } else {
      throw new Error(`Failed to save Strava webhook event (event_time=${event.event_time})`);
    }
  }

  async exists(eventId: string): Promise<boolean> {
    const command: QueryCommand = new QueryCommand({
      TableName: TableNames.StravaWebhookEvent,
      KeyConditionExpression: 'id = :eventId',
      ExpressionAttributeValues: {
        ':eventId': eventId,
      },
    });

    const response: QueryCommandOutput = await this.dbClient.send(command);
    if (!isCommandOutput2xxSuccessful(response)) {
      throw new Error(`An error occurred during DynamoDB QueryCommand execution.`);
    }

    return response.Count !== 0;
  }

  async setStatus(eventId: string, status: EventStatus, message: string): Promise<void> {
    const command: UpdateCommand = new UpdateCommand({
      TableName: TableNames.StravaWebhookEvent,
      Key: { id: eventId },
      UpdateExpression: 'set #status = :status, statusDetails = :statusDetails, updatedAt = :updatedAt',
      ConditionExpression: 'attribute_exists(id)',
      ExpressionAttributeNames: { '#status': 'status' },
      ExpressionAttributeValues: {
        ':status': status,
        ':statusDetails': message,
        ':updatedAt': new Date().toISOString(),
      },
      ReturnValues: 'NONE',
    });

    try {
      const response: UpdateCommandOutput = await this.dbClient.send(command);
      if (isCommandOutput2xxSuccessful(response)) {
        console.log(`Successfully set a new status '${status}' for the event with _id='${eventId}'. ${message ?? ''}`);
      } else {
        throw new Error();
      }
    } catch (error) {
      if (error.name === 'ConditionalCheckFailedException') {
        console.log(`No event item found with id:${eventId}`);
      } else {
        console.error(`Failed to set a new status='${status}' for the event with _id='${eventId}'.`);
        console.error(error);
        throw error;
      }
    }
  }

  async getFailedOrNotProcessed(): Promise<StravaEventItem[]> {
    const [failedEvents, notProcessedEvents] = await Promise.all([
      this.getEventsByStatus(EventStatus.FAILED),
      this.getEventsByStatus(EventStatus.NOT_PROCESSED),
    ]);

    return [...failedEvents, ...notProcessedEvents];
  }

  private async getEventsByStatus(status: EventStatus): Promise<StravaEventItem[]> {
    const command: QueryCommand = new QueryCommand({
      TableName: TableNames.StravaWebhookEvent,
      IndexName: IndexNames.GSI_StravaWebhookEvent,
      ExpressionAttributeNames: { '#status': 'status' },
      KeyConditionExpression: '#status = :status',
      ExpressionAttributeValues: { ':status': status },
    });

    const response: QueryCommandOutput = await this.dbClient.send(command);
    if (!isCommandOutput2xxSuccessful(response)) {
      throw new Error(`An error occurred during DynamoDB QueryCommand execution.`);
    }

    return response.Items as StravaEventItem[];
  }
}

export const stravaWebhookEventRepository = new StravaWebhookEventRepository(dbClient);
