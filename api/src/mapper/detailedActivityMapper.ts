import { DetailedActivity } from '../strava/models/detailed-activity';
import { Activity } from '../models/domain/Activity';

export class DetailedActivityMapper {
  constructor() {}

  mapDetailedActivity(detailedActivity: DetailedActivity): Activity {
    const activity: Activity = {
      id: detailedActivity.id,
      athleteId: detailedActivity.athlete.id,
      name: detailedActivity.name,
      distance: detailedActivity.distance,
      moving_time: detailedActivity.moving_time,
      elapsed_time: detailedActivity.elapsed_time,
      total_elevation_gain: detailedActivity.total_elevation_gain,
      sport_type: detailedActivity.sport_type,
      start_date: detailedActivity.start_date,
      average_speed: detailedActivity.average_speed,
      description: detailedActivity.description,
      photos: detailedActivity.photos,
      perceived_exertion: detailedActivity.perceived_exertion,
      private: detailedActivity.private,
    };

    return activity;
  }
}

export const detailedActivityMapper: DetailedActivityMapper = new DetailedActivityMapper();
