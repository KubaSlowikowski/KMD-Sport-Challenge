export const CHALLENGE_START_DATE = '12/07/2024'; // DD/MM/YYYY
export const CHALLENGE_END_DATE = '30/09/2024'; // DD/MM/YYYY. If the challenge should be finished at 23:59:59 06/05/2024 value should be 07/05/2024
