export type ActivityType = 'timeExertionEffort' | 'timeEffort' | 'timePaceHillPowerFnEffort' | 'timePacePowerFnEffort';

export const MET_MIN = 1;
export const MET_MAX = 25;

// points / 1 minute = a * (exertion effort) ^ n + b;
export const EXERTION_CONFIG = {
  a: 1.10352,
  n: 1.20681,
  b: 1.96539,
};

export function getSportConfig(type: string | null) {
  return RANKING_CONFIG.find(r => r.sport_type === type) ?? RANKING_CONFIG.find(r => r.sport_type == null);
}

export const RANKING_CONFIG = [
  /*
        TODO
        EBikeRide EMountainBikeRide should use mets points from below table

        Activity	           MET      Value Description
        Leisurely E-Bike Ride  3.5	    Minimal effort, primarily motor-assisted
        Moderate E-Bike Ride   4.5-6	Moderate pedaling with motor assistance
        Vigorous E-Bike Ride   7-8	    High effort pedaling with motor assistance
        Non-Assisted Biking	   8-10	    No motor assistance, high intensity
    */
  ...[
    { sport_type: 'VirtualRide', name: 'Virtual Ride', tuningRatio: 1 },
    { sport_type: 'Ride', name: 'Bike ride', tuningRatio: 1 },
    { sport_type: 'GravelRide', name: 'Gravel Ride', tuningRatio: 1.2 },
    { sport_type: 'MountainBikeRide', name: 'Mountain Bike Ride', tuningRatio: 1.3 },
    { sport_type: 'EBikeRide', name: 'EBike Ride', tuningRatio: 0.3 },
    { sport_type: 'EMountainBikeRide', name: 'EMountain Bike Ride', tuningRatio: 0.3 },
  ].map(args => ({
    ...args,
    type: 'timePaceHillPowerFnEffort',
    metPowerFnA: 0.00385222,
    metPowerFnB: 3.22541,
    metPowerFnN: 2.34305,
    baseSpeedKmH: 15,
  })),
  ...[
    { sport_type: 'Run', name: 'Run', tuningRatio: 1 },
    { sport_type: 'TrailRun', name: 'Trail Run', tuningRatio: 1.125 },
  ].map(args => ({
    ...args,
    type: 'timePaceHillPowerFnEffort',
    metPowerFnA: 0.148047,
    metPowerFnB: 4.45934,
    metPowerFnN: 1.53949,
    baseSpeedKmH: 8,
  })),
  ...[
    { sport_type: 'Walk', name: 'Walk', tuningRatio: 1 },
    { sport_type: 'Hike', name: 'Hike', tuningRatio: 1.4 },
  ].map(args => ({
    ...args,
    type: 'timePaceHillPowerFnEffort',
    metPowerFnA: 0.120943,
    metPowerFnB: 1.07054,
    metPowerFnN: 1.92168,
    baseSpeedKmH: 6,
  })),
  {
    sport_type: 'Swim',
    name: 'Swim',
    type: 'timePacePowerFnEffort',
    metPowerFnA: 2.05676,
    metPowerFnB: 1.06474,
    metPowerFnN: 1.14781,
    tuningRatio: 1.5,
    baseSpeedKmH: 0.5,
  },
  {
    sport_type: 'Handcycle',
    name: 'Handcycle',
    type: 'timePaceHillPowerFnEffort',
    metPowerFnA: 0.02154,
    metPowerFnB: 4.77395,
    metPowerFnN: 2.323,
    tuningRatio: 1,
    baseSpeedKmH: 10,
  },
  {
    sport_type: 'Velomobile',
    name: 'Velomobile',
    type: 'timePaceHillPowerFnEffort',
    metPowerFnA: 0.00811867,
    metPowerFnB: 3.31735,
    metPowerFnN: 1.62183,
    tuningRatio: 1,
    baseSpeedKmH: 20,
  },
  {
    sport_type: 'Wheelchair',
    name: 'Wheelchair',
    type: 'timePaceHillPowerFnEffort',
    metPowerFnA: 0.252776,
    metPowerFnB: 1.7834,
    metPowerFnN: 1.36025,
    tuningRatio: 1,
    baseSpeedKmH: 6.5,
  },
  {
    sport_type: 'InlineSkate',
    name: 'Inline Skate',
    type: 'timePaceHillPowerFnEffort',
    metPowerFnA: 0.00123992,
    metPowerFnB: 5.22743,
    metPowerFnN: 2.83568,
    tuningRatio: 1,
    baseSpeedKmH: 14.4,
  },
  {
    sport_type: 'RollerSki',
    name: 'Roller Ski',
    type: 'timePaceHillPowerFnEffort',
    metPowerFnA: 0.0674544,
    metPowerFnB: 1.24229,
    metPowerFnN: 1.61808,
    tuningRatio: 1,
    baseSpeedKmH: 14.4,
  },
  {
    sport_type: 'VirtualRow',
    name: 'Virtual Row',
    type: 'timePacePowerFnEffort',
    metPowerFnA: 0.00080215,
    metPowerFnB: 4.09143,
    metPowerFnN: 3.27811,
    tuningRatio: 1,
    baseSpeedKmH: 3.21,
  },
  ...[
    { sport_type: 'NordicSki', name: 'Nordic Ski' },
    { sport_type: 'BackcountrySki', name: 'Backcountry Ski' },
  ].map(args => ({
    ...args,
    type: 'timePaceHillPowerFnEffort',
    metPowerFnA: 4.72416,
    metPowerFnB: -2.05657,
    metPowerFnN: 0.433778,
    tuningRatio: 1,
    baseSpeedKmH: 4,
  })),
  {
    sport_type: 'VirtualRun',
    name: 'Virtual Run',
    type: 'timePaceHillPowerFnEffort',
    metPowerFnA: 0.0605975,
    metPowerFnB: 5.53953,
    metPowerFnN: 1.85289,
    tuningRatio: 1,
    baseSpeedKmH: 8,
  },
  {
    sport_type: 'Snowshoe',
    name: 'Snowshoe',
    type: 'timePaceHillPowerFnEffort',
    metPowerFnA: 0.297368,
    metPowerFnB: 1.02901,
    metPowerFnN: 1.90616,
    tuningRatio: 1,
    baseSpeedKmH: 6,
  },
  ...[{ sport_type: 'Rowing' }, { sport_type: 'Canoeing' }, { sport_type: 'Kayaking' }].map(r => ({
    ...r,
    name: 'Rowing',
    type: 'timePacePowerFnEffort',
    metPowerFnA: 0.000162714,
    metPowerFnB: 1.631,
    metPowerFnN: 4.89614,
    tuningRatio: 1,
    baseSpeedKmH: 3.21,
  })),
  ...[
    { sport_type: 'Yoga', name: 'Yoga' },
    { sport_type: 'Pickleball', name: 'Pickleball' },
    { sport_type: 'Racquetball', name: 'Racquetball' },
    { sport_type: 'Skateboard', name: 'Skateboard' },
    { sport_type: 'WeightTraining', name: 'Weight Training' },
    { sport_type: 'Workout', name: 'Workout' },
    { sport_type: 'Tennis', name: 'Tennis' },
    { sport_type: 'Soccer', name: 'Soccer' },
    { sport_type: 'Pilates', name: 'Pilates' },
    { sport_type: 'StandUpPaddling', name: 'Stand Up Paddling' },
    { sport_type: 'StairStepper', name: 'Stair Stepper' },
    { sport_type: 'Squash', name: 'Squash' },
    { sport_type: 'HighIntensityIntervalTraining', name: 'High Intensity Interval Training' },
    { sport_type: 'Crossfit', name: 'Crossfit' },
    { sport_type: 'Elliptical', name: 'Elliptical' },
    { sport_type: 'RockClimbing', name: 'Rock Climbing' },
  ].map(args => ({
    ...args,
    type: 'timeExertionEffort',
    tuningRatio: 0.7,
  })),
  ...[
    { sport_type: 'Kitesurf', name: 'Kitesurf' },
    { sport_type: 'Windsurf', name: 'Windsurf' },
  ].map(args => ({
    ...args,
    type: 'timeEffort',
    timeRatio: 7,
    tuningRatio: 1,
  })),
  {
    sport_type: 'TableTennis',
    name: 'Table Tennis',
    type: 'timeEffort',
    timeRatio: 4,
    tuningRatio: 1,
  },
  ...[
    { sport_type: 'AlpineSki', name: 'Alpine Ski' },
    { sport_type: 'Snowboard', name: 'Snowboard' },
  ].map(args => ({
    ...args,
    type: 'timeEffort',
    timeRatio: 5.3,
    tuningRatio: 1,
  })),
  {
    sport_type: 'IceSkate',
    name: 'Ice Skate',
    type: 'timeEffort',
    timeRatio: 5.5,
    tuningRatio: 1,
  },
  {
    sport_type: 'Golf',
    name: 'Golf',
    type: 'timeEffort',
    timeRatio: 4.8,
    tuningRatio: 1,
  },
  {
    sport_type: 'Badminton',
    name: 'Badminton',
    type: 'timeEffort',
    timeRatio: 5.5,
    tuningRatio: 1,
  },
  {
    sport_type: 'Sail',
    name: 'Sail',
    type: 'timeEffort',
    timeRatio: 2.5,
    tuningRatio: 1,
  },
  // Default value for missing and upcoming sports
  {
    sport_type: null,
    name: null,
    type: 'timeEffort',
    timeRatio: 2.5,
    tuningRatio: 1,
  },
];
