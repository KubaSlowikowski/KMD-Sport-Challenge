import express from 'express';
import {
  ENDPOINTS,
  GRANT_TYPES,
  FRONTEND_PROD_URL,
  FRONTEND_LOCAL_URL,
  TEST_MODE_ENABLED,
  SCOPES,
} from '../utils/utils';
import axios from 'axios';
import jwt from 'jsonwebtoken';
import * as stravaService from '../services/stravaService';
import { Athlete } from '../models/domain/Athlete';
import { Activity } from '../models/domain/Activity';
import { stravaApiService } from '../strava/service/StravaApiService';
import { athleteService } from '../services/athleteService';
import { activityService } from '../services/activityService';
export const router = express.Router();

router.get('/', async (req, res) => {
  // TODO check user accepted scopes
  const { code } = req.query;
  const callbackHostUrl = req.query['callbackHostUrl'] as string;
  /*
    'amp;' is required here since Strava is adding this token to query parameters 
  */
  const callbackPath = req.query['amp;callbackPath'] as string;
  const redirectTo = req.query['amp;redirectTo'] as string;

  const acceptedScopes: string = req.query.scope as string;
  if (!callbackHostUrl) {
    return res.status(400).json({ error: "'callbackHostUrl' param required" });
  }

  if (!callbackPath) {
    return res.status(400).json({ error: "'callbackPath' param required" });
  }

  if (req.query.error === 'access_denied' || !areRequiredScopesAccepted(acceptedScopes)) {
    if (TEST_MODE_ENABLED) {
      return res.redirect(FRONTEND_LOCAL_URL);
    } else {
      return res.redirect(FRONTEND_PROD_URL);
    }
  }

  const tokenEndpoint = ENDPOINTS.STRAVA_TOKEN;
  const requestBody = {
    client_id: process.env.CLIENT_ID,
    client_secret: process.env.CLIENT_SECRET,
    code,
    grant_type: GRANT_TYPES.AUTHORIZATION_CODE,
  };

  try {
    const response = await axios.post(tokenEndpoint, requestBody);
    const { token_type, access_token, refresh_token, expires_at, expires_in, athlete } = response.data;

    const athleteData = await stravaApiService.getLoggedInAthlete(access_token);

    const data: Athlete = {
      token_type: token_type,
      access_token: access_token,
      refresh_token: refresh_token,
      expires_at: expires_at,
      expires_in: expires_in,
      athleteId: athlete.id,
      name: `${athlete.firstname} ${athlete.lastname}`,
      imageUrl: athlete.profile,
      sex: athleteData.data.sex,
    };

    if (!(await athleteService.athleteExists(data.athleteId))) {
      const updateSuccessful: boolean = await athleteService.saveAthlete(data);
      if (updateSuccessful) console.log(`User '${data.name}' registered.`);

      await fetchActivitiesOnStart(data);
    }

    const payload = {
      athleteName: data.name,
      athleteId: data.athleteId,
    };
    const jwt_token = jwt.sign(payload, process.env.JWT_PRIVATE_KEY);

    const redirectUrl = new URL(callbackPath, callbackHostUrl);
    redirectUrl.searchParams.set('token', jwt_token);
    if (redirectTo != null) {
      redirectUrl.searchParams.set('redirectTo', redirectTo);
    }

    res.redirect(redirectUrl.href);
  } catch (error) {
    const message: string = 'Error occurred during user registration.';
    console.error(message, { error: error });
    res.json({ error: message });
  }

  function areRequiredScopesAccepted(acceptedScopes: string): boolean {
    const readAccepted: boolean = acceptedScopes.includes(SCOPES.READ);
    const activityReadAccepted: boolean = acceptedScopes.includes(SCOPES.ACTIVITY_READ);
    const activityReadAllAccepted: boolean = acceptedScopes.includes(SCOPES.ACTIVITY_READ_ALL);

    return readAccepted && (activityReadAccepted || activityReadAllAccepted);
  }
});

async function fetchActivitiesOnStart(athlete: Athlete): Promise<void> {
  const activities: Activity[] = await stravaService.getAthleteActivities(athlete);

  const success: boolean = await activityService.saveActivities(activities);
  if (success) {
    console.log(`Initialized activities data for athlete with id=${athlete.athleteId}.`);
  } else {
    console.log(`Failed to initialize activities data for athlete with id=${athlete.athleteId}.`);
  }
}
