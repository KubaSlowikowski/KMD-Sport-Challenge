import express from 'express';
import { securityMiddleware } from '../security/securityMiddleware';
import {
  calculateActivityPoints,
  getActivityEffort,
  getCalculationParams,
  isActivitySupported,
} from '../services/rankingService';
import { STRINGS } from '../utils/strings';
import { Athlete } from '../models/domain/Athlete';
import { Activity } from '../models/domain/Activity';
import { getSportConfig } from '../config/rankingConfig';
import { athleteService } from '../services/athleteService';
import { activityService } from '../services/activityService';
import { securityService } from '../services/securityService';
import { activityOverrideService } from '../services/activityOverrideService';

export const router = express.Router();

/**
 * Get athlete's all activities.
 */
router.get('/:athleteId/activities', securityMiddleware, async (req, res) => {
  const invokerAthleteId: number = res.locals.athleteId;
  const athleteId = Number(req.params.athleteId === 'me' ? invokerAthleteId : req.params.athleteId);

  const athlete: Athlete = await athleteService.findAthleteById(athleteId); // FIXME remove athlete fetching
  if (!athlete) {
    const message = `Athlete with id='${athleteId}' does not exist.`;
    console.log(message);
    return res.status(404).json({ message: message });
  }

  await securityService.refreshToken(athlete.refresh_token, athleteId);
  try {
    const activities: Activity[] = await activityService.getAthleteActivities(athleteId);
    const mappedActivities: MappedActivity[] = mapActivities(activities);
    const sortedActivities: MappedActivity[] = [...mappedActivities].sort(
      (a, b) => new Date(b.createdDate).getTime() - new Date(a.createdDate).getTime(),
    );
    res.send(sortedActivities);
  } catch (error) {
    console.log(error);
    if (error.response?.status === 429) {
      res.status(429).json({ error: error.response.statusText });
    }

    res.status(500).json({ error: 'error in /athlete/activities endpoint', ...error });
  }
});

/**
 * Get single activity by its ID.
 */
router.get('/:athleteId/activity/:activityId', securityMiddleware, async (req, res) => {
  const invokerAthleteId: number = res.locals.athleteId;
  const athleteId: number = Number(req.params.athleteId === 'me' ? invokerAthleteId : req.params.athleteId);
  const activityId: number = +req.params.activityId;

  const athlete: Athlete = await athleteService.findAthleteById(athleteId);
  if (!athlete) {
    const message = `Athlete with id='${athleteId}' does not exist.`;
    console.log(message);
    return res.status(404).json({ message: message });
  }

  await securityService.refreshToken(athlete.refresh_token, athleteId);
  try {
    const activities: Activity[] = await activityService.getAthleteActivities(athleteId);
    const activity: Activity = activities.find(activity => activity.id === activityId);
    const mappedActivities: MappedActivity = mapActivity(activity);

    return res.send(mappedActivities);
  } catch (error) {
    console.error(error);
    if (error.response?.status === 429) {
      res.status(429).json({ error: error.response.statusText });
    }

    res.status(500).json({ error: 'error in /athlete/activities endpoint', ...error });
  }
});

/**
 * Get athlete's activity details.
 */
router.get('/:athleteId/activity/:activityId/details', securityMiddleware, async (req, res) => {
  const invokerAthleteId: number = res.locals.athleteId;
  const athleteId = Number(req.params.athleteId === 'me' ? invokerAthleteId : req.params.athleteId);
  const activityId = Number(req.params.activityId);

  const activities: Activity[] = await activityService.getAthleteActivities(athleteId);
  const activity: Activity = activities.find(a => a.id === activityId);
  const calculation = getCalculationParams(activity);

  const result = {
    // TODO add typing (?)
    createdDate: activity.start_date,
    distance: activity.distance,
    id: activity.id,
    movingTime: activity.moving_time,
    name: activity.name,
    supported: isActivitySupported(activity),
    type: activity.sport_type,
    elevationGain: activity.total_elevation_gain,
    averageSpeed: activity.average_speed * 3.6,
    calculation,
    requiresEffort: calculation.calculationType === 'timeExertionEffort',
  };

  res.send(result);
});

/**
 * Refresh athlete's activity - fetch new data from Strava.
 */
router.put('/:athleteId/activity/:activityId/refresh', securityMiddleware, async (req, res) => {
  const invokerAthleteId: number = res.locals.athleteId;
  const athleteId = Number(req.params.athleteId === 'me' ? invokerAthleteId : req.params.athleteId);
  const activityId = Number(req.params.activityId);

  const activityBefore: Activity = await activityService.getById(activityId);

  await activityService.refreshActivity(athleteId, activityBefore.id);

  const activity: Activity = await activityService.getById(activityId);

  const result = {
    createdDate: activity.start_date,
    distance: activity.distance,
    id: activity.id,
    movingTime: activity.moving_time,
    name: activity.name,
    supported: isActivitySupported(activity),
    type: activity.sport_type,
    elevationGain: activity.total_elevation_gain,
    averageSpeed: activity.average_speed * 3.6,
    calculation: getCalculationParams(activity),
  };

  res.send(result);
});

/**
 * Deletes an athlete.
 */
router.delete('', securityMiddleware, async (req, res) => {
  const athleteId: number = res.locals.athleteId;
  try {
    await athleteService.deleteAthleteById(athleteId);
  } catch (e) {
    if (e.message === STRINGS.ATHLETE_NOT_FOUND) {
      return res.status(404).json({ message: 'Athlete not found.' });
    } else {
      return res.sendStatus(500);
    }
  }

  res.status(200).json({ message: 'Athlete deleted.' });
});

/**
 * Determines whether an athlete exists or not.
 */
router.get('/exists', securityMiddleware, async (req, res) => {
  const athleteId: number = res.locals.athleteId;

  if (await athleteService.athleteExists(athleteId)) {
    res.sendStatus(204);
  } else {
    res.status(404).json({ message: 'Athlete does not exist in DB.' });
  }
});

/**
 * Find a list of activities with missing exertion level.
 */
router.get('/:athleteId/missingExertionLevelActivities', securityMiddleware, async (req, res) => {
  const invokerAthleteId: number = res.locals.athleteId;
  const athleteId = Number(req.params.athleteId === 'me' ? invokerAthleteId : req.params.athleteId);

  if (!(await athleteService.athleteExists(athleteId))) {
    const message = `Athlete with id='${athleteId}' does not exist.`;
    console.log(message);
    return res.status(404).json({ message: message });
  }
  const activitiesWithMissingExertionLevel: Activity[] =
    await activityService.getActivitiesWithMissingEffortByAthleteId(athleteId);
  const mappedActivities: MappedActivity[] = mapActivities(activitiesWithMissingExertionLevel);
  const sortedActivities: MappedActivity[] = [...mappedActivities].sort(
    (a, b) => new Date(b.createdDate).getTime() - new Date(a.createdDate).getTime(),
  );

  res.send(sortedActivities);
});

/**
 * Updates activity exertion level.
 */
router.patch('/:athleteId/activity/:activityId/setExertion', securityMiddleware, async (req, res) => {
  const invokerAthleteId: number = res.locals.athleteId;
  const athleteId = Number(req.params.athleteId === 'me' ? invokerAthleteId : req.params.athleteId);
  const activityId = +req.params.activityId;
  const { exertionLevel } = req.body;

  if (!isValid(exertionLevel, activityId)) return res.status(400);

  if (!(await athleteService.athleteExists(athleteId))) {
    const message = `Athlete with id='${athleteId}' does not exist.`;
    console.log(message);
    return res.status(404).json({ message: message });
  }

  const activity: Activity = await activityService.getById(activityId);
  if (!activity) {
    const message: string = `Activity with id='${activityId}' does not exist for an athlete with id='${athleteId}'.`;
    console.log(message);
    return res.status(404).json({ message: message });
  }

  await activityOverrideService.update({
    athleteId: athleteId,
    activityId: activityId,
    perceived_exertion: exertionLevel,
  });

  const updatedActivity: Activity = await activityService.getById(activityId);
  const result: MappedActivity = mapActivity(updatedActivity);

  res.send(result);

  function isValid(exertionLevel: any, activityId: any): boolean {
    if (!exertionLevel || !activityId) {
      const message = `Wrong request data. 'exertionLevel' or 'activityId' are null or undefined.`;
      res.status(400).json({ message: message });
      return false;
    }
    if (isNaN(Number(exertionLevel))) {
      const message = `Wrong request data. 'exertionLevel' is not a number.`;
      res.status(400).json({ message: message });
      return false;
    }
    if (Number(exertionLevel) < 0 || Number(exertionLevel) > 10) {
      const message = `Wrong request data. 'exertionLevel' value should be between 1-10.`;
      res.status(400).json({ message: message });
      return false;
    }

    return true;
  }
});

type MappedActivity = {
  id: number;
  name: string;
  createdDate: string;
  type: string;
  points: number;
  supported: boolean;
  distance: number;
  movingTime: number;
  effort: number | null;
  requiresEffort: boolean;
  photoUrl?: string;
};

function mapActivity(activity: Activity): MappedActivity {
  return {
    id: activity.id,
    name: activity.name,
    createdDate: activity.start_date,
    type: activity.sport_type,
    points: calculateActivityPoints(activity),
    supported: isActivitySupported(activity),
    distance: activity.distance,
    movingTime: activity.moving_time,
    effort: getActivityEffort(activity),
    requiresEffort: getSportConfig(activity.sport_type)?.type === 'timeExertionEffort',
    photoUrl: activity.photos?.primary?.urls,
  };
}

function mapActivities(activities: Activity[]): MappedActivity[] {
  return activities.map(activity => mapActivity(activity));
}
