import express from 'express';
import { securityMiddleware } from '../security/securityMiddleware';
import { ManualFeedAddDto } from 'src/features/manual-feed';
import { addManualFeed } from '../features/feed-builder/logic/add-manual-feed.function';
import { feedService } from '../features/feed/database/feedService';
import { FeedModel } from '../features/feed/models';

export const router = express.Router();

router.get('/:page', securityMiddleware, async (req, res) => {
  const page = Number(req.params.page);
  const pageSize = 15;

  const feeds: FeedModel[] = await feedService.getPage(page, pageSize);
  const count = feeds.length;

  res.send({ items: feeds, count, pageSize });
});

router.post('/manual', securityMiddleware, async (req, res) => {
  const dto = req.body as ManualFeedAddDto;
  const result = await addManualFeed(dto);
  res.send(result);
});
