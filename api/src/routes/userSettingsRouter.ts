import express from 'express';
import { securityMiddleware } from '../security/securityMiddleware';
import { UserSetting } from '../models/domain/UserSetting';
import { userSettingsService } from '../services/userSettingsService';

export const router = express.Router();

/**
 * Get all user settings by athlete ID.
 */
router.get('/all', securityMiddleware, async (req, res, next) => {
  try {
    const athleteId: number = +req.query.athleteId;
    if (!athleteId) {
      return res.status(400).json({ message: 'Athlete ID not found.' });
    }

    const userSettings: UserSetting[] = await userSettingsService.getAllByAthleteId(athleteId);

    return res.send(userSettings);
  } catch (error) {
    next(error);
  }
});

/**
 * Get user setting by setting-name and athleteID.
 */
router.get('/', securityMiddleware, async (req, res, next) => {
  try {
    const settingName: string = req.query.settingName;
    const athleteId: number = +req.query.athleteId;

    if (!athleteId) return res.status(400).json({ message: 'Athlete ID not found.' });
    if (!settingName) return res.status(400).json({ message: 'Setting name not found.' });

    const userSetting: UserSetting = await userSettingsService.getByNameAndAthleteId(settingName, athleteId);

    return res.send(userSetting);
  } catch (error) {
    next(error);
  }
});

/**
 * Save/update user setting for the given athlete.
 */
router.post('/save', securityMiddleware, async (req, res, next) => {
  try {
    const athleteId: number = +req.query.athleteId;
    const toSave = req.body as UserSetting;

    if (!athleteId) return res.status(400).json({ message: 'Athlete ID not found.' });
    if (!userSettingsService.isValid(toSave)) return res.status(400).json({ message: 'Invalid request data.' });

    const success: boolean = await userSettingsService.save(toSave);

    if (success) {
      return res.sendStatus(204);
    } else {
      return res.status(500).json({ message: 'Something went wrong during user-setting saving.' });
    }
  } catch (error) {
    next(error);
  }
});

/**
 * Delete user-setting by its name and athlete ID.
 */
router.delete('', securityMiddleware, async (req, res, next) => {
  try {
    const athleteId: number = +req.query.athleteId;
    const settingName: string = req.query.settingName;

    if (!athleteId) return res.status(400).json({ message: 'Athlete ID not found.' });
    if (!settingName) return res.status(400).json({ message: 'Setting name not found.' });

    const success: boolean = await userSettingsService.delete(settingName, athleteId);
    if (success) {
      return res.sendStatus(204);
    } else {
      return res.status(500).json({
        message: 'Something went wrong during user-setting deletion.',
      });
    }
  } catch (error) {
    next(error);
  }
});
