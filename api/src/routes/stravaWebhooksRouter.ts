import { WebhookEventFromStrava } from '../models/domain/StravaWebhookEvents';
import { stravaWebhookEventService } from '../services/stravaWebhookEventService';

const express = require('express');
const { ENDPOINTS } = require('../utils/utils');
const axios = require('axios');

export const router = express.Router();

router.get('/viewSubscription', async (req, res) => {
  const subscriptionEndpoint = ENDPOINTS.STRAVA_WEBHOOK_PUSH_SUBSCRIPTION;
  const queryParams = new URLSearchParams({
    client_id: process.env.CLIENT_ID,
    client_secret: process.env.CLIENT_SECRET,
  });

  const url = `${subscriptionEndpoint}?${queryParams}`;

  try {
    const axiosRes = await axios.get(url);
    res.status(200).json(axiosRes.data);
  } catch (e) {
    const message = 'An error occurred during the request to view a strava subscription.';
    console.error(message, e);
    res.status(404).json({ message: message });
  }
});

router.post('/createSubscription', async (req, res) => {
  console.log('Received a request to create a new Strava webhook subscription');

  const subscriptionEndpoint = ENDPOINTS.STRAVA_WEBHOOK_PUSH_SUBSCRIPTION;
  const queryParams = new URLSearchParams({
    client_id: process.env.CLIENT_ID,
    client_secret: process.env.CLIENT_SECRET,
    callback_url: process.env.STRAVA_WEBHOOK_CALLBACK_URL,
    verify_token: process.env.STRAVA_WEBHOOK_VERIFY_TOKEN,
  });
  const url = `${subscriptionEndpoint}?${queryParams}`;

  try {
    await axios.post(url);
    const message = 'Strava webhooks subscription created successfully :).';
    console.log(message);
    res.status(200).json({ message: message });
  } catch (e) {
    const message = 'An error occurred during strava subscription creation.';
    console.error(message, e);
    res.status(500).json({ message: message });
  }
});

router.get('/callback', async (req, res) => {
  const mode = req.query['hub.mode'];
  const challenge = req.query['hub.challenge'];
  const verifyToken = req.query['hub.verify_token'];

  // Checks if a token and mode is in the query string of the request
  if (!mode || !verifyToken) {
    console.error(`'hub.mode' or 'hub.verify_token' parameters not present in subscription validation request.`);
    return res.status(400);
  }

  // Verifies that the mode and token sent are valid
  if (mode !== 'subscribe' || verifyToken !== process.env.STRAVA_WEBHOOK_VERIFY_TOKEN) {
    console.error(`'hub.mode' or 'hub.verify_token' parameters are invalid in subscription validation request.`);
    return res.status(403);
  }

  // Responds with the challenge token from the request
  console.log('Webhook callback from Strava verified.');
  return res.status(200).json({ 'hub.challenge': challenge });
});

router.post('/callback', async (req, res) => {
  console.log('Webhook event received!:');
  console.log(req.body);

  const event: WebhookEventFromStrava = req.body;
  try {
    await stravaWebhookEventService.save(event);
    res.sendStatus(200);
  } catch (error) {
    console.error(error.message);
    res.sendStatus(500);
  }
});

router.delete('/unsubscribe/:id', async (req, res) => {
  const { id } = req.params;

  const subscriptionEndpoint = ENDPOINTS.STRAVA_WEBHOOK_PUSH_SUBSCRIPTION;
  const queryParams = new URLSearchParams({
    client_id: process.env.CLIENT_ID,
    client_secret: process.env.CLIENT_SECRET,
  });
  const url = `${subscriptionEndpoint}/${id}?${queryParams}`;

  try {
    await axios.delete(url);
    res.status(200).json({ message: 'Subscription deleted successfully.' });
  } catch (e) {
    if (e.response.status === 404) {
      const message = `Subscription with id=${id} not found.`;
      console.log(message);
      return res.status(404).json({ message: message });
    }
    console.error(
      `An error occurred during deleting a Strava webhooks subscription (id=${id}). Error message: '${e.response.data.message}'`,
    );
    res.sendStatus(500);
  }
});
