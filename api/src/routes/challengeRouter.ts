import express from 'express';
import { securityMiddleware } from '../security/securityMiddleware';
export const router = express.Router();
import { getRanking } from '../services/rankingService';

router.get('/ranking', securityMiddleware, async (req, res) => {
  const ranking = await getRanking();
  res.send(ranking);
});
