import express from 'express';
import { invokeActivityAddedFeedFlow } from '../features/feed-builder/logic/invoke-activity-added-feed-flow.function';
import { sumPoints, getRanking, Ranking } from '../services/rankingService';
import { activityService } from '../services/activityService';
import { athleteService } from '../services/athleteService';

export const router = express.Router();

router.post('/feed/:athleteId/:activityId', async (req, res) => {
  const athleteId = Number(req.params.athleteId);
  const activityId = Number(req.params.activityId);
  const simulate = req.query['simulate'] != null ? req.query['simulate'] === 'true' : false;
  const keepHistory = req.query['keepHistory'] != null ? req.query['keepHistory'] === 'true' : false;

  const feed = await invokeActivityAddedFeedFlow(athleteId, activityId, {
    simulate,
    keepHistory,
  });

  res.send(feed);
});

router.post('/feed/runForAll', async (req, res) => {
  const [activities, athletes] = await Promise.all([
    activityService.getAllActivities(),
    athleteService.getAllAthletes(),
  ]);

  const result = [];
  for (const activity of activities) {
    try {
      result.push(
        await invokeActivityAddedFeedFlow(activity.athleteId, activity.id, {
          cachedAthletes: athletes,
          cachedActivities: activities,
          keepHistory: true,
        }),
      );
    } catch (e) {
      console.log(`An error occurred during feed generation for all activities.`);
      console.error(e);
    }
  }

  res.send(result.filter(r => r != null));
});

router.get('/export/activities', async (req, res) => {
  // TODO to be refactored since we extracted activities from an athlete table.
  // const athletes = await getAllAthletes();
  let result = [];
  // const athletesSorted = [...athletes].sort((a, b) => a.name > b.name ? 1 : (a.name < b.name ? -1 : 0));
  // for (const athlete of athletesSorted) {
  //     const activitiesSorted = athlete.activities.sort((a, b) => a.start_date > b.start_date ? 1 : (a.start_date < b.start_date ? -1 : 0))
  //     const athleteActivities = activitiesSorted.map(a => ({
  //         athleteId: athlete.athleteId,
  //         athleteName: athlete.name,
  //         sex: athlete.sex,
  //         name: a.name,
  //         sportType: a.sport_type,
  //         distance: a.distance,
  //         movingTime: a.moving_time,
  //         perceivedExertion: a.perceived_exertion,
  //         totalElevationGain: a.total_elevation_gain,
  //         startDate: a.start_date,
  //         supported: isActivitySupported(a),
  //         points: calculateActivityPoints(a),
  //     }));
  //     result = [...result, ...athleteActivities];
  // }
  //
  res.send(result);
});

router.get('/export/ranking', async (req, res) => {
  const ranking: Ranking = await getRanking();
  const result: AthleteRankingStats[] = [
    ...ranking.map(r => ({
      athleteId: r.athleteId,
      athleteName: r.athleteName,
      points: sumPoints(r.activities),
    })),
  ].sort((a, b) => (a.athleteName > b.athleteName ? 1 : a.athleteName < b.athleteName ? -1 : 0));

  res.send(result);
});

type AthleteRankingStats = {
  athleteId: number;
  athleteName: string;
  points: number;
};
