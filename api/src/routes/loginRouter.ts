import express from 'express';
import { REDIRECT_URI_WHITELIST, ENDPOINTS, SCOPES } from '../utils/utils';
export const router = express.Router();

router.get('/', (req, res) => {
  const { callbackHostUrl, callbackPath, redirectTo } = req.query;
  if (!callbackHostUrl) {
    return res.status(400).json({ error: "'callbackUrl' param required" });
  }

  if (!REDIRECT_URI_WHITELIST.some(uri => (callbackHostUrl as string).startsWith(uri))) {
    return res.status(400).json({ error: "Given 'callbackUrl' is not allowed." });
  }

  if (!callbackPath) {
    return res.status(400).json({ error: "'callbackUrl' param required" });
  }

  const redirectParams = new URLSearchParams({
    callbackHostUrl: callbackHostUrl as string,
    callbackPath: callbackPath as string,
  });
  if (redirectTo != null) {
    redirectParams.set('redirectTo', redirectTo as string);
  }

  const authorizeEndpoint = ENDPOINTS.STRAVA_OAUTH_AUTHORIZE;
  const queryParams = new URLSearchParams({
    client_id: process.env.CLIENT_ID,
    response_type: 'code',
    redirect_uri: `${process.env.REDIRECT_URI}?${redirectParams.toString()}`,
    approval_prompt: 'force',
    scope: `${SCOPES.READ},${SCOPES.ACTIVITY_READ},${SCOPES.ACTIVITY_READ_ALL}`,
  });

  const url = `${authorizeEndpoint}?${queryParams}`;
  res.redirect(url);
});
