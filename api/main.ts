import express from 'express';
import dotenv from 'dotenv';
import serverless from 'serverless-http';
import { closeMongoDBClient } from './src/db/mongoDBAdapter';
import logger from 'morgan';
import cors from 'cors';
import { CORS_WHITELIST } from './src/utils/utils';

import { router as loginRouter } from './src/routes/loginRouter';
import { router as oauth2Router } from './src/routes/oauth2CallbackRouter';
import { router as athleteRouter } from './src/routes/athleteRouter';
import { router as challengeRouter } from './src/routes/challengeRouter';
import { router as stravaWebhooksRouter } from './src/routes/stravaWebhooksRouter';
import { router as devRouter } from './src/routes/devRouter';
import { router as feedRouter } from './src/routes/feedRouter';
import { router as userSettingsRouter } from './src/routes/userSettingsRouter';
import process from 'process';
import { errorHandlingMiddleware } from './src/error/errorHandlingMiddleware';

dotenv.config();

const app = express();
app.use(logger('dev'));
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(
  cors({
    origin: function (origin, callback) {
      if (CORS_WHITELIST.indexOf(origin) !== -1 || !origin) {
        callback(null, true);
      } else {
        callback(new Error(`Origin '${origin}' not allowed by CORS.`));
      }
    },
  }),
);

app.get('/', (req, res) => {
  res.send('Hello World from SportsApp!!!');
});

app.use('/login', loginRouter);

app.use('/oauth2-callback', oauth2Router);

app.use('/athlete', athleteRouter);

app.use('/challenge', challengeRouter);

app.use('/webhooks', stravaWebhooksRouter);

app.use('/dev', devRouter);

app.use('/feed', feedRouter);

app.use('/userSettings', userSettingsRouter);

app.listen(3000, async () => {
  console.log('Server listening on http://localhost:3000');
});

app.use(errorHandlingMiddleware); // IT MUST BE AT THE BOTTOM!!!. No app.use(...) should be placed below it!!!

async function cleanup() {
  console.log('Cleaning up...');
  await closeMongoDBClient();
  process.exit();
}

process.on('SIGINT', cleanup);
process.on('SIGTERM', cleanup);

module.exports.handler = serverless(app);
