/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{html,ts}'],
  theme: {
    extend: {
      fontFamily: {
        worksans: ['Work Sans', 'sans'],
      },
      // animation: {
      //   wiggle: 'wiggle 1s ease-in-out infinite',
      // },
      keyframes: {
        // unroll: {
        //     '0%': { transform: 'scaleY(0)', 'transform-origin': 'top' },
        //     '100%': { transform: 'scaleY(1)', 'transform-origin': 'top' }
        // }
        //   wiggle: {
        //     '0%, 100%': { transform: 'rotate(-30deg)' },
        //     '50%': { transform: 'rotate(30deg)' },
        //   }
      },
    },
  },
  plugins: [],
};
