export interface PagedResultDto<T> {
  count: number;
  pageSize: number;
  items: T[];
}
