import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'sports-app-points-view',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './points-view.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PointsViewComponent {
  @Input({ required: true })
  public points!: number;
}
