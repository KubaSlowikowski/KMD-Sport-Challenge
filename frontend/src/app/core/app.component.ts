import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  inject,
} from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { DynamicDialogModule } from 'primeng/dynamicdialog';
import { ToastModule } from 'primeng/toast';
import { ConfirmationDialogService } from '../features/confirmation-dialog';
import { ConfirmationDialogTranslocoProvider } from '../features/confirmation-dialog/i18n';
import { TranslocoModule } from '@jsverse/transloco';

@Component({
  selector: 'sports-app-root',
  templateUrl: './app.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [RouterOutlet, DynamicDialogModule, ToastModule, TranslocoModule],
  providers: [ConfirmationDialogTranslocoProvider],
})
export class AppComponent {
  private confirmationService = inject(ConfirmationDialogService);
  private changeDetectorRef = inject(ChangeDetectorRef);

  constructor() {
    // mipa - this is workaround for https://github.com/primefaces/primeng/issues/16402
    this.confirmationService.closed$.pipe().subscribe(ref => {
      ref.destroy();
      this.changeDetectorRef.detectChanges();
    });
  }
}
