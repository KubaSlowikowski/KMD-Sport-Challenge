import { Route } from '@angular/router';
import { LandingPageComponent } from '../features/landing-page/landing-page';
import { LoginComponent } from './intro';
import {
  authorizedCanActivateFn,
  unauthorizedCanActivateFn,
} from '../features/user';
import { AuthCallbackComponent } from './auth-callback';

export const appRoutes: Route[] = [
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [unauthorizedCanActivateFn],
  },
  {
    path: 'app',
    loadChildren: () =>
      import('../features/sport-app/routes').then(mod => mod.routes),
    canActivate: [authorizedCanActivateFn],
    providers: [],
  },
  {
    path: '',
    component: LandingPageComponent,
    children: [],
  },
  {
    path: 'auth-callback',
    component: AuthCallbackComponent,
  },
];
