import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { map, take } from 'rxjs';
import { Store } from '@ngrx/store';
import { UserActions, UserSelectors, UserState } from '../../features/user';

@Component({
  selector: 'sports-app-auth-callback',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [CommonModule],
  templateUrl: './auth-callback.component.html',
})
export class AuthCallbackComponent {
  private route = inject(ActivatedRoute);
  private store = inject(Store<UserState>);

  public vm$ = this.store
    .select(UserSelectors.getError)
    .pipe(map(error => ({ error })));

  constructor() {
    this.route.queryParams
      .pipe(
        map(query => ({
          token: query['token'],
          redirectTo: query['redirectTo'],
        })),
        take(1),
      )
      .subscribe(({ token, redirectTo }) => {
        this.handleCode(token, redirectTo);
      });
  }

  private handleCode(token: string, redirectTo: string): void {
    this.store.dispatch(UserActions.handleCallback({ token, redirectTo }));
  }
}
