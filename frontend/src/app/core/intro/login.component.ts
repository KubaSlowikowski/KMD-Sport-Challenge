import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { CommonModule, NgOptimizedImage } from '@angular/common';
import { Store } from '@ngrx/store';
import { ActivatedRoute, RouterOutlet } from '@angular/router';
import { UserActions, UserState } from '../../features/user';
import { MobileViewPortComponent } from '../../features/mobile-view-port/mobile-view-port.component';
import { map } from 'rxjs';
import { toSignal } from '@angular/core/rxjs-interop';

@Component({
  selector: 'sports-app-login',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    CommonModule,
    NgOptimizedImage,
    RouterOutlet,
    MobileViewPortComponent,
  ],
  templateUrl: './login.component.html',
})
export class LoginComponent {
  private store = inject(Store<UserState>);
  private route = inject(ActivatedRoute);
  private redirectToQueryParam = toSignal<string | undefined>(
    this.route.queryParams.pipe(map(query => query['redirectTo'])),
  );

  public login(): void {
    this.store.dispatch(
      UserActions.login({ redirectTo: this.redirectToQueryParam() }),
    );
  }

  public createStravaAccount(): void {
    this.store.dispatch(UserActions.createStravaAccount());
  }
}
