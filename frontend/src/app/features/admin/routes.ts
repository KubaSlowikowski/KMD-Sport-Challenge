import { Routes } from '@angular/router';
import { AdminPanelComponent } from './admin-panel';
import { ManualFeedFormComponent } from '../manual-feed-form';

export const ADMIN_ROUTES: Routes = [
  {
    path: '',
    component: AdminPanelComponent,
    children: [
      {
        path: '',
        component: ManualFeedFormComponent,
      },
    ],
  },
];
