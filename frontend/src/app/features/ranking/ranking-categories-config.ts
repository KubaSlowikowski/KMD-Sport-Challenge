import { SportType } from '../ranking-store';

export type RankingCategoryConfig = {
  categoryName: string;
  sportTypes: SportType[] | null;
};

export enum RankingCategoryName {
  GENERAL = 'General',
  BIKE = 'Bike',
  RUN = 'Run',
  SWIM = 'Swim',
  GYM = 'Gym',
  GPS_ONLY = 'GPS only',
}

export const rankingCategoriesConfig: RankingCategoryConfig[] = [
  {
    categoryName: RankingCategoryName.GENERAL,
    sportTypes: null,
  },
  {
    categoryName: RankingCategoryName.BIKE,
    sportTypes: [
      'Ride',
      'MountainBikeRide',
      'GravelRide',
      'VirtualRide',
      'MountainBikeRide',
      'EBikeRide',
      'EMountainBikeRide',
      'Handcycle',
      'Velomobile',
    ],
  },
  {
    categoryName: RankingCategoryName.RUN,
    sportTypes: ['Run', 'TrailRun', 'VirtualRun'],
  },
  {
    categoryName: RankingCategoryName.SWIM,
    sportTypes: ['Swim'],
  },
  {
    categoryName: RankingCategoryName.GYM,
    sportTypes: [
      'WeightTraining',
      'Workout',
      'StairStepper',
      'HighIntensityIntervalTraining',
      'Crossfit',
      'Pilates',
      'Yoga',
    ],
  },
  {
    categoryName: RankingCategoryName.GPS_ONLY,
    sportTypes: [
      'Ride',
      'Walk',
      'Run',
      'Swim',
      'TrailRun',
      'VirtualRide',
      'GravelRide',
      'Hike',
      'MountainBikeRide',
      'RollerSki',
      'Rowing',
      'VirtualRow',
      'Kitesurf',
      'StandUpPaddling',
      'Windsurf',
      'AlpineSki',
      'NordicSki',
      'BackcountrySki',
      'IceSkate',
      'InlineSkate',
      'Snowboard',
      'VirtualRun',
      'Snowshoe',
      'Handcycle',
      'EBikeRide',
      'EMountainBikeRide',
      'Velomobile',
      'Wheelchair',
      'Skateboard',
      'Canoeing',
      'Kayaking',
    ],
  },
];
