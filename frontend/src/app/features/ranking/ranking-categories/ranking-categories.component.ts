import {
  ChangeDetectionStrategy,
  Component,
  effect,
  inject,
} from '@angular/core';
import { NgClass, NgForOf } from '@angular/common';
import {
  rankingCategoriesConfig,
  RankingCategoryConfig,
  RankingCategoryName,
} from '../ranking-categories-config';
import { Store } from '@ngrx/store';
import { RankingActions, RankingState } from '../../ranking-store';
import { ActivatedRoute, Router } from '@angular/router';
import { toSignal } from '@angular/core/rxjs-interop';
import { map } from 'rxjs';

@Component({
  selector: 'app-ranking-categories',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [NgForOf, NgClass],
  templateUrl: './ranking-categories.component.html',
  styleUrl: './ranking-categories.component.scss',
})
export class RankingCategoriesComponent {
  protected readonly rankingCategories: RankingCategoryConfig[] =
    rankingCategoriesConfig;
  private route = inject(ActivatedRoute);
  private router = inject(Router);
  private store = inject(Store<RankingState>);

  public $categoryQueryParam = toSignal(
    this.route.queryParams.pipe(map(params => params['category'])),
  );

  constructor() {
    effect(
      () => {
        this.updateCategoryFromStoreEffect();
      },
      { allowSignalWrites: true },
    );
  }

  onCategoryChange(categoryName: string): void {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {
        category: categoryName,
      },
      queryParamsHandling: 'merge',
    });
  }

  private updateCategoryFromStoreEffect(): void {
    const categoryConfig: RankingCategoryConfig | undefined =
      rankingCategoriesConfig.find(
        c => c.categoryName === this.$categoryQueryParam(),
      );
    if (categoryConfig == null) {
      this.onCategoryChange(RankingCategoryName.GENERAL.toString());
      return;
    }

    this.store.dispatch(
      RankingActions.selectCategory({
        categoryName: this.$categoryQueryParam(),
      }),
    );
  }
}
