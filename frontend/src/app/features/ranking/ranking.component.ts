import {
  ChangeDetectionStrategy,
  Component,
  computed,
  inject,
  OnInit,
  Signal,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  Ranking,
  RankingActions,
  RankingSelector,
  RankingState,
} from '../ranking-store';
import { Store } from '@ngrx/store';
import { combineLatest, map } from 'rxjs';
import { SkeletonModule } from 'primeng/skeleton';
import { UserSelectors } from '../user';
import { PointsViewComponent } from '../../shared/points-view/points-view.component';
import { RouterModule } from '@angular/router';
import { SportBadgeComponent } from '../sport-badge/sport-badge.component';
import { RankingCategoriesComponent } from './ranking-categories/ranking-categories.component';
import { RankingLogic } from '../ranking-store/ranking.logic';
import { ChallengeContext } from '../challenges/challenge-logic';

@Component({
  selector: 'sports-app-ranking',
  standalone: true,
  imports: [
    CommonModule,
    SkeletonModule,
    PointsViewComponent,
    RouterModule,
    SportBadgeComponent,
    RankingCategoriesComponent,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './ranking.component.html',
})
export class RankingComponent implements OnInit {
  private store = inject(Store<RankingState>);
  private rankingLogic = inject(RankingLogic);
  private challengeContext = inject(ChallengeContext);

  public vm$ = combineLatest([
    this.store.select(RankingSelector.getIsLoading),
    this.store.select(RankingSelector.getError),
    this.store.select(UserSelectors.getClaims),
  ]).pipe(
    map(([isLoading, error, claims]) => ({
      isLoading,
      error,
      athleteId: claims!.athleteId,
    })),
  );

  public $ranking: Signal<Ranking> = computed(() => {
    const $rankingFromStore = this.store.selectSignal(RankingSelector.getData);
    const $selectedCategory = this.store.selectSignal(
      RankingSelector.getSelectedCategoryName,
    );

    const filteredRanking: Ranking = this.rankingLogic.filterRankingByCategory(
      $rankingFromStore(),
      $selectedCategory(),
    );
    return this.rankingLogic.calculateRanking(filteredRanking);
  });

  ngOnInit(): void {
    this.store.dispatch(
      RankingActions.init({ challengeId: this.challengeContext.challengeId() }),
    );
  }

  public load(): void {
    this.store.dispatch(
      RankingActions.load({ challengeId: this.challengeContext.challengeId() }),
    );
  }
}
