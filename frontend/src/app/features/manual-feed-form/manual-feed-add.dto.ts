export interface ManualFeedAddDto {
  text: string;
  challengeId: string;
}
