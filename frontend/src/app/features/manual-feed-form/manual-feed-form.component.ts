import {
  ChangeDetectionStrategy,
  Component,
  effect,
  inject,
} from '@angular/core';
import {
  FormsModule,
  NonNullableFormBuilder,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { ManualFeedFormService } from './manual-feed-form.service';
import { ManualFeedFormStore } from './manual-feed-form.store';
import { ActivatedRoute } from '@angular/router';
import { toSignal } from '@angular/core/rxjs-interop';
import { map } from 'rxjs';
import { ChallengeContext } from '../challenges/challenge-logic';

@Component({
  selector: 'app-manual-feed-form',
  standalone: true,
  imports: [InputTextareaModule, FormsModule, ReactiveFormsModule],
  templateUrl: './manual-feed-form.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [ManualFeedFormService, ManualFeedFormStore],
})
export class ManualFeedFormComponent {
  private fb = inject(NonNullableFormBuilder);
  private store = inject(ManualFeedFormStore);
  private challengeContext = inject(ChallengeContext);

  public formGroup = this.fb.group({
    text: ['', Validators.required],
  });

  constructor() {
    effect(
      () => {
        if (this.store.$isLoading()) {
          this.formGroup.disable();
        } else {
          this.formGroup.enable();
        }
      },
      { allowSignalWrites: true },
    );
  }

  public submit(): void {
    this.formGroup.markAllAsTouched();
    if (this.formGroup.invalid) {
      return;
    }

    this.store.createNew({
      dto: {
        ...this.formGroup.getRawValue(),
        challengeId: this.challengeContext.challengeId(),
      },
    });
  }
}
