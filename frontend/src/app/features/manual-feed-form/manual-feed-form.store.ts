import { Injectable, inject } from '@angular/core';
import { patchState, signalState } from '@ngrx/signals';
import { rxMethod } from '@ngrx/signals/rxjs-interop';
import { exhaustMap, pipe, tap } from 'rxjs';
import { ManualFeedFormService } from './manual-feed-form.service';
import { MessageService } from 'primeng/api';
import { ManualFeedAddDto } from './manual-feed-add.dto';
import { tapResponse } from '@ngrx/operators';

interface ManualFeedFormStoreState {
  isLoading: boolean;
}

const initialState: ManualFeedFormStoreState = {
  isLoading: false,
};

@Injectable()
export class ManualFeedFormStore {
  private readonly service = inject(ManualFeedFormService);
  private readonly messageService = inject(MessageService);

  private readonly state = signalState(initialState);

  public $isLoading = this.state.isLoading;

  public createNew = rxMethod<{ dto: ManualFeedAddDto }>(
    pipe(
      tap(() => patchState(this.state, { isLoading: true })),
      exhaustMap(({ dto }) =>
        this.service.add(dto).pipe(
          tapResponse({
            next: () => {
              patchState(this.state, { isLoading: false });
              this.messageService.add({
                severity: 'success',
                detail: 'Feed was created',
              });
            },
            error: () => {
              this.messageService.add({
                severity: 'error',
                detail: 'There was an error when creating feed.',
              });
            },
            finalize: () => patchState(this.state, { isLoading: false }),
          }),
        ),
      ),
    ),
  );
}
