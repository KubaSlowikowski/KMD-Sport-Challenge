import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ManualFeedAddDto } from './manual-feed-add.dto';
import { environment } from '../../../environments/environment';

@Injectable()
export class ManualFeedFormService {
  constructor(private httpClient: HttpClient) {}

  public add(feed: ManualFeedAddDto): Observable<void> {
    return this.httpClient.post<void>(
      `${environment.apiUrl}/feed/manual`,
      feed,
    );
  }
}
