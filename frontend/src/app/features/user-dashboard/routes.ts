import { Routes } from '@angular/router';
import { UserDashboardSmartComponent } from './user-dashboard-smart';

export const routes: Routes = [
  {
    path: '',
    component: UserDashboardSmartComponent,
  },
];
