import { ChangeDetectionStrategy, Component } from '@angular/core';
import { RouterLink } from '@angular/router';
import { ChallengeListSmartComponent } from '../../challenges/challenge-list';

import { TranslocoModule } from '@jsverse/transloco';
import {
  USER_DASHBOARD_TRANSLATION_SCOPE,
  UserDashboardTranslocoProvider,
} from '../i18n';

@Component({
  selector: 'app-user-dashboard-smart',
  standalone: true,
  imports: [RouterLink, ChallengeListSmartComponent, TranslocoModule],
  templateUrl: './user-dashboard-smart.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [UserDashboardTranslocoProvider],
})
export class UserDashboardSmartComponent {
  public scope = USER_DASHBOARD_TRANSLATION_SCOPE;
}
