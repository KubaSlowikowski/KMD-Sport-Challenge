import { TransLocoProvider, featureLoader } from 'src/app/features/transloco';

export const USER_DASHBOARD_TRANSLATION_SCOPE = 'userDashboard';
const loader = () => featureLoader(lang => import(`./${lang}.json`));
export const UserDashboardTranslocoProvider = TransLocoProvider(
  USER_DASHBOARD_TRANSLATION_SCOPE,
  () => loader(),
);
