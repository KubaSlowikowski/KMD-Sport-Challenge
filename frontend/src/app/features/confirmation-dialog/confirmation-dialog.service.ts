import { Injectable, inject } from '@angular/core';
import { ConfirmationDialogModel } from './confirmation-dialog.model';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ConfirmationDialogComponent } from './confirmation-dialog.component';
import { Subject, map, switchMap } from 'rxjs';
import { TranslocoService } from '@jsverse/transloco';
import { CONFIRMATION_DIALOG_TRANSLATION_SCOPE } from './i18n';

@Injectable()
export class ConfirmationDialogService {
  private dialogService = inject(DialogService);
  private t = inject(TranslocoService);
  private ref$ = new Subject<DynamicDialogRef<ConfirmationDialogComponent>>();
  public closed$ = this.ref$.pipe(
    switchMap(ref => ref.onClose.pipe(map(() => ref))),
  );

  public ask(model: ConfirmationDialogModel): void {
    const ref = this.dialogService.open(ConfirmationDialogComponent, {
      header:
        model.title != null
          ? this.t.translate(model.title, model.titleTranslationArgs)
          : this.t.translate(
              `${CONFIRMATION_DIALOG_TRANSLATION_SCOPE}.DEFAULT_TITLE`,
            ),
      data: model,
      closeOnEscape: true,
      modal: true,
      dismissableMask: true,
      style: { 'max-width': '80%' },
    });
    this.ref$.next(ref);
  }
}
