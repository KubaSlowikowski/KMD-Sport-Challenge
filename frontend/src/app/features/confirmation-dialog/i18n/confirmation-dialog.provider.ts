import { TransLocoProvider, featureLoader } from 'src/app/features/transloco';

export const CONFIRMATION_DIALOG_TRANSLATION_SCOPE = 'confirmationDialog';
const loader = () => featureLoader(lang => import(`./${lang}.json`));
export const ConfirmationDialogTranslocoProvider = TransLocoProvider(
  CONFIRMATION_DIALOG_TRANSLATION_SCOPE,
  () => loader(),
);
