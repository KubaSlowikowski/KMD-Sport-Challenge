import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DialogService, DynamicDialogModule } from 'primeng/dynamicdialog';

@NgModule({
  providers: [DialogService],
  imports: [CommonModule, DynamicDialogModule],
})
export class ConfirmationDialogModule {}
