import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ConfirmationDialogModel } from './confirmation-dialog.model';
import { TranslocoModule } from '@jsverse/transloco';
import { CONFIRMATION_DIALOG_TRANSLATION_SCOPE } from './i18n';

@Component({
  selector: 'sports-app-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, TranslocoModule],
})
export class ConfirmationDialogComponent {
  private ref = inject(DynamicDialogRef);

  private dialogService = inject(DialogService);
  public data = this.dialogService.getInstance(this.ref)
    .data as ConfirmationDialogModel;
  public scope = CONFIRMATION_DIALOG_TRANSLATION_SCOPE;

  public approve(): void {
    if (this.data.approveFn != null) {
      this.data.approveFn();
    }
    this.ref.close();
  }

  public decline(): void {
    if (this.data.declineFn != null) {
      this.data.declineFn();
    }
    this.ref.close();
  }
}
