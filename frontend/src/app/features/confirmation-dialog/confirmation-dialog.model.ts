import { HashMap } from '@jsverse/transloco';

export interface ConfirmationDialogModel {
  title?: string;
  titleTranslationArgs?: HashMap;
  text?: string;
  textTranslationArgs?: HashMap;
  approveLabel?: string;
  declineLabel?: string;
  approveFn?: () => void;
  approveLabelTranslationArgs?: HashMap;
  declineFn?: () => void;
  declineLabelTranslationArgs?: HashMap;
}
