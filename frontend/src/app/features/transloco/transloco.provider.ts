import { Provider } from '@angular/core';
import { TRANSLOCO_SCOPE, TranslocoScope } from '@jsverse/transloco';

/**
 * @param injectedScopeLoader i18n files should be prefixed with component name to avoid issues.
 * @returns TransLocoProvider required for component to perform HTML and TS translations.
 */
export const TransLocoProvider = (scope: string, injectedScopeLoader: any) =>
  ({
    provide: TRANSLOCO_SCOPE,
    useFactory: translationDefaultFactory(scope, injectedScopeLoader),
    deps: [],
    multi: true,
  }) as Provider;

const translationDefaultFactory =
  (
    scope: string,
    injectedScopeLoader: (lang: string | TranslocoScope) => Promise<unknown>,
  ) =>
  (defaultLang: TranslocoScope) => {
    return {
      scope,
      loader: injectedScopeLoader(defaultLang),
    };
  };
