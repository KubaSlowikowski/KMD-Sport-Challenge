export const featureLoader = (importer: (lang: string) => Promise<unknown>) => {
  const langArray = ['en', 'pl'].map(lang => lang.trim());

  return langArray.reduce(
    (acc: { [key: string]: () => Promise<unknown> }, lang) => {
      acc[lang] = async () => {
        try {
          return await importer(lang);
        } catch (error) {
          const FALLBACK_LANGUAGE = 'en';
          return await importer(FALLBACK_LANGUAGE);
        }
      };
      return acc;
    },
    {},
  );
};
