import { createAction, props } from '@ngrx/store';
import { UserModel } from './models';

export class UserActions {
  public static readonly login = createAction(
    '[User Actions] Login',
    props<{ redirectTo?: string }>(),
  );

  public static readonly createStravaAccount = createAction(
    '[User Actions] Create Strava Account',
  );

  public static readonly handleCallback = createAction(
    '[User Actions] Handle Callback',
    props<{ token: string; redirectTo?: string }>(),
  );

  public static readonly handleCallbackSuccess = createAction(
    '[User Actions] Handle Callback Success',
    props<{ claims: UserModel; token: string; redirectTo?: string }>(),
  );

  public static readonly handleCallbackError = createAction(
    '[User Actions] Handle Callback Error',
    props<{ error: Error }>(),
  );

  public static readonly restoreSession = createAction(
    '[User Actions] Restore Session',
    props<{ claims: UserModel; token: string }>(),
  );

  public static readonly logout = createAction('[User Actions] Logout');

  public static readonly leaveChallenge = createAction(
    '[User Actions] Leave challenge',
  );

  public static readonly leaveChallengeSuccess = createAction(
    '[User Actions] Leave challenge success',
  );

  public static readonly leaveChallengeError = createAction(
    '[User Actions] Leave challenge error',
    props<{ error: Error }>(),
  );

  public static readonly verifyExistingToken = createAction(
    '[User Actions] Check that logged user exists',
  );
}
