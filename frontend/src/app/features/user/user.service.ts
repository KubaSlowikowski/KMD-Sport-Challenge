import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private httpClient: HttpClient) {}

  public delete(): Observable<void> {
    return this.httpClient.delete<void>(`${environment.apiUrl}/athlete`);
  }

  public verifyLoggedUser(): Observable<HttpResponse<any>> {
    return this.httpClient.get<any>(`${environment.apiUrl}/athlete/exists`, {
      observe: 'response',
    });
  }
}
