import { HttpHandlerFn, HttpRequest } from '@angular/common/http';
import { inject } from '@angular/core';
import { UserState } from './user.state';
import { Store } from '@ngrx/store';
import { UserSelectors } from './user.selector';

export function authInterceptor(
  req: HttpRequest<unknown>,
  next: HttpHandlerFn,
) {
  const store = inject(Store<UserState>);
  const token = store.selectSignal(UserSelectors.getToken)();
  const clonedRequest = req.clone({
    headers: req.headers.set('Authorization', `Bearer ${token}`),
  });
  return next(clonedRequest);
}
