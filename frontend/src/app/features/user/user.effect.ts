import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { UserActions } from './user.action';
import {
  catchError,
  EMPTY,
  exhaustMap,
  filter,
  map,
  of,
  switchMap,
  tap,
  withLatestFrom,
} from 'rxjs';
import { Router } from '@angular/router';
import { UserState } from './user.state';
import { Store } from '@ngrx/store';
import { UserSelectors } from './user.selector';
import { HttpParams } from '@angular/common/http';
import { UserService } from './user.service';
import { environment } from '../../../environments/environment';

@Injectable()
export class UserEffects {
  constructor(
    private actions$: Actions,
    private router: Router,
    private store: Store<UserState>,
    private userService: UserService,
  ) {}

  login$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(UserActions.login),
        tap(({ redirectTo }) => {
          const callbackHostUrl = `${window.location.protocol}//${window.location.hostname}${window.location.port != null ? ':' + window.location.port : ''}`;

          const loginUrl = this.router.createUrlTree([], {
            queryParams: {
              callbackHostUrl,
              callbackPath: '/auth-callback',
              redirectTo,
            },
          });

          const stravaUrlSerialized =
            environment.apiUrl + this.router.serializeUrl(loginUrl);
          window.location.href = stravaUrlSerialized;
        }),
      ),
    { dispatch: false },
  );

  createStravaAccount$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(UserActions.createStravaAccount),
        tap(() => {
          window.location.href = 'https://www.strava.com/register/free';
        }),
      ),
    { dispatch: false },
  );

  handleCallback$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UserActions.handleCallback),
      map(({ token, redirectTo }) => ({
        claims: JSON.parse(atob(token.split('.')[1])),
        token,
        redirectTo,
      })),
      map(args => UserActions.handleCallbackSuccess({ ...args })),
      catchError(error => of(UserActions.handleCallbackError({ error }))),
    ),
  );

  handleCallbackSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(UserActions.handleCallbackSuccess),
        tap(({ redirectTo }) => {
          if (redirectTo != null) {
            const redirectToPath = this.router.parseUrl(redirectTo);
            this.router.navigateByUrl(redirectToPath);
          } else {
            this.router.navigate(['/app']);
          }
        }),
      ),
    { dispatch: false },
  );

  restoreSession$ = createEffect(() =>
    of(localStorage.getItem('sport-app-token')).pipe(
      filter(token => token != null),
      map(token => token ?? ''),
      withLatestFrom(this.store.select(UserSelectors.getToken)),
      filter(([token, currentToken]) => token !== currentToken),
      map(([token]) => token),
      map(token => ({
        claims: JSON.parse(atob(token.split('.')[1])),
        token,
      })),
      map(args => UserActions.restoreSession({ ...args })),
    ),
  );

  saveSession$ = createEffect(
    () =>
      this.store.select(UserSelectors.getToken).pipe(
        filter(token => token != null),
        tap(token => {
          localStorage.setItem('sport-app-token', token as string);
        }),
      ),
    { dispatch: false },
  );

  logout$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(UserActions.logout),
        tap(() => {
          localStorage.removeItem('sport-app-token');
          this.router.navigate(['']);
        }),
      ),
    { dispatch: false },
  );

  leaveChallenge$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UserActions.leaveChallenge),
      exhaustMap(() =>
        this.userService.delete().pipe(
          // TODO move http call to separate service
          map(() => UserActions.leaveChallengeSuccess()),
          catchError(error => of(UserActions.leaveChallengeError({ error }))),
        ),
      ),
    ),
  );

  leaveChallengeSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(UserActions.leaveChallengeSuccess),
        tap(() => {
          localStorage.removeItem('sport-app-token');
          this.router.navigate(['']);
        }),
      ),
    { dispatch: false },
  );

  leaveChallengeError$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(UserActions.leaveChallengeError),
        tap(error => {
          console.error(error.error);
          localStorage.removeItem('sport-app-token');
          this.router.navigate(['']);
          window.location.reload();
          // alert('Error occurred during user deletion. Let us know about this issue on our Teams channel :). ');
        }),
      ),
    { dispatch: false },
  );

  verifyExistingToken$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(UserActions.verifyExistingToken),
        switchMap(() =>
          this.userService.verifyLoggedUser().pipe(
            catchError(error => {
              if (error.status === 404) {
                localStorage.removeItem('sport-app-token');
                this.router.navigate(['']);
                window.location.reload();
              }
              return EMPTY;
            }),
          ),
        ),
      ),
    { dispatch: false },
  );
}
