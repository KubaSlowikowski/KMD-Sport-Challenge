import { inject } from '@angular/core';
import {
  ActivatedRoute,
  CanActivateFn,
  Router,
  UrlTree,
} from '@angular/router';
import { Store } from '@ngrx/store';
import { UserState } from './user.state';
import { UserSelectors } from './user.selector';
import { map, withLatestFrom } from 'rxjs';

export const authorizedCanActivateFn: CanActivateFn = (route, state) => {
  const store = inject(Store<UserState>);
  const router = inject(Router);
  return store.select(UserSelectors.getClaims).pipe(
    map(user => {
      if (user != null) {
        return true;
      }

      const url = router.createUrlTree(['/login'], {
        queryParams: {
          redirectTo: state.url,
        },
      });
      return user != null ? true : url;
    }),
  );
};
