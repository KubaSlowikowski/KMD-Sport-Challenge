import { createReducer, on } from '@ngrx/store';
import { UserState } from './user.state';
import { UserActions } from './user.action';

const initialState: UserState = {
  claims: null,
  token: null,
  error: null,
};

export const userReducer = createReducer(
  initialState,
  on(UserActions.handleCallback, () => ({
    ...initialState,
  })),
  on(
    UserActions.handleCallbackSuccess,
    UserActions.restoreSession,
    (state, { claims, token }) => ({
      ...state,
      claims,
      token,
    }),
  ),
  on(UserActions.handleCallbackError, (state, { error }) => ({
    ...state,
    error,
  })),
  on(UserActions.logout, () => ({
    ...initialState,
  })),
  on(UserActions.leaveChallenge, state => ({
    ...state,
  })),
  on(UserActions.leaveChallengeSuccess, () => ({
    ...initialState,
  })),
);
