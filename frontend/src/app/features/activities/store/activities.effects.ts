import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { catchError, filter, map, of, switchMap, withLatestFrom } from 'rxjs';
import { ActivityState } from './activities.state';
import { ActivityService } from './activities.service';
import { ActivityActions } from './activities.actions';
import { ActivitiesSelector } from './activities.selector';
import { RankingActions } from '../../ranking-store';

@Injectable()
export class ActivityEffects {
  constructor(
    private actions$: Actions,
    private store: Store<ActivityState>,
    private service: ActivityService,
  ) {}

  triggerLoad$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ActivityActions.triggerLoad),
      withLatestFrom(
        this.store.select(ActivitiesSelector.getAthleteId),
        this.store.select(ActivitiesSelector.isObsolete),
      ),
      filter(
        ([{ athleteId, challengeId, force }, currentAthleteId, obsolete]) =>
          force || currentAthleteId !== athleteId || obsolete,
      ),
      map(([{ athleteId, challengeId }]) =>
        ActivityActions.load({ athleteId, challengeId }),
      ),
    ),
  );

  login$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ActivityActions.load),
      switchMap(({ athleteId, challengeId }) =>
        this.service.getAllForAthlete(athleteId, challengeId).pipe(
          map(data => ActivityActions.loadSuccess({ data, athleteId })),
          catchError(error => of(ActivityActions.loadError({ error }))),
        ),
      ),
    ),
  );

  triggerLoadDetails$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ActivityActions.triggerLoadDetails),
      map(action => ({
        action,
        currentDetails: this.store.selectSignal(
          ActivitiesSelector.getDetailsDto(action.activityId),
        )(),
      })),
      map(args =>
        args.currentDetails != null
          ? ActivityActions.detailsAlreadyLoaded()
          : ActivityActions.loadDetails({ ...args.action }),
      ),
    ),
  );

  loadDetails$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ActivityActions.loadDetails),
      switchMap(({ activityId, athleteId }) =>
        this.service.getDetails(athleteId, activityId).pipe(
          map(dto => ActivityActions.loadDetailsSuccess({ dto })),
          catchError(error =>
            of(ActivityActions.loadDetailsError({ activityId, error })),
          ),
        ),
      ),
    ),
  );

  refresh$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ActivityActions.refresh),
      switchMap(({ activityId, athleteId }) =>
        this.service.refresh(athleteId, activityId).pipe(
          map(dto => ActivityActions.refreshSuccess({ activityId, dto })),
          catchError(error =>
            of(ActivityActions.refreshError({ activityId, error })),
          ),
        ),
      ),
    ),
  );

  refreshSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ActivityActions.refreshSuccess),
      map(() => RankingActions.markAsObsolete()),
    ),
  );
}
