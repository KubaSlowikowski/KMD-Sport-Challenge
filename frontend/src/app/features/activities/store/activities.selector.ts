import { createFeatureSelector, createSelector } from '@ngrx/store';
import { ActivityState } from './activities.state';

const getState = createFeatureSelector<ActivityState>('activityFeature');

export class ActivitiesSelector {
  public static readonly getIsLoading = createSelector(
    getState,
    state => state.isLoading,
  );

  public static readonly getError = createSelector(
    getState,
    state => state.error,
  );

  public static readonly getData = createSelector(
    getState,
    state => state.data,
  );

  public static readonly isObsolete = createSelector(
    getState,
    state => state.obsolete,
  );

  public static readonly getAthleteId = createSelector(
    getState,
    state => state.athleteId,
  );

  public static readonly getLoadingDetails = (activityId: number) =>
    createSelector(getState, state =>
      state.loadingDetails.includes(activityId),
    );

  public static readonly getErroredDetails = (activityId: number) =>
    createSelector(
      getState,
      state =>
        state.erroredDetails.find(d => d.activityId === activityId)?.error,
    );

  public static readonly getDetailsDto = (activityId: number) =>
    createSelector(getState, state =>
      state.detailsDtos.find(d => d.id === activityId),
    );

  public static readonly getOpenedDetails = (activityId: number) =>
    createSelector(getState, state => state.openedDetails.includes(activityId));
}
