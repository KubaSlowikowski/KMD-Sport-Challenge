import { ActivityDetailsDto } from './activity-details.dto';
import { ActivityDto } from './activity.dto';

export interface ActivityState {
  isLoading: boolean;
  error: Error | null;
  data: ActivityDto[];
  athleteId: string | null;
  loadingDetails: number[];
  openedDetails: number[];
  erroredDetails: { activityId: number; error: Error }[];
  detailsDtos: ActivityDetailsDto[];
  obsolete: boolean;
}
