import { SportType } from '../../ranking-store';

export interface ActivityDto {
  id: number;
  type: SportType;
  name: string;
  createdDate: Date;
  points: number;
  supported: boolean;
  distance: number;
  movingTime: number;
  effort?: number;
  requiresEffort: boolean;
  photoUrl?: {
    '100': string;
    '600': string;
  };
}
