import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivityDto } from './activity.dto';
import { Observable, map } from 'rxjs';
import { ActivityDetailsDto } from './activity-details.dto';
import { environment } from '../../../../environments/environment';

type ActivityApi = ActivityDto & { createdData: string };
type ActivityDetailsApi = ActivityDetailsDto & { createdData: string };

@Injectable()
export class ActivityService {
  constructor(private httpClient: HttpClient) {}

  public getAllForAthlete(
    athleteId: string | 'me',
    challengeId: string,
  ): Observable<ActivityDto[]> {
    let params = new HttpParams();
    params = params.set('challengeId', challengeId);
    return this.httpClient
      .get<
        ActivityApi[]
      >(`${environment.apiUrl}/athlete/${athleteId}/activities`, { params })
      .pipe(
        map(activities =>
          activities.map(a => this.parseActivity(a as ActivityApi)),
        ),
      );
  }

  public getById(
    athleteId: number | 'me',
    activityId: number,
  ): Observable<ActivityDto> {
    return this.httpClient.get<ActivityDto>(
      `${environment.apiUrl}/athlete/${athleteId}/activity/${activityId}`,
    );
  }

  public getDetails(
    athleteId: number,
    activityId: number,
  ): Observable<ActivityDetailsDto> {
    return this.httpClient
      .get<ActivityDetailsApi>(
        `${environment.apiUrl}/athlete/${athleteId}/activity/${activityId}/details`,
      )
      .pipe(map(dto => this.parseActivityDetails(dto as ActivityDetailsApi)));
  }

  public refresh(
    athleteId: number | 'me',
    activityId: number,
  ): Observable<ActivityDetailsDto> {
    return this.httpClient
      .put<ActivityDetailsApi>(
        `${environment.apiUrl}/athlete/${athleteId}/activity/${activityId}/refresh`,
        null,
      )
      .pipe(map(dto => this.parseActivityDetails(dto as ActivityDetailsApi)));
  }

  private parseActivity(activity: ActivityApi): ActivityDto {
    return {
      ...activity,
      createdDate: new Date(activity.createdDate),
    };
  }

  private parseActivityDetails(
    activity: ActivityDetailsApi,
  ): ActivityDetailsDto {
    return {
      ...activity,
      createdDate: new Date(activity.createdDate),
    };
  }
}
