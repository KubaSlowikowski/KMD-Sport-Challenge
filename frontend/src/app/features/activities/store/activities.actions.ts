import { createAction, props } from '@ngrx/store';
import { ActivityDto } from './activity.dto';
import { ActivityDetailsDto } from './activity-details.dto';

export class ActivityActions {
  public static readonly triggerLoad = createAction(
    '[Activity] Trigger Load',
    props<{ athleteId: string; challengeId: string; force?: boolean }>(),
  );

  public static readonly load = createAction(
    '[Activity] Load',
    props<{ athleteId: string; challengeId: string }>(),
  );

  public static readonly loadSuccess = createAction(
    '[Activity] Load Success',
    props<{ athleteId: string; data: ActivityDto[] }>(),
  );

  public static readonly loadError = createAction(
    '[Activity] Load Error',
    props<{ error: Error }>(),
  );

  public static readonly refresh = createAction(
    '[Activity] Refresh',
    props<{ athleteId: number; activityId: number }>(),
  );

  public static readonly refreshSuccess = createAction(
    '[Activity] Refresh Success',
    props<{ activityId: number; dto: ActivityDetailsDto }>(),
  );

  public static readonly refreshError = createAction(
    '[Activity] Refresh Error',
    props<{ activityId: number; error: Error }>(),
  );

  public static readonly triggerLoadDetails = createAction(
    '[Activity] Trigger Load Details',
    props<{ athleteId: number; activityId: number }>(),
  );

  public static readonly detailsAlreadyLoaded = createAction(
    '[Activity] Details Already Loaded',
  );

  public static readonly loadDetails = createAction(
    '[Activity] Load Details',
    props<{ athleteId: number; activityId: number }>(),
  );

  public static readonly loadDetailsSuccess = createAction(
    '[Activity] Load Details Success',
    props<{ dto: ActivityDetailsDto }>(),
  );

  public static readonly loadDetailsError = createAction(
    '[Activity] Load Details Error',
    props<{ activityId: number; error: Error }>(),
  );

  public static readonly openDetails = createAction(
    '[Activity] Open Details',
    props<{ activityId: number }>(),
  );

  public static readonly closeDetails = createAction(
    '[Activity] Close Details',
    props<{ activityId: number }>(),
  );

  public static readonly markAsObsolete = createAction(
    '[Activity] Mark As Obsolete',
  );
}
