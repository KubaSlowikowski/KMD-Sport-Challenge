import { createFeature, createReducer, on } from '@ngrx/store';
import { ActivityActions } from './activities.actions';
import { ActivityState } from './activities.state';
import { ActivityParser } from './activity.parser';

const initialState: ActivityState = {
  isLoading: false,
  error: null,
  data: [],
  athleteId: null,
  detailsDtos: [],
  loadingDetails: [],
  erroredDetails: [],
  openedDetails: [],
  obsolete: false,
};

const reducer = createReducer(
  initialState,
  on(ActivityActions.load, state => ({
    ...state,
    isLoading: true,
    error: null,
    data: [],
    detailsDtos: [],
    loadingDetails: [],
    erroredDetails: [],
    openedDetails: [],
  })),
  on(ActivityActions.loadSuccess, (state, { data, athleteId }) => ({
    ...state,
    isLoading: false,
    error: null,
    data,
    athleteId,
    obsolete: false,
  })),
  on(ActivityActions.loadError, (state, { error }) => ({
    ...state,
    isLoading: false,
    error,
  })),
  on(ActivityActions.loadDetails, (state, { activityId }) => ({
    ...state,
    loadingDetails: state.loadingDetails
      .filter(d => d !== activityId)
      .concat([activityId]),
    erroredDetails: state.erroredDetails.filter(
      d => d.activityId !== activityId,
    ),
  })),
  on(ActivityActions.loadDetailsSuccess, (state, { dto }) => ({
    ...state,
    detailsDtos: [...state.detailsDtos, dto],
    loadingDetails: state.loadingDetails.filter(d => d !== dto.id),
    erroredDetails: state.erroredDetails.filter(d => d.activityId !== dto.id),
  })),
  on(ActivityActions.loadDetailsError, (state, { error, activityId }) => ({
    ...state,
    loadingDetails: state.loadingDetails.filter(d => d !== activityId),
    erroredDetails: [...state.erroredDetails, { activityId, error }],
  })),
  on(ActivityActions.refresh, (state, { activityId }) => ({
    ...state,
    loadingDetails: state.loadingDetails
      .filter(d => d !== activityId)
      .concat([activityId]),
    erroredDetails: state.erroredDetails.filter(
      d => d.activityId !== activityId,
    ),
  })),
  on(ActivityActions.refreshSuccess, (state, { activityId, dto }) => ({
    ...state,
    data: state.data.map(a =>
      a.id === activityId ? ActivityParser.parseDetailsToActivity(dto) : a,
    ),
    detailsDtos: [...state.detailsDtos.filter(d => d.id !== activityId), dto],
    loadingDetails: state.loadingDetails.filter(d => d !== activityId),
    erroredDetails: state.erroredDetails.filter(
      d => d.activityId !== activityId,
    ),
  })),
  on(ActivityActions.refreshError, (state, { error, activityId }) => ({
    ...state,
    loadingDetails: state.loadingDetails.filter(d => d !== activityId),
    erroredDetails: [...state.erroredDetails, { activityId, error }],
  })),
  on(ActivityActions.openDetails, (state, { activityId }) => ({
    ...state,
    openedDetails: [
      ...state.openedDetails.filter(o => o !== activityId),
      activityId,
    ],
  })),
  on(ActivityActions.closeDetails, (state, { activityId }) => ({
    ...state,
    openedDetails: state.openedDetails.filter(o => o !== activityId),
  })),
  on(ActivityActions.markAsObsolete, state => ({
    ...state,
    obsolete: true,
  })),
);

export const activityFeature = createFeature({
  name: 'activityFeature',
  reducer,
});
