import { SportType } from '../../ranking-store';

export type CalculationType =
  | 'timePaceHillPowerFnEffort'
  | 'timePacePowerFnEffort'
  | 'timeExertionEffort'
  | 'timeEffort';
export type Calculation =
  | TimePaceHillEffortCalculationParams
  | TimePaceEffortCalculationParams
  | TimeExertionEffortCalculationParams
  | TimeEffortCalculationParams;

export interface ActivityDetailsDto {
  id: number;
  type: SportType;
  name: string;
  createdDate: Date;
  supported: boolean;
  distance: number;
  movingTime: number;
  elevationGain: number;
  averageSpeed: number;
  calculation: Calculation;
  requiresEffort: boolean;
}

export interface TimePaceHillEffortCalculationParams
  extends BaseCalculationParams {
  calculationType: 'timePaceHillPowerFnEffort';
  defaultPace: number;
  defaultPacePoints: number;
  movingTime: number;
  pointsForMovingTime: number;
  pointsForAverageSpeed: number;
  pointsForElevationGain: number;
}

export interface TimePaceEffortCalculationParams extends BaseCalculationParams {
  calculationType: 'timePacePowerFnEffort';
  defaultPace: number;
  defaultPacePoints: number;
  movingTime: number;
  pointsForMovingTime: number;
  pointsForAverageSpeed: number;
}

export interface TimeExertionEffortCalculationParams
  extends BaseCalculationParams {
  calculationType: 'timeExertionEffort';
  movingTime: number;

  defaultPoints: number;
  effort?: number;
  pointsForMovingTime: number;
  pointsForEffort: number;
}

export interface TimeEffortCalculationParams extends BaseCalculationParams {
  calculationType: 'timeEffort';
  movingTime: number;

  defaultPoints: number;
}

export interface BaseCalculationParams {
  calculationType: CalculationType;
  totalPoints: number;
}
