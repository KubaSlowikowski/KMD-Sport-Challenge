import { ActivityDetailsDto } from './activity-details.dto';
import { ActivityDto } from './activity.dto';

export class ActivityParser {
  public static parseDetailsToActivity(
    details: ActivityDetailsDto,
  ): ActivityDto {
    return {
      id: details.id,
      type: details.type,
      name: details.name,
      createdDate: details.createdDate,
      points: details.calculation?.totalPoints,
      supported: details.supported,
      distance: details.distance,
      movingTime: details.movingTime,
      effort:
        details.calculation.calculationType === 'timeExertionEffort'
          ? details.calculation.effort
          : undefined,
      requiresEffort: details.requiresEffort,
    };
  }
}
