import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  inject,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { Store } from '@ngrx/store';
import {
  ActivitiesSelector,
  ActivityActions,
  ActivityState,
  CalculationType,
} from '../store';
import { sportTypeConfig } from '../../sport-badge/sport-type.config';
import {
  BehaviorSubject,
  combineLatest,
  distinctUntilChanged,
  filter,
  firstValueFrom,
  map,
  Subscription,
  switchMap,
} from 'rxjs';
import { ActivatedRoute, RouterLink } from '@angular/router';
import { CommonModule } from '@angular/common';
import { SkeletonModule } from 'primeng/skeleton';
import { PointsViewComponent } from '../../../shared/points-view/points-view.component';

@Component({
  selector: 'sports-app-activity-details',
  templateUrl: './activity-details.component.html',
  imports: [CommonModule, RouterLink, SkeletonModule, PointsViewComponent],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ActivityDetailsComponent implements OnInit, OnDestroy {
  @Input() set activityId(value: number) {
    this.activityId$.next(value);
  }

  @Input({ required: true })
  public currentUser: boolean = false;

  @Output() toggle = new EventEmitter<void>();

  private route = inject(ActivatedRoute);
  private subscriptions = new Subscription();
  private store = inject(Store<ActivityState>);
  private sportConfig = sportTypeConfig;
  private activityId$ = new BehaviorSubject<number>(0);

  public vm$ = this.activityId$.pipe(
    switchMap(activityId =>
      combineLatest([
        this.store.select(ActivitiesSelector.getErroredDetails(activityId)),
        this.store.select(ActivitiesSelector.getLoadingDetails(activityId)),
      ]).pipe(
        map(([error, isLoading]) => ({
          error,
          isLoading,
        })),
      ),
    ),
  );

  public data$ = this.activityId$.pipe(
    switchMap(activityId =>
      this.store.select(ActivitiesSelector.getDetailsDto(activityId)).pipe(
        filter(data => data != null),
        map(data => data!),
        map(data => ({
          ...data,
          sportTypeName:
            this.sportConfig.find(c => c.type === data.type)?.name ?? data.type,
        })),
      ),
    ),
  );

  ngOnInit(): void {
    const activityId = this.activityId$.value;
    this.subscriptions.add(
      this.route.params
        .pipe(
          map(p => ({ athleteId: p['athleteId'] })),
          distinctUntilChanged(),
        )
        .subscribe(({ athleteId }) => {
          this.store.dispatch(
            ActivityActions.triggerLoadDetails({ activityId, athleteId }),
          );
        }),
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  public async load(): Promise<void> {
    const activityId = await firstValueFrom(this.activityId$);
    const routeParams = await firstValueFrom(this.route.params);
    this.store.dispatch(
      ActivityActions.loadDetails({
        activityId,
        athleteId: routeParams['athleteId'],
      }),
    );
  }

  public async refresh(): Promise<void> {
    const activityId = await firstValueFrom(this.activityId$);
    const routeParams = await firstValueFrom(this.route.params);
    this.store.dispatch(
      ActivityActions.refresh({
        activityId,
        athleteId: routeParams['athleteId'],
      }),
    );
  }

  public getTime(time: number): string {
    const timeInMinutes = time / 60;
    if (timeInMinutes < 60) {
      return `${Math.floor(timeInMinutes)} min`;
    }

    const hours = Math.floor(timeInMinutes / 60);
    const minutes = Math.floor(timeInMinutes - hours * 60);
    return `${hours}h ${minutes}min`;
  }

  public getCalculationTypeName(calculationType: CalculationType): string {
    switch (calculationType) {
      case 'timeExertionEffort':
        return 'Time and Effort';
      case 'timeEffort':
        return 'Time';
      case 'timePaceHillPowerFnEffort':
        return 'Time, Pace and Hills';
      case 'timePacePowerFnEffort':
        return 'Time and Pace';
      default:
        throw new Error('Not mapped');
    }
  }

  public onToggle(): void {
    this.toggle.emit();
  }
}
