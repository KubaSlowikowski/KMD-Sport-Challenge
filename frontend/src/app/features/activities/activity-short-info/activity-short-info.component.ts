import {
  ChangeDetectionStrategy,
  Component,
  computed,
  input,
} from '@angular/core';
import { SportBadgeComponent } from '../../sport-badge/sport-badge.component';
import { PointsViewComponent } from '../../../shared/points-view/points-view.component';
import { DatePipe } from '@angular/common';
import { ActivityDto } from '../store';

@Component({
  selector: 'app-activity-short-info',
  standalone: true,
  imports: [DatePipe, SportBadgeComponent, PointsViewComponent],
  templateUrl: './activity-short-info.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ActivityShortInfoComponent {
  public $activity = input.required<ActivityDto>({ alias: 'activity' });
  public $showPoints = input(false, { alias: 'showPoints' });
  public $showPhoto = input(false, { alias: 'showPhoto' });
  public $showDate = input(true, { alias: 'showDate' });
  public $photoUrl = computed(
    () => this.$activity().photoUrl?.[600] ?? this.$activity().photoUrl?.[100],
  );

  public getDistance(distance: number): string {
    if (distance >= 1000) {
      return `${Math.round((distance / 1000) * 100) / 100} km`;
    }

    return `${distance} m`;
  }

  public getTime(time: number): string {
    const timeInMinutes = time / 60;
    if (timeInMinutes < 60) {
      return `${Math.floor(timeInMinutes)} min`;
    }

    const hours = Math.floor(timeInMinutes / 60);
    const minutes = Math.floor(timeInMinutes - hours * 60);
    return `${hours}h ${minutes}min`;
  }
}
