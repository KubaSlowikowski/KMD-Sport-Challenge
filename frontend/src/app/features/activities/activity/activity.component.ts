import {
  ChangeDetectionStrategy,
  Component,
  Input,
  computed,
  input,
} from '@angular/core';
import {
  ActivitiesSelector,
  ActivityActions,
  ActivityDto,
  ActivityState,
} from '../store';
import { CommonModule } from '@angular/common';
import { SportBadgeComponent } from '../../sport-badge/sport-badge.component';
import { PointsViewComponent } from '../../../shared/points-view/points-view.component';
import { ActivityDetailsComponent } from '../activity-details/activity-details.component';
import { Store } from '@ngrx/store';
import { ActivityShortInfoComponent } from '../activity-short-info';

@Component({
  selector: 'sports-app-activity',
  templateUrl: './activity.component.html',
  standalone: true,
  imports: [
    CommonModule,
    SportBadgeComponent,
    PointsViewComponent,
    ActivityDetailsComponent,
    ActivityShortInfoComponent,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ActivityComponent {
  public $activity = input.required<ActivityDto>({ alias: 'activity' });

  @Input()
  public currentUser: boolean = false;
  public $layout = input.required<'simple' | 'detailed'>({ alias: 'layout' });

  public $showDetails = computed(() =>
    this.store.selectSignal(
      ActivitiesSelector.getOpenedDetails(this.$activity().id),
    )(),
  );

  constructor(private store: Store<ActivityState>) {}

  public openDetails(): void {
    this.store.dispatch(
      ActivityActions.openDetails({ activityId: this.$activity().id }),
    );
  }

  public closeDetails(): void {
    this.store.dispatch(
      ActivityActions.closeDetails({ activityId: this.$activity().id }),
    );
  }
}
