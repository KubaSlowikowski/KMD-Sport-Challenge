import {
  ChangeDetectionStrategy,
  Component,
  Input,
  inject,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { SkeletonModule } from 'primeng/skeleton';
import {
  ActivitiesSelector,
  ActivityActions,
  ActivityDto,
  ActivityState,
} from '../store';
import { Store } from '@ngrx/store';
import { combineLatest, map } from 'rxjs';
import { SportBadgeComponent } from '../../sport-badge/sport-badge.component';
import { RouterModule } from '@angular/router';
import { PointsViewComponent } from '../../../shared/points-view/points-view.component';
import { ActivityComponent } from '../activity/activity.component';
import { ChallengeContext } from '../../challenges/challenge-logic';

@Component({
  selector: 'sports-app-activities',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,

    PointsViewComponent,
    ActivityComponent,
    SkeletonModule,
    SportBadgeComponent,
  ],
  templateUrl: './activities-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ActivitiesListComponent {
  private store = inject(Store<ActivityState>);
  private challengeContext = inject(ChallengeContext);

  public activities: ActivityDto[] = [];
  public isLoading: boolean = false;
  public athleteIdValue: string | 'me' = 'me';

  public vm$ = combineLatest([
    this.store.select(ActivitiesSelector.getData),
    this.store.select(ActivitiesSelector.getError),
    this.store.select(ActivitiesSelector.getIsLoading),
    this.store.select(ActivitiesSelector.getAthleteId),
  ]).pipe(
    map(([data, error, isLoading, athleteId]) => ({
      data,
      error,
      isLoading,
      athleteId,
    })),
  );

  @Input()
  public set athleteId(athleteId: string | 'me') {
    this.athleteIdValue = athleteId;
    this.store.dispatch(
      ActivityActions.triggerLoad({
        athleteId,
        challengeId: this.challengeContext.challengeId(),
      }),
    );
  }

  public load(): void {
    this.store.dispatch(
      ActivityActions.triggerLoad({
        athleteId: this.athleteIdValue,
        challengeId: this.challengeContext.challengeId(),
        force: true,
      }),
    );
  }

  getErrorStatus(error: any) {
    return (error as any).status;
  }
}
