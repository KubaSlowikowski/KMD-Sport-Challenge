import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SportType } from '../ranking-store';
import { sportTypeConfig } from './sport-type.config';

@Component({
  selector: 'sports-app-sport-badge',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './sport-badge.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SportBadgeComponent {
  @Input({ required: true })
  public sportType: SportType = 'Ride';

  public translateSportType(sportType: SportType | string): string {
    const config = sportTypeConfig.find(c => c.type === sportType);
    return config?.name ?? sportType.replace(/([a-z])([A-Z])/g, '$1 $2');
  }

  public getColor(sportType: SportType): string {
    const config = sportTypeConfig.find(c => c.type === sportType);
    return config?.bgColor ?? 'bg-gray-200';
  }

  public getIcon(sportType: SportType): string {
    const config = sportTypeConfig.find(c => c.type === sportType);
    return config?.icon ?? '';
  }
}
