import { createFeature, createReducer, on } from '@ngrx/store';
import { MissingExertionState } from './missing-exertion.state';
import { MissingExertionActions } from './missing-exertion.actions';

const initialState: MissingExertionState = {
  isLoading: false,
  error: null,
  data: [],
  activityId: null,
  activity: null,
  errorActivity: null,
  loadingActivitiesIds: [],
  isLoadingActivity: false,
};

const reducer = createReducer(
  initialState,
  on(MissingExertionActions.load, state => ({
    ...state,
    isLoading: true,
    error: null,
  })),
  on(MissingExertionActions.loadSuccess, (state, { data }) => ({
    ...state,
    isLoading: false,
    data,
  })),
  on(MissingExertionActions.loadError, (state, { error }) => ({
    ...state,
    isLoading: false,
    error,
  })),
  on(MissingExertionActions.loadActivity, (state, { activityId }) => ({
    ...state,
    activityId,
    isLoadingActivity: true,
    error: null,
  })),
  on(MissingExertionActions.loadActivitySuccess, (state, { activity }) => ({
    ...state,
    isLoadingActivity: false,
    activity,
  })),
  on(MissingExertionActions.loadActivityError, (state, { error }) => ({
    ...state,
    isLoadingActivity: false,
    errorActivity: error,
  })),
  on(MissingExertionActions.setExertion, (state, { activityId }) => ({
    ...state,
    loadingActivitiesIds: [
      ...state.loadingActivitiesIds.filter(a => a !== activityId),
      activityId,
    ],
  })),
  on(MissingExertionActions.setExertionSuccess, (state, { activity }) => ({
    ...state,
    loadingActivitiesIds: state.loadingActivitiesIds.filter(
      a => a !== activity.id,
    ),
    data: state.data.filter(a => a.id !== activity.id),
  })),
  on(
    MissingExertionActions.setExertionError,
    (state, { activityId, error }) => ({
      ...state,
      errorActivity: error,
      loadingActivitiesIds: state.loadingActivitiesIds.filter(
        a => a !== activityId,
      ),
    }),
  ),
  on(MissingExertionActions.refresh, (state, { activityId }) => ({
    ...state,
    loadingActivitiesIds: [
      ...state.loadingActivitiesIds.filter(a => a !== activityId),
      activityId,
    ],
  })),
  on(MissingExertionActions.refreshSuccess, (state, { activity }) => ({
    ...state,
    loadingActivitiesIds: state.loadingActivitiesIds.filter(
      a => a !== activity.id,
    ),
    data: activity.requiresEffort
      ? state.data
      : state.data.filter(a => a.id !== activity.id),
  })),
  on(MissingExertionActions.refreshError, (state, { activityId, error }) => ({
    ...state,
    errorActivity: error,
    loadingActivitiesIds: state.loadingActivitiesIds.filter(
      a => a !== activityId,
    ),
  })),
);

export const missingExertionFeature = createFeature({
  name: 'missingExertionFeature',
  reducer,
});
