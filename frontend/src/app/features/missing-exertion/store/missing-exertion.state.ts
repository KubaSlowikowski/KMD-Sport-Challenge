import { ActivityDto } from '../../activities/store';

export interface MissingExertionState {
  data: ActivityDto[];
  isLoading: boolean;
  error: Error | null;
  activityId: number | null;
  activity: ActivityDto | null;
  isLoadingActivity: boolean;
  errorActivity: Error | null;
  loadingActivitiesIds: number[];
}
