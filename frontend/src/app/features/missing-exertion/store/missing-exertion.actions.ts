import { createAction, props } from '@ngrx/store';
import { ActivityDto } from '../../activities/store';

export class MissingExertionActions {
  public static readonly load = createAction('[Missing Exertion] Load');

  public static readonly loadSuccess = createAction(
    '[Missing Exertion] Load Success',
    props<{ data: ActivityDto[] }>(),
  );

  public static readonly loadError = createAction(
    '[Missing Exertion] Load Error',
    props<{ error: Error }>(),
  );

  public static readonly loadActivity = createAction(
    '[Missing Exertion] Load Activity',
    props<{ activityId: number }>(),
  );

  public static readonly loadActivitySuccess = createAction(
    '[Missing Exertion] Load Activity Success',
    props<{ activity: ActivityDto }>(),
  );

  public static readonly loadActivityError = createAction(
    '[Missing Exertion] Load Activity Error',
    props<{ error: Error }>(),
  );

  public static readonly setExertion = createAction(
    '[Missing Exertion] Set Exertion',
    props<{ activityId: number; exertionLevel: number; navigate: boolean }>(),
  );

  public static readonly setExertionSuccess = createAction(
    '[Missing Exertion] Set Exertion Success',
    props<{ activity: ActivityDto; navigate: boolean }>(),
  );

  public static readonly setExertionError = createAction(
    '[Missing Exertion] Set Exertion Error',
    props<{ activityId: number; error: Error }>(),
  );

  public static readonly refresh = createAction(
    '[Missing Exertion] Refresh',
    props<{ activityId: number; navigate: boolean }>(),
  );

  public static readonly refreshSuccess = createAction(
    '[Missing Exertion] Refresh Success',
    props<{ activity: ActivityDto; navigate: boolean }>(),
  );

  public static readonly refreshError = createAction(
    '[Missing Exertion] Refresh Error',
    props<{ activityId: number; error: Error }>(),
  );
}
