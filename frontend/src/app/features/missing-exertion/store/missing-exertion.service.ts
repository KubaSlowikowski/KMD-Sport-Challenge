import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { ActivityDto } from '../../activities/store';

@Injectable()
export class MissingExertionService {
  private httpClient = inject(HttpClient);

  public get(): Observable<ActivityDto[]> {
    return this.httpClient.get<ActivityDto[]>(
      `${environment.apiUrl}/athlete/me/missingExertionLevelActivities`,
    );
  }

  public setExertion(
    activityId: number,
    exertionLevel: number,
  ): Observable<ActivityDto> {
    return this.httpClient.patch<ActivityDto>(
      `${environment.apiUrl}/athlete/me/activity/${activityId}/setExertion`,
      { exertionLevel },
    );
  }
}
