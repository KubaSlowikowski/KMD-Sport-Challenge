import { createFeatureSelector, createSelector } from '@ngrx/store';
import { MissingExertionState } from './missing-exertion.state';

const getState = createFeatureSelector<MissingExertionState>(
  'missingExertionFeature',
);

export class MissingExertionSelector {
  public static readonly getIsLoading = createSelector(
    getState,
    state => state.isLoading,
  );

  public static readonly getIsLoadingActivity = createSelector(
    getState,
    state => state.isLoadingActivity,
  );

  public static readonly getError = createSelector(
    getState,
    state => state.error,
  );

  public static readonly getData = createSelector(
    getState,
    state => state.data,
  );

  public static readonly getLoadingActivitiesIds = createSelector(
    getState,
    state => state.loadingActivitiesIds,
  );

  public static readonly getActivityId = createSelector(
    getState,
    state => state.activityId,
  );

  public static readonly getErrorActivity = createSelector(
    getState,
    state => state.errorActivity,
  );

  public static readonly getActivity = createSelector(
    getState,
    state => state.activity,
  );
}
