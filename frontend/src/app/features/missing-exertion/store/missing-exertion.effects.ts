import { Injectable, inject } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  catchError,
  exhaustMap,
  filter,
  map,
  mergeMap,
  of,
  switchMap,
  tap,
} from 'rxjs';
import { MissingExertionService } from './missing-exertion.service';
import { MissingExertionActions } from './missing-exertion.actions';
import { ConfirmationDialogService } from '../../confirmation-dialog';
import { Router } from '@angular/router';
import { MissingExertionState } from './missing-exertion.state';
import { Store } from '@ngrx/store';
import { MissingExertionSelector } from './missing-exertion.selector';
import { MessageService } from 'primeng/api';
import { RankingActions } from '../../ranking-store';
import {
  ActivityActions,
  ActivityParser,
  ActivityService,
} from '../../activities/store';

@Injectable()
export class MissingExertionEffects {
  private actions$ = inject(Actions);
  private service = inject(MissingExertionService);
  private activityService = inject(ActivityService);
  private confirmationService = inject(ConfirmationDialogService);
  private router = inject(Router);
  private store = inject(Store<MissingExertionState>);
  private messageService = inject(MessageService);

  load$ = createEffect(() =>
    this.actions$.pipe(
      ofType(MissingExertionActions.load),
      exhaustMap(() =>
        this.service.get().pipe(
          map(data => MissingExertionActions.loadSuccess({ data })),
          catchError(error => of(MissingExertionActions.loadError({ error }))),
        ),
      ),
    ),
  );

  loadActivity$ = createEffect(() =>
    this.actions$.pipe(
      ofType(MissingExertionActions.loadActivity),
      map(({ activityId }) => ({
        activityId,
        activity: this.store
          .selectSignal(MissingExertionSelector.getData)()
          .find(a => a.id === activityId),
      })),
      exhaustMap(({ activityId, activity }) =>
        (activity != null
          ? of(activity)
          : this.activityService.getById('me', activityId)
        ).pipe(
          map(activity =>
            MissingExertionActions.loadActivitySuccess({ activity }),
          ),
          catchError(error =>
            of(MissingExertionActions.loadActivityError({ error })),
          ),
        ),
      ),
    ),
  );

  openDialog$ = createEffect(
    () =>
      this.store.select(MissingExertionSelector.getData).pipe(
        filter(
          data =>
            data.length > 0 && this.router.url !== '/app/missing-exertion/list',
        ),
        tap(data => {
          this.confirmationService.ask({
            title: 'Hey',
            text: `We found ${data.length > 1 ? `some activities (${data.length})` : 'activity'}, which ${data.length > 1 ? 'do not' : 'does not '} have exertion filled. Do you want to fill it now?`,
            approveLabel: "Let's go!",
            declineLabel: 'Not now',
            approveFn: () => {
              this.router.navigate(['/app/missing-exertion/list/']);
            },
          });
        }),
      ),
    { dispatch: false },
  );

  setExertion$ = createEffect(() =>
    this.actions$.pipe(
      ofType(MissingExertionActions.setExertion),
      mergeMap(({ activityId, exertionLevel, navigate }) =>
        this.service.setExertion(activityId, exertionLevel).pipe(
          map(activity =>
            MissingExertionActions.setExertionSuccess({ activity, navigate }),
          ),
          catchError(error =>
            of(MissingExertionActions.setExertionError({ activityId, error })),
          ),
        ),
      ),
    ),
  );

  setExertionSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(MissingExertionActions.setExertionSuccess),
        tap(({ navigate }) => {
          this.messageService.add({
            severity: 'success',
            detail: 'Exertion set',
          });
          this.store.dispatch(RankingActions.markAsObsolete());
          this.store.dispatch(ActivityActions.markAsObsolete());
          if (navigate) {
            this.router.navigate(['/app/profile/me']);
          }
        }),
      ),
    { dispatch: false },
  );

  setExertionError$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(MissingExertionActions.setExertionError),
        tap(() => {
          this.messageService.add({
            severity: 'error',
            detail: 'There was an error when setting exertion',
          });
        }),
      ),
    { dispatch: false },
  );

  refresh$ = createEffect(() =>
    this.actions$.pipe(
      ofType(MissingExertionActions.refresh),
      switchMap(({ activityId, navigate }) =>
        this.activityService.refresh('me', activityId).pipe(
          map(dto =>
            MissingExertionActions.refreshSuccess({
              activity: ActivityParser.parseDetailsToActivity(dto),
              navigate,
            }),
          ),
          catchError(error =>
            of(MissingExertionActions.refreshError({ activityId, error })),
          ),
        ),
      ),
    ),
  );

  refreshSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(MissingExertionActions.refreshSuccess),
        tap(({ navigate }) => {
          this.store.dispatch(RankingActions.markAsObsolete());
          this.store.dispatch(ActivityActions.markAsObsolete());
          if (navigate) {
            this.router.navigate(['/app/profile/me']);
          }
        }),
      ),
    { dispatch: false },
  );

  refreshError$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(MissingExertionActions.setExertionError),
        tap(() => {
          this.messageService.add({
            severity: 'error',
            detail: 'There was an error when refreshing activity',
          });
        }),
      ),
    { dispatch: false },
  );
}
