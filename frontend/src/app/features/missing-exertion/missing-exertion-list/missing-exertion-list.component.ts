import {
  ChangeDetectionStrategy,
  Component,
  computed,
  inject,
} from '@angular/core';
import { Store } from '@ngrx/store';
import {
  MissingExertionActions,
  MissingExertionSelector,
  MissingExertionState,
} from '../store';
import { RouterLink, RouterOutlet } from '@angular/router';
import { SkeletonModule } from 'primeng/skeleton';
import { DatePipe } from '@angular/common';
import { MissingExertionViewComponent } from '../missing-exertion-view';

@Component({
  selector: 'app-missing-exertion-list',
  standalone: true,
  imports: [
    RouterLink,
    RouterOutlet,
    SkeletonModule,
    DatePipe,
    MissingExertionViewComponent,
  ],
  templateUrl: './missing-exertion-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MissingExertionListComponent {
  private store = inject(Store<MissingExertionState>);

  public $activities = this.store.selectSignal(MissingExertionSelector.getData);
  public $isLoading = this.store.selectSignal(
    MissingExertionSelector.getIsLoading,
  );
  public $error = this.store.selectSignal(MissingExertionSelector.getError);

  public load(): void {
    this.store.dispatch(MissingExertionActions.load());
  }
}
