import {
  ChangeDetectionStrategy,
  Component,
  computed,
  effect,
  inject,
  input,
  signal,
} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { SliderModule } from 'primeng/slider';
import { ActivityShortInfoComponent } from '../../activities/activity-short-info';
import { ActivityDto } from '../../activities/store';
import { Store } from '@ngrx/store';
import {
  MissingExertionActions,
  MissingExertionSelector,
  MissingExertionState,
} from '../store';
import { SkeletonModule } from 'primeng/skeleton';
import { sportTypeConfig } from '../../sport-badge/sport-type.config';

@Component({
  selector: 'app-missing-exertion-view',
  standalone: true,
  imports: [
    FormsModule,
    SliderModule,
    SkeletonModule,

    ActivityShortInfoComponent,
  ],
  templateUrl: './missing-exertion-view.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MissingExertionViewComponent {
  private store = inject(Store<MissingExertionState>);

  public $activity = input.required<ActivityDto>({ alias: 'activity' });
  public $navigateToRankingOnSet = input.required<boolean>({
    alias: 'navigateToRankingOnSet',
  });

  public $selectedEffort = signal<number | null>(null);
  public $touched = signal(false);
  public $loadingActivitiesIds = this.store.selectSignal(
    MissingExertionSelector.getLoadingActivitiesIds,
  );
  public $isLoading = computed(() =>
    this.$loadingActivitiesIds().includes(this.$activity().id),
  );
  public $highExertionWarningVisible = computed(() =>
    this.isHighExertionWarningVisible(),
  );

  public exertionLevels = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  public tip = [
    {
      tip: '1 - Very calm, undemanding training.',
      walk: 'Slow walk',
      pointsPerMinute: 3.07,
    },
    {
      tip: '2 - Very calm training.',
      message: 'You can talk normally. Felt very comfortable.',
      ride: 'Avg. 12km/h',
      walk: 'Walk',
      pointsPerMinute: 4.51,
    },
    {
      tip: '3 - Light training.',
      message:
        'The intensity increases slightly. You’re working out, but you can still carry on a conversation without getting winded',
      ride: 'Avg. 17km/h',
      walk: 'Walking uphill (1%-3%)',
      run: '10min/km',
      pointsPerMinute: 6.12,
    },
    {
      tip: '4 - Normal training.',
      message:
        'You can still talk, but it’s getting harder. Your heart rate is elevated.',
      ride: 'Riding (Avg. 15km/h) for entire duration of training.',
      walk: 'Walking uphill (5%)',
      run: 'Running (6min/km) for entire duration of training',
      pointsPerMinute: 8.85,
    },
    {
      tip: '5 - Normal training.',
      message:
        'You can still talk, but it’s getting harder. Your heart rate is elevated.',
      ride: 'Riding (Avg. 24km/h) for entire duration of training.',
      run: 'Running (7min/km) for entire duration of training',
      pointsPerMinute: 9.66,
    },
    {
      tip: '6 - Heavy training.',
      message:
        'You’re pushing hard, and talking is difficult. You’re sweating and your heart rate is high.',
      ride: 'Riding (Avg. 26km/h) for entire duration of training.',
      run: 'Running (6min/km) for entire duration of training',
      pointsPerMinute: 11.56,
    },
    {
      tip: '7 - Heavy training.',
      message:
        'You’re pushing hard, and talking is very difficult. You’re sweating more and your heart rate is high.',
      ride: 'Riding with high speed for entire duration of training. (Avg. 28km/h)',
      run: 'Running (5min/km) for entire duration of training.',
      pointsPerMinute: 13.52,
    },
    {
      tip: '8 - Intensive training 🔥.',
      message:
        'This is near your maximum effort. You’re pushing very hard, talking is almost impossible. You’re sweating and your heart rate is very high.',
      ride: 'Riding with high speed for entire duration of training. (Avg. 31km/h)',
      run: 'Fast running (4min/km) for entire duration of training.',
      pointsPerMinute: 15.54,
    },
    {
      tip: '9 - Very Intensive training 🔥🔥.',
      message:
        'This is near your maximum effort. You can’t talk, your muscles are burning. You can only maintain this level for a short time. You can barely stand after training.',
      ride: 'Riding with high speed for entire duration of training. (Avg. 33km/h)',
      run: 'Fast running (3min/km) for entire duration of training.',
      pointsPerMinute: 17.61,
    },
    {
      tip: '10 - Extremely intensive training 🔥🔥🔥.',
      message:
        'This is your absolute limit. You can only maintain this level for a very short time. You can barely stand after training.',
      ride: 'Riding with high speed for entire duration of training. (Avg. 35km/h)',
      run: 'Sprint (2.5min/km) for entire duration of training.',
      pointsPerMinute: 19.73,
    },
  ];

  constructor() {
    effect(
      () => {
        this.$selectedEffort.set(this.$activity().effort ?? null);
      },
      { allowSignalWrites: true },
    );
  }

  public setEffort(): void {
    this.$touched.set(true);
    const exertionLevel = this.$selectedEffort();
    if (exertionLevel == null) {
      return;
    }

    this.store.dispatch(
      MissingExertionActions.setExertion({
        activityId: this.$activity().id,
        exertionLevel,
        navigate: this.$navigateToRankingOnSet(),
      }),
    );
  }

  public refresh(): void {
    this.store.dispatch(
      MissingExertionActions.refresh({
        activityId: this.$activity().id,
        navigate: this.$navigateToRankingOnSet(),
      }),
    );
  }

  private isHighExertionWarningVisible(): boolean {
    const config = sportTypeConfig.find(c => c.type === this.$activity().type);
    if (config == null) {
      throw new Error('Config not found for activity');
    }

    const exertionTip = config.exertionTips!.find(
      e => e.level === this.$selectedEffort(),
    );
    const activityMovingTime = this.$activity().movingTime;
    console.log(exertionTip, activityMovingTime);
    return (
      exertionTip?.movingTimeWarningInS != null &&
      activityMovingTime > exertionTip.movingTimeWarningInS
    );
  }
}
