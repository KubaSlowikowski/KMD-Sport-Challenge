import { Routes } from '@angular/router';
import { MissingExertionListComponent } from './missing-exertion-list';
import { MissingExertionSingleComponent } from './missing-exertion-single';

export const routes: Routes = [
  {
    path: 'list',
    component: MissingExertionListComponent,
  },
  {
    path: ':id',
    component: MissingExertionSingleComponent,
  },
];
