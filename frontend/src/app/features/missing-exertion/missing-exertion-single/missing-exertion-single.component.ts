import {
  ChangeDetectionStrategy,
  Component,
  effect,
  inject,
} from '@angular/core';
import { Store } from '@ngrx/store';
import {
  MissingExertionActions,
  MissingExertionSelector,
  MissingExertionState,
} from '../store';
import { ActivatedRoute, RouterLink } from '@angular/router';
import { toSignal } from '@angular/core/rxjs-interop';
import { map } from 'rxjs';
import { MissingExertionViewComponent } from '../missing-exertion-view';
import { SkeletonModule } from 'primeng/skeleton';

@Component({
  selector: 'app-missing-exertion-single',
  standalone: true,
  imports: [SkeletonModule, MissingExertionViewComponent, RouterLink],
  templateUrl: './missing-exertion-single.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MissingExertionSingleComponent {
  private store = inject(Store<MissingExertionState>);
  private route = inject(ActivatedRoute);
  public $activityId = toSignal(
    this.route.params.pipe(map(params => +params['id'])),
    {
      initialValue: -1,
    },
  );
  public $activity = this.store.selectSignal(
    MissingExertionSelector.getActivity,
  );
  public $isLoading = this.store.selectSignal(
    MissingExertionSelector.getIsLoadingActivity,
  );
  public $error = this.store.selectSignal(
    MissingExertionSelector.getErrorActivity,
  );

  constructor() {
    effect(
      () => {
        this.load();
      },
      { allowSignalWrites: true },
    );
  }

  public load(): void {
    this.store.dispatch(
      MissingExertionActions.loadActivity({ activityId: this.$activityId() }),
    );
  }
}
