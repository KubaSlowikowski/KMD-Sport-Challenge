import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-mobile-view-port',
  standalone: true,
  imports: [],
  templateUrl: './mobile-view-port.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MobileViewPortComponent {}
