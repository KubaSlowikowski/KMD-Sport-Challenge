import {
  ChangeDetectionStrategy,
  Component,
  computed,
  effect,
  inject,
  input,
} from '@angular/core';
import { FeedDto, HighlightType, ReferenceAthleteDto } from '../models';
import { DatePipe } from '@angular/common';
import { RouterLink } from '@angular/router';
import { Store } from '@ngrx/store';
import { UserSelectors, UserState } from '../../user';
import { FeedStore } from '../+store';
import { SkeletonModule } from 'primeng/skeleton';
import { ActivityShortInfoComponent } from '../../activities/activity-short-info';

type TextPart =
  | { type: 'text'; text: string }
  | { type: 'athlete'; athleteId: number; athleteName: string };

@Component({
  selector: 'app-feed-list-item',
  standalone: true,
  imports: [DatePipe, RouterLink, SkeletonModule, ActivityShortInfoComponent],
  templateUrl: './feed-list-item.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FeedListItemComponent {
  private userStore = inject(Store<UserState>);
  private feedStore = inject(FeedStore);
  public $feed = input.required<FeedDto>({ alias: 'feed' });
  public $isLoading = computed(() =>
    this.feedStore.$loadingActivitiesIds().includes(this.$feed().activityId!),
  );
  public $isErrored = computed(() =>
    this.feedStore.$erroredActivitiesIds().includes(this.$feed().activityId!),
  );
  public $activity = computed(() =>
    this.feedStore.$activites().find(a => a.id === this.$feed().activityId!),
  );

  public $textParts = computed(() => this.getTextParts());
  private $currentUserClaims = this.userStore.selectSignal(
    UserSelectors.getClaims,
  );
  public $currentUserId = computed(() => this.$currentUserClaims()?.athleteId);
  public $highlights = computed(() =>
    (this.$feed().highlights ?? []).filter(
      h =>
        h !== 'PersonalRecordPoints' ||
        !(this.$feed().highlights ?? []).includes('GlobalRecordPoints'),
    ),
  );

  constructor() {
    effect(
      () => {
        if (this.$feed().athlete == null) {
          return;
        }

        this.feedStore.loadActivity({
          athleteId: this.$feed().athlete!.athleteId,
          activityId: this.$feed().activityId!,
        });
      },
      { allowSignalWrites: true },
    );
  }

  public logFeed(): void {
    console.log({ feed: this.$feed(), activity: this.$activity() });
  }

  private getTextParts(): TextPart[] {
    let result: TextPart[] = [
      {
        type: 'text',
        text: '',
      },
    ];
    let isInToken = false;
    let token = '[';

    for (const char of this.$feed().text) {
      if (isInToken) {
        token += char;
        if (char === ']') {
          const athleteIdFromToken = +token
            .replace('[ATHLETE_', '')
            .replace(']', '');
          const athleteToken = this.$feed().athleteTokens?.find(
            t => t.id === athleteIdFromToken,
          );
          if (athleteToken != null) {
            result.push({
              type: 'athlete',
              athleteId: athleteToken.athleteId,
              athleteName: athleteToken.athleteName,
            });
          } else {
            result.push({
              type: 'text',
              text: token,
            });
          }

          result.push({
            type: 'text',
            text: '',
          });

          token = '[';
          isInToken = false;
        }
      } else {
        if (char === '[') {
          isInToken = true;
        } else {
          const previousPart = result[result.length - 1];
          if (previousPart.type === 'text') {
            previousPart.text += char;
          } else {
            throw new Error('Can append only textPart of type text');
          }
        }
      }
    }

    this.removePronouns(result);

    return result;
  }

  private removePronouns(result: TextPart[]) {
    for (const part of result) {
      if (part.type === 'text') {
        part.text = part.text
          .replaceAll('(he/his)', '')
          .replaceAll('(she/her)', '')
          .replaceAll('(they/them)', '');
      }
    }
  }

  public getHighlightText(highlightType: HighlightType): string {
    switch (highlightType) {
      case 'FirstActivity':
        return 'First activity';
      case 'GlobalRecordPoints':
        return 'Global Best';
      case 'PersonalRecordPoints':
        return 'Personal Best';
      case 'RankingAdvancement':
        return 'Level up';
      default:
        throw new Error('Not mapped');
    }
  }

  public getHighlightColor(highlightType: HighlightType): string {
    switch (highlightType) {
      case 'FirstActivity':
        return 'bg-purple-300';
      case 'GlobalRecordPoints':
        return 'bg-lime-300';
      case 'PersonalRecordPoints':
        return 'bg-yellow-300';
      case 'RankingAdvancement':
        return 'bg-red-300';
      default:
        throw new Error('Not mapped');
    }
  }

  public getHighlightIcon(highlightType: HighlightType): string {
    switch (highlightType) {
      case 'FirstActivity':
        return 'ri-cake-3-line';
      case 'GlobalRecordPoints':
        return 'ri-medal-line';
      case 'PersonalRecordPoints':
        return 'ri-boxing-line';
      case 'RankingAdvancement':
        return 'ri-game-line';
      default:
        throw new Error('Not mapped');
    }
  }
}
