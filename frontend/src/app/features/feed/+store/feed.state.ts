import { ActivityDto } from '../../activities/store';
import { FeedDto } from '../models';

export interface FeedState {
  isLoading: boolean;
  feeds: FeedDto[];
  error: Error | null;
  pageSize: number;
  count: number;
  currentPage: number;
  activities: ActivityDto[];
  loadingActivitiesIds: number[];
  erroredActivitiesIds: number[];
}
