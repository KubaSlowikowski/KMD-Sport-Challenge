import { Injectable, inject } from '@angular/core';
import { patchState, signalState } from '@ngrx/signals';
import { FeedState } from './feed.state';
import { rxMethod } from '@ngrx/signals/rxjs-interop';
import { filter, mergeMap, pipe, switchMap, tap } from 'rxjs';
import { FeedService } from '../logic';
import { tapResponse } from '@ngrx/operators';
import { ActivityService } from '../../activities/store';

const initialState: FeedState = {
  isLoading: false,
  feeds: [],
  error: null,
  count: 0,
  pageSize: 0,
  currentPage: 0,
  activities: [],
  loadingActivitiesIds: [],
  erroredActivitiesIds: [],
};

@Injectable()
export class FeedStore {
  private readonly state = signalState(initialState);
  private readonly feedService = inject(FeedService);
  private readonly activityService = inject(ActivityService);

  public $isLoading = this.state.isLoading;
  public $feeds = this.state.feeds;
  public $error = this.state.error;
  public $pageSize = this.state.pageSize;
  public $count = this.state.count;
  public $currentPage = this.state.currentPage;
  public $activites = this.state.activities;
  public $loadingActivitiesIds = this.state.loadingActivitiesIds;
  public $erroredActivitiesIds = this.state.erroredActivitiesIds;

  public load = rxMethod<{ currentPage: number; challengeId: string }>(
    pipe(
      tap(({ currentPage }) =>
        patchState(this.state, { currentPage, isLoading: true }),
      ),
      switchMap(({ currentPage, challengeId }) =>
        this.feedService.get(currentPage, challengeId).pipe(
          tapResponse({
            next: result =>
              patchState(this.state, {
                pageSize: result.pageSize,
                count: result.count,
                feeds: result.items,
                error: null,
                activities: [],
              }),
            error: (error: Error) => {
              patchState(this.state, { error });
            },
            finalize: () => patchState(this.state, { isLoading: false }),
          }),
        ),
      ),
    ),
  );

  public loadActivity = rxMethod<{ athleteId: number; activityId: number }>(
    pipe(
      filter(
        ({ activityId }) =>
          !this.$loadingActivitiesIds().includes(activityId) &&
          !this.$erroredActivitiesIds().includes(activityId) &&
          !this.$activites().some(a => a.id === activityId),
      ),
      tap(({ activityId }) =>
        patchState(this.state, state => ({
          ...state,
          loadingActivitiesIds: [...state.loadingActivitiesIds, activityId],
        })),
      ),
      mergeMap(({ athleteId, activityId }) =>
        this.activityService.getById(athleteId, activityId).pipe(
          tapResponse({
            next: activity =>
              patchState(this.state, state => ({
                ...state,
                activities: [...state.activities, activity],
              })),
            error: () =>
              patchState(this.state, state => ({
                ...state,
                erroredActivitiesIds: [
                  ...state.erroredActivitiesIds,
                  activityId,
                ],
              })),
            finalize: () =>
              patchState(this.state, state => ({
                ...state,
                loadingActivitiesIds: state.loadingActivitiesIds.filter(
                  a => a !== activityId,
                ),
              })),
          }),
        ),
      ),
    ),
  );
}
