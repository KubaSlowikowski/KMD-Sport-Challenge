import {
  ChangeDetectionStrategy,
  Component,
  computed,
  effect,
  inject,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { toSignal } from '@angular/core/rxjs-interop';
import { map } from 'rxjs';
import { FeedStore } from '../+store';
import { PaginatorModule } from 'primeng/paginator';
import { SkeletonModule } from 'primeng/skeleton';
import { FeedListItemComponent } from '../feed-list-item/feed-list-item.component';
import { ChallengeContext } from '../../challenges/challenge-logic';

@Component({
  selector: 'app-feed-list',
  standalone: true,
  imports: [FeedListItemComponent, PaginatorModule, SkeletonModule],
  templateUrl: './feed-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FeedListComponent {
  private route = inject(ActivatedRoute);
  private router = inject(Router);
  private store = inject(FeedStore);
  private challengeContext = inject(ChallengeContext);

  private $currentPage = toSignal(
    this.route.queryParams.pipe(
      map(q => q['page']),
      map(page => page ?? 0),
    ),
  );

  public $totalRecords = this.store.$count;
  public $pageSize = this.store.$pageSize;
  public $first = computed(
    () => this.store.$currentPage() * this.store.$pageSize(),
  );
  public $isLoading = this.store.$isLoading;
  public $error = this.store.$error;
  public $feeds = this.store.$feeds;

  constructor() {
    effect(() => this.load(), { allowSignalWrites: true });
  }

  public load(): void {
    this.store.load({
      currentPage: this.$currentPage(),
      challengeId: this.challengeContext.challengeId(),
    });
  }

  public pageChanged(event: { page?: number }): void {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {
        page: event.page,
      },
      queryParamsHandling: 'merge',
    });
  }
}
