import { Routes } from '@angular/router';
import { FeedListComponent } from './feed-list';

export const FEED_LIST_ROUTES: Routes = [
  {
    path: '',
    component: FeedListComponent,
  },
];
