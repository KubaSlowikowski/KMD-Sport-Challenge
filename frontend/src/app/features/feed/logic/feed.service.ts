import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { Observable, of } from 'rxjs';
import { FeedDto } from '../models';
import { environment } from '../../../../environments/environment';
import { PagedResultDto } from '../../../shared/models/paged-result.dto';

@Injectable()
export class FeedService {
  private httpClient = inject(HttpClient);

  public get(
    page: number,
    challengeId: string,
  ): Observable<PagedResultDto<FeedDto>> {
    let httpParams = new HttpParams();
    httpParams = httpParams.set('challengeId', challengeId);

    return this.httpClient.get<PagedResultDto<FeedDto>>(
      `${environment.apiUrl}/feed/${page}`,
      { params: httpParams },
    );
  }
}
