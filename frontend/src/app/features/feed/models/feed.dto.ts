import { AthleteTokenDto } from './athlete-token.dto';
import { HighlightType } from './highlight.type';
import { ReferenceAthleteDto } from './reference-athlete.dto';

export interface FeedDto {
  id: string;
  text: string;
  referencedAthletes: ReferenceAthleteDto[];
  createdDate: Date;

  highlights?: HighlightType[];
  athlete?: ReferenceAthleteDto;
  activityId?: number;
  athleteTokens?: AthleteTokenDto[];
}
