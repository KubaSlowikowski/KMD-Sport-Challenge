import { SportType } from '../../ranking-store';

export interface PrefeedRequestActivityModel {
  activityId: number;
  sportType: SportType;
  supported: boolean;
  points: number;
  createdDate: Date;
  endDate: Date;
}
