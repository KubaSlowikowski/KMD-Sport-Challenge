export interface AthleteTokenDto {
  id: number;
  tokenText: string;
  athleteId: number;
  athleteName: string;
}
