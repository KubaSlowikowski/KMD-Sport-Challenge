export type HighlightType =
  | 'FirstActivity'
  | 'PersonalRecordPoints'
  | 'RankingAdvancement'
  | 'GlobalRecordPoints';
