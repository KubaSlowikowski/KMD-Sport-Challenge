import {
  ChangeDetectionStrategy,
  Component,
  inject,
  OnInit,
} from '@angular/core';
import { RouterModule } from '@angular/router';
import { Store } from '@ngrx/store';
import { UserActions, UserState } from '../user';
import { MissingExertionActions } from '../missing-exertion/store';
import { MobileViewPortComponent } from '../mobile-view-port/mobile-view-port.component';

@Component({
  selector: 'sports-app-sport-app',
  templateUrl: './sport-app.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [RouterModule, MobileViewPortComponent],
})
export class SportAppComponent implements OnInit {
  private store = inject(Store<UserState>);

  ngOnInit(): void {
    this.store.dispatch(UserActions.verifyExistingToken());
    this.store.dispatch(MissingExertionActions.load());
  }
}
