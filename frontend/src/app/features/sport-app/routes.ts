import { Routes } from '@angular/router';
import { SportAppComponent } from './sport-app.component';
import {
  RankingEffects,
  RankingService,
  rankingFeature,
} from '../ranking-store';
import { provideEffects } from '@ngrx/effects';
import { provideState } from '@ngrx/store';
import {
  MissingExertionEffects,
  MissingExertionService,
  missingExertionFeature,
} from '../missing-exertion/store';
import {
  ActivityEffects,
  ActivityService,
  activityFeature,
} from '../activities/store';
import { FeedService } from '../feed/logic';
import { FeedStore } from '../feed/+store';
import { RankingLogic } from '../ranking-store/ranking.logic';

export const routes: Routes = [
  {
    path: '',
    component: SportAppComponent,
    children: [
      {
        path: 'settings',
        loadChildren: () =>
          import('../user-settings/routes').then(mod => mod.routes),
      },
      {
        path: 'challenge',
        children: [
          {
            path: 'form',
            loadChildren: () =>
              import('../challenges/challenge-form').then(mod => mod.routes),
          },
          {
            path: 'join',
            loadChildren: () =>
              import('../challenges/challenge-join').then(mod => mod.routes),
          },
          {
            path: ':challengeId',
            loadChildren: () =>
              import('../challenges/challenge-dashboard/routes').then(
                mod => mod.routes,
              ),
          },
        ],
      },
      {
        path: 'dashboard',
        loadChildren: () => import('../user-dashboard').then(mod => mod.routes),
      },
      {
        path: '**',
        redirectTo: 'dashboard',
      },
    ],
    providers: [
      provideState(rankingFeature),
      provideEffects(RankingEffects),
      RankingService,
      RankingLogic,
      provideState(missingExertionFeature),
      provideEffects(MissingExertionEffects),
      MissingExertionService,
      provideState(activityFeature),
      provideEffects(ActivityEffects),
      ActivityService,
      FeedStore,
      FeedService,
    ],
  },
];
