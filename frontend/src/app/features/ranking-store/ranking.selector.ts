import { createFeatureSelector, createSelector } from '@ngrx/store';
import { RankingState } from './ranking.state';

const getState = createFeatureSelector<RankingState>('rankingFeature');

export class RankingSelector {
  public static readonly getIsLoading = createSelector(
    getState,
    state => state.isLoading,
  );

  public static readonly getError = createSelector(
    getState,
    state => state.error,
  );

  public static readonly getData = createSelector(
    getState,
    state => state.data,
  );

  public static readonly getIsLoaded = createSelector(
    getState,
    state => state.isLoaded,
  );

  public static readonly isObsolete = createSelector(
    getState,
    state => state.obsolete,
  );

  public static readonly getSelectedCategoryName = createSelector(
    getState,
    state => state.selectedCategoryName,
  );
}
