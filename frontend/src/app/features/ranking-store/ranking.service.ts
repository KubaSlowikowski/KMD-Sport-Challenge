import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { RankingDto } from './ranking.dto';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable()
export class RankingService {
  private httpClient = inject(HttpClient);

  public get(challengeId: string): Observable<RankingDto[]> {
    let httpParams = new HttpParams();
    httpParams = httpParams.set('challengeId', challengeId);
    return this.httpClient.get<RankingDto[]>(
      `${environment.apiUrl}/challenge/ranking`,
      { params: httpParams },
    );
  }
}
