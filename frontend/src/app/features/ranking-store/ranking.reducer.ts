import { createFeature, createReducer, on } from '@ngrx/store';
import { RankingState } from './ranking.state';
import { RankingActions } from './ranking.actions';
import { RankingCategoryName } from '../ranking/ranking-categories-config';

const initialState: RankingState = {
  isLoading: false,
  error: null,
  data: [],
  isLoaded: false,
  obsolete: false,
  selectedCategoryName: RankingCategoryName.GENERAL,
};

const reducer = createReducer(
  initialState,
  on(RankingActions.load, state => ({
    ...state,
    isLoading: true,
    error: null,
  })),
  on(RankingActions.loadSuccess, (state, { data }) => ({
    ...state,
    isLoading: false,
    data,
    isLoaded: true,
    obsolete: false,
  })),
  on(RankingActions.loadError, (state, { error }) => ({
    ...state,
    isLoading: false,
    error,
    isLoaded: true,
  })),
  on(RankingActions.markAsObsolete, state => ({
    ...state,
    obsolete: true,
  })),
  on(RankingActions.selectCategory, (state, { categoryName }) => ({
    ...state,
    selectedCategoryName: categoryName,
  })),
);

export const rankingFeature = createFeature({
  name: 'rankingFeature',
  reducer,
});
