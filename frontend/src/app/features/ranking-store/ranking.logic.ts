import { Injectable } from '@angular/core';
import { RankingActivityDto, RankingDto } from './ranking.dto';
import {
  rankingCategoriesConfig,
  RankingCategoryConfig,
  RankingCategoryName,
} from '../ranking/ranking-categories-config';
import { Ranking, RankingPosition } from './ranking.state';

@Injectable()
export class RankingLogic {
  public filterRankingByCategory(
    ranking: Ranking,
    categoryName: RankingCategoryName,
  ): Ranking {
    const category: RankingCategoryConfig | undefined =
      rankingCategoriesConfig.find(c => c.categoryName === categoryName);
    if (category == null) {
      throw new Error(`Category with name ${categoryName} not found`);
    }

    const filteredRanking: Ranking = ranking.map(rankingPosition => ({
      ...rankingPosition,
      activities: filterActivitiesBySportType(rankingPosition, category),
    }));

    return filteredRanking;

    function filterActivitiesBySportType(
      rankingPosition: RankingPosition,
      category: RankingCategoryConfig,
    ): RankingActivityDto[] {
      if (category.sportTypes === null) {
        return rankingPosition.activities;
      }

      return rankingPosition.activities.filter(activity =>
        category.sportTypes?.includes(activity.type),
      );
    }
  }

  public calculateRanking(ranking: Ranking): Ranking {
    const rankingWithCountedPoints: Ranking = ranking.map(r => ({
      ...r,
      points: this.sumPoints(r.activities),
      latterlyPoints: this.sumLatterlyPoints(r.activities),
    }));

    const newRanking: Ranking = this.calculatePositions(
      rankingWithCountedPoints,
    );
    return newRanking;
  }

  private sumPoints(activities: RankingActivityDto[]): number {
    return activities.reduce((p, c) => p + c.points, 0);
  }

  private sumLatterlyPoints(activities: RankingActivityDto[]): number {
    return activities.reduce((p, c) => p + c.latterlyPoints, 0);
  }

  private calculatePositions(ranking: Ranking): Ranking {
    const currentRanking = [...ranking].sort((a, b) => b.points - a.points);
    const latterlyRanking = [...ranking].sort(
      (a, b) => b.latterlyPoints - a.latterlyPoints,
    );

    return currentRanking.map(r => ({
      athleteId: r.athleteId,
      athleteName: r.athleteName,
      imageUrl: r.imageUrl,
      activities: r.activities,
      points: r.points,
      position: currentRanking.findIndex(v => v.athleteId === r.athleteId),
      latterlyPoints: r.latterlyPoints,
      latterlyPosition: latterlyRanking.findIndex(
        v => v.athleteId === r.athleteId,
      ),
    }));
  }

  public mapFromDto(rankingDtos: RankingDto[]): Ranking {
    return rankingDtos.map(r => ({
      ...r,
      points: 0,
      position: 0,
      latterlyPoints: 0,
      latterlyPosition: 0,
    }));
  }
}
