import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { Ranking, RankingState } from './ranking.state';
import { RankingActions } from './ranking.actions';
import { catchError, exhaustMap, filter, map, of, withLatestFrom } from 'rxjs';
import { RankingService } from './ranking.service';
import { RankingSelector } from './ranking.selector';
import { RankingDto } from './ranking.dto';
import { RankingLogic } from './ranking.logic';

@Injectable()
export class RankingEffects {
  constructor(
    private actions$: Actions,
    private store: Store<RankingState>,
    private service: RankingService,
    private logic: RankingLogic,
  ) {}

  init$ = createEffect(() =>
    this.actions$.pipe(
      ofType(RankingActions.init),
      withLatestFrom(
        this.store.select(RankingSelector.getIsLoaded),
        this.store.select(RankingSelector.isObsolete),
      ),
      filter(([, isLoaded, obsolete]) => !isLoaded || obsolete),
      map(([{ challengeId }]) => RankingActions.load({ challengeId })),
    ),
  );

  load$ = createEffect(() =>
    this.actions$.pipe(
      ofType(RankingActions.load),
      exhaustMap(({ challengeId }) =>
        this.service.get(challengeId).pipe(
          map((data: RankingDto[]) => this.logic.mapFromDto(data)),
          map((data: Ranking) => this.logic.calculateRanking(data)),
          map((data: Ranking) => RankingActions.loadSuccess({ data })),
          catchError(error => of(RankingActions.loadError({ error }))),
        ),
      ),
    ),
  );
}
