import { RankingActivityDto } from './ranking.dto';
import { RankingCategoryName } from '../ranking/ranking-categories-config';

export interface RankingState {
  isLoading: boolean;
  error: Error | null;
  data: Ranking;
  isLoaded: boolean;
  obsolete: boolean;
  selectedCategoryName: RankingCategoryName;
}

export type Ranking = RankingPosition[];

export interface RankingPosition {
  athleteId: number;
  athleteName: string;
  imageUrl: string;
  activities: RankingActivityDto[];
  points: number;
  position: number;
  latterlyPoints: number;
  latterlyPosition: number;
}
