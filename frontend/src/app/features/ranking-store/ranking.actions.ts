import { createAction, props } from '@ngrx/store';
import { RankingCategoryName } from '../ranking/ranking-categories-config';
import { Ranking } from './ranking.state';

export class RankingActions {
  public static readonly init = createAction(
    '[Ranking] Init',
    props<{ challengeId: string }>(),
  );

  public static readonly load = createAction(
    '[Ranking] Load',
    props<{ challengeId: string }>(),
  );

  public static readonly loadSuccess = createAction(
    '[Ranking] Load Success',
    props<{ data: Ranking }>(),
  );

  public static readonly loadError = createAction(
    '[Ranking] Load Success Error',
    props<{ error: Error }>(),
  );

  public static readonly markAsObsolete = createAction(
    '[Ranking] Mark As Obsolete',
  );

  public static readonly selectCategory = createAction(
    '[Ranking] Set selected category',
    props<{ categoryName: RankingCategoryName }>(),
  );
}
