export interface RankingDto {
  athleteId: number;
  athleteName: string;
  imageUrl: string;
  activities: RankingActivityDto[];
}

export interface RankingActivityDto {
  type: SportType;
  points: number;
  latterlyPoints: number;
}

export type SportType =
  | 'Ride'
  | 'Walk'
  | 'Run'
  | 'Swim'
  | 'WeightTraining'
  | 'Workout'
  | 'Yoga'
  | 'Tennis'
  | 'Soccer'
  | 'TrailRun'
  | 'Pilates'
  | 'VirtualRide'
  | 'GravelRide'
  | 'Hike'
  | 'MountainBikeRide'
  | 'RollerSki'
  | 'Rowing'
  | 'VirtualRow'
  | 'Kitesurf'
  | 'StandUpPaddling'
  | 'Windsurf'
  | 'StairStepper'
  | 'HighIntensityIntervalTraining'
  | 'Squash'
  | 'AlpineSki'
  | 'NordicSki'
  | 'BackcountrySki'
  | 'IceSkate'
  | 'InlineSkate'
  | 'Snowboard'
  | 'VirtualRun'
  | 'TableTennis'
  | 'Snowshoe'
  | 'Golf'
  | 'Badminton'
  | 'Crossfit'
  | 'Elliptical'
  | 'Sail'
  | 'RockClimbing'
  | 'Handcycle'
  | 'EBikeRide'
  | 'EMountainBikeRide'
  | 'Velomobile'
  | 'Wheelchair'
  | 'Pickleball'
  | 'Racquetball'
  | 'Skateboard'
  | 'Canoeing'
  | 'Kayaking';
