import { TransLocoProvider, featureLoader } from 'src/app/features/transloco';

export const LANDING_PAGE_TRANSLATION_SCOPE = 'landingPage';
const loader = () => featureLoader(lang => import(`./${lang}.json`));
export const LandingPageTranslocoProvider = TransLocoProvider(
  LANDING_PAGE_TRANSLATION_SCOPE,
  () => loader(),
);
