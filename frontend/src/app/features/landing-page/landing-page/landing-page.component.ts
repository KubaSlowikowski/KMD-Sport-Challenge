import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { RouterLink } from '@angular/router';
import {
  LANDING_PAGE_TRANSLATION_SCOPE,
  LandingPageTranslocoProvider,
} from './i18n';
import { TranslocoModule, TranslocoService } from '@jsverse/transloco';

@Component({
  selector: 'app-landing-page',
  standalone: true,
  imports: [TranslocoModule, RouterLink],
  templateUrl: './landing-page.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [LandingPageTranslocoProvider],
})
export class LandingPageComponent {
  public scope = LANDING_PAGE_TRANSLATION_SCOPE;
  private translocoService = inject(TranslocoService);

  public setLanguage(language: string): void {
    this.translocoService.setActiveLang(language);
  }
}
