import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
  inject,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Store } from '@ngrx/store';
import { combineLatest, distinctUntilChanged, map, withLatestFrom } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import {
  RankingActions,
  RankingSelector,
  RankingState,
} from '../ranking-store';
import { UserSelectors } from '../user';
import { SkeletonModule } from 'primeng/skeleton';
import { PointsViewComponent } from '../../shared/points-view/points-view.component';
import { ActivitiesListComponent } from '../activities/activities-list/activities-list.component';
import { toSignal } from '@angular/core/rxjs-interop';
import { ChallengeContext } from '../challenges/challenge-logic';

@Component({
  selector: 'sports-app-profile',
  standalone: true,
  imports: [
    CommonModule,
    PointsViewComponent,
    ActivitiesListComponent,
    SkeletonModule,
  ],
  templateUrl: './profile.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProfileComponent implements OnInit {
  private route = inject(ActivatedRoute);
  private challengeContext = inject(ChallengeContext);

  private athlete$ = this.route.params.pipe(
    map(params => params['athleteId'] as string),
    distinctUntilChanged(),
    withLatestFrom(this.store.select(UserSelectors.getClaims)),
    map(([athleteId, user]) => ({
      athleteId,
      realAthleteId:
        athleteId === 'me' ? user!.athleteId.toString() : athleteId,
    })),
  );

  public athleteData$ = combineLatest([
    this.store.select(RankingSelector.getData),
    this.athlete$,
  ]).pipe(
    map(([data, athlete]) => ({
      data,
      athleteData: data.find(
        d => d.athleteId.toString() === athlete.realAthleteId,
      ),
    })),
    map(args => ({
      ...args.athleteData,
      place:
        args.data.findIndex(r => r.athleteId === args.athleteData?.athleteId) +
        1,
    })),
  );

  public vm$ = combineLatest([
    this.store.select(RankingSelector.getIsLoading),
    this.store.select(RankingSelector.getError),
    this.athlete$,
  ]).pipe(
    map(([isLoading, error, athlete]) => ({
      isLoading,
      error,
      athlete,
    })),
  );

  constructor(private store: Store<RankingState>) {}

  ngOnInit(): void {
    this.store.dispatch(
      RankingActions.init({ challengeId: this.challengeContext.challengeId() }),
    );
  }

  public load(): void {
    this.store.dispatch(
      RankingActions.load({ challengeId: this.challengeContext.challengeId() }),
    );
  }

  public getPlaceSuffix(place: number) {
    const penultimateDigit =
      place.toString().length > 1
        ? place.toString()[place.toString().length - 2]
        : '0';
    if (penultimateDigit == '1') {
      return 'th';
    }

    const lastDigit = place.toString()[place.toString().length - 1];
    switch (lastDigit) {
      case '1':
        return 'st';
      case '2':
        return 'nd';
      case '3':
        return 'rd';
      default:
        return 'th';
    }
  }
}
