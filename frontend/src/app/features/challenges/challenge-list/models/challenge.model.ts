export interface ChallengeModel {
  id: string;
  name: string;
  userIds: number[];
  ownerId: number;
  joinPassword: string;
  startDate: string;
  endDate: string;
}
