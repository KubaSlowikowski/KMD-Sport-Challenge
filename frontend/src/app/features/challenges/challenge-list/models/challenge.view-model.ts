export interface ChallengeViewModel {
  id: string;
  name: string;
  userIds: number[];
  ownerId: number;
  joinPassword: string;
  startDate: Date;
  endDate: Date;
}
