import { TransLocoProvider, featureLoader } from 'src/app/features/transloco';

export const CHALLENGE_LIST_TRANSLATION_SCOPE = 'challengeList';
const loader = () => featureLoader(lang => import(`./${lang}.json`));
export const ChallengeListTranslocoProvider = TransLocoProvider(
  CHALLENGE_LIST_TRANSLATION_SCOPE,
  () => loader(),
);
