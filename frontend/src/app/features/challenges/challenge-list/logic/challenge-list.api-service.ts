import { Injectable, inject } from '@angular/core';
import { ChallengeViewModel } from '../models';
import { Observable, map, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({ providedIn: 'root' })
export class ChallengeListApiService {
  private httpClient = inject(HttpClient);

  public getAll(): Observable<ChallengeViewModel[]> {
    /*this.httpClient
      .get<ChallengeModel[]>(`${environment.apiUrl}/challenges`)*/
    return of([
      {
        id: 'id1',
        name: 'Challenge Name 1',
        startDate: '2024-09-19T10:50:54.844Z',
        endDate: '2024-09-29T10:50:54.844Z',
        userIds: [],
        ownerId: 123,

        joinPassword: 'secret',
      },
      {
        id: 'id2',
        name: 'My Challenge Name 2',
        startDate: '2024-09-20T10:50:54.844Z',
        endDate: '2024-09-23T10:50:54.844Z',
        userIds: [],
        ownerId: 321,

        joinPassword: 'secret2',
      },
    ]).pipe(
      map(result =>
        result.map(r => ({
          ...r,
          startDate: new Date(r.startDate),
          endDate: new Date(r.endDate),
        })),
      ),
    );
  }

  public remove(): Observable<void> {
    return this.httpClient.delete<void>(`${environment.apiUrl}/challenges`);
  }
}
