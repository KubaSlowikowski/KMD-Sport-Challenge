import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { ChallengeListApiService } from '../logic';
import { AsyncPipe } from '@angular/common';
import { ChallengeViewModel } from '../models';
import { ConfirmationDialogService } from 'src/app/features/confirmation-dialog';
import {
  CHALLENGE_LIST_TRANSLATION_SCOPE,
  ChallengeListTranslocoProvider,
} from '../i18n';
import { TranslocoModule } from '@jsverse/transloco';
import { ChallengeFormDumbComponent } from '../../challenge-form/challenge-form-dumb/challenge-form-dumb.component';
import { ChallengeListDumbComponent } from '../challenge-list-dumb/challenge-list-dumb.component';

@Component({
  selector: 'app-challenge-list-smart',
  standalone: true,
  imports: [
    AsyncPipe,
    TranslocoModule,
    ChallengeFormDumbComponent,
    ChallengeListDumbComponent,
  ],
  templateUrl: './challenge-list-smart.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [ChallengeListTranslocoProvider],
})
export class ChallengeListSmartComponent {
  public service = inject(ChallengeListApiService);
  private confirmationService = inject(ConfirmationDialogService);

  public scope = CHALLENGE_LIST_TRANSLATION_SCOPE;

  public removeChallenge(challenge: ChallengeViewModel): void {
    this.confirmationService.ask({
      title: `${CHALLENGE_LIST_TRANSLATION_SCOPE}.REMOVE_CONFIRMATION_TITLE`,
      text: `${CHALLENGE_LIST_TRANSLATION_SCOPE}.REMOVE_CONFIRMATION_TEXT`,
      textTranslationArgs: { challengeName: challenge.name },
      approveFn: () => {
        this.service.remove().subscribe();
      },
    });
  }
}
