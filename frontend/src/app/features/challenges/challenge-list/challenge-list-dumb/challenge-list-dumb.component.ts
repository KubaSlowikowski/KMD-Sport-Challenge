import {
  ChangeDetectionStrategy,
  Component,
  input,
  output,
  signal,
} from '@angular/core';
import { ChallengeViewModel } from '../models';
import { TranslocoModule } from '@jsverse/transloco';
import { RouterLink } from '@angular/router';
import { ButtonModule } from 'primeng/button';
import { CHALLENGE_LIST_TRANSLATION_SCOPE } from '../i18n';

@Component({
  selector: 'app-challenge-list-dumb',
  standalone: true,
  imports: [RouterLink, ButtonModule, TranslocoModule],
  templateUrl: './challenge-list-dumb.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChallengeListDumbComponent {
  public challenges = input.required<ChallengeViewModel[]>();
  public challengeRemoved = output<ChallengeViewModel>();

  public scope = CHALLENGE_LIST_TRANSLATION_SCOPE;

  public remove(challenge: ChallengeViewModel): void {
    this.challengeRemoved.emit(challenge);
  }
}
