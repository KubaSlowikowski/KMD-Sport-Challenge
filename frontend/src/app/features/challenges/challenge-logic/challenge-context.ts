import { Injectable, computed, signal } from '@angular/core';

@Injectable()
export class ChallengeContext {
  private challengeIdSignal = signal<string | null>(null);
  public challengeId = computed(() => {
    const challengeId = this.challengeIdSignal();
    if (challengeId == null) {
      throw new Error('ChallengeId was not set');
    }

    return challengeId;
  });

  public set(challengeId: string): void {
    this.challengeIdSignal.set(challengeId);
  }
}
