import { Routes } from '@angular/router';
import { ChallengeJoinSmartComponent } from './challenge-join-smart';

export const routes: Routes = [
  {
    path: '',
    component: ChallengeJoinSmartComponent,
  },
];
