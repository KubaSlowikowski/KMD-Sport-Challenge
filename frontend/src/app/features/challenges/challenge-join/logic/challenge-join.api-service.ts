import { Injectable, inject } from '@angular/core';
import { JoinChallengeModel } from '../models';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({ providedIn: 'root' })
export class ChallengeJoinApiService {
  private httpClient = inject(HttpClient);

  public join(model: JoinChallengeModel): Observable<void> {
    return this.httpClient.put<void>(
      `${environment.apiUrl}/challenges/join`,
      model,
    );
  }
}
