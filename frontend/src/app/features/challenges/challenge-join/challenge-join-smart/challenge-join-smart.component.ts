import { Component, inject } from '@angular/core';
import { JoinChallengeModel } from '../models';
import { ChallengeFormDumbComponent as ChallengeJoinDumbComponent } from '../challenge-join-dumb';
import { ChallengeJoinApiService as ChallengeJoinApiService } from '../logic';
import { ChallengeJoinTranslocoProvider } from '../i18n';
import { ActivatedRoute } from '@angular/router';
import { filter, map } from 'rxjs';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

@Component({
  selector: 'app-challenge-join-smart',
  standalone: true,
  imports: [ChallengeJoinDumbComponent],
  templateUrl: './challenge-join-smart.component.html',
  providers: [ChallengeJoinTranslocoProvider],
})
export class ChallengeJoinSmartComponent {
  private service = inject(ChallengeJoinApiService);
  private route = inject(ActivatedRoute);

  private queryPassword$ = this.route.queryParams.pipe(
    map(query => query['c']),
    filter(password => password != null),
  );

  constructor() {
    this.queryPassword$.pipe(takeUntilDestroyed()).subscribe(password => {
      this.join({ password });
    });
  }

  public join(value: JoinChallengeModel): void {
    this.service.join(value).subscribe();
  }
}
