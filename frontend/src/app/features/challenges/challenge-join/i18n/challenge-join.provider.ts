import { TransLocoProvider, featureLoader } from 'src/app/features/transloco';

export const CHALLENGE_JOIN_TRANSLATION_SCOPE = 'challengeJoin';
const loader = () => featureLoader(lang => import(`./${lang}.json`));
export const ChallengeJoinTranslocoProvider = TransLocoProvider(
  CHALLENGE_JOIN_TRANSLATION_SCOPE,
  () => loader(),
);
