import {
  ChangeDetectionStrategy,
  Component,
  inject,
  output,
} from '@angular/core';
import {
  FormsModule,
  NonNullableFormBuilder,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { TranslocoModule } from '@jsverse/transloco';
import { ButtonModule } from 'primeng/button';
import { CalendarModule } from 'primeng/calendar';
import { InputTextModule } from 'primeng/inputtext';
import { CHALLENGE_JOIN_TRANSLATION_SCOPE } from '../i18n';
import { JoinChallengeModel } from '../models';
import { RippleModule } from 'primeng/ripple';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-challenge-join-dumb',
  standalone: true,
  imports: [
    FormsModule,
    ReactiveFormsModule,
    InputTextModule,
    ButtonModule,
    RippleModule,
    TranslocoModule,
    RouterLink,
  ],
  templateUrl: './challenge-join-dumb.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChallengeFormDumbComponent {
  private fb = inject(NonNullableFormBuilder);

  public scope = CHALLENGE_JOIN_TRANSLATION_SCOPE;

  public form = this.fb.group({
    password: ['', [Validators.required, Validators.maxLength(50)]],
  });
  public joined = output<JoinChallengeModel>();
  public currentDate = new Date();

  public invokeSubmit(): void {
    this.form.markAllAsTouched();
    if (this.form.invalid) {
      return;
    }

    const formValue = this.form.getRawValue();
    const value: JoinChallengeModel = {
      ...formValue,
    };
    this.joined.emit(value);
  }
}
