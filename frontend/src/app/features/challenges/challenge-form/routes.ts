import { Routes } from '@angular/router';
import { ChallengeFormSmartComponent } from './challenge-form-smart';

export const routes: Routes = [
  {
    path: '',
    component: ChallengeFormSmartComponent,
  },
];
