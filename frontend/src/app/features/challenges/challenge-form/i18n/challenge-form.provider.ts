import { TransLocoProvider, featureLoader } from 'src/app/features/transloco';

export const CHALLENGE_FORM_TRANSLATION_SCOPE = 'challengeForm';
const loader = () => featureLoader(lang => import(`./${lang}.json`));
export const ChallengeFormTranslocoProvider = TransLocoProvider(
  CHALLENGE_FORM_TRANSLATION_SCOPE,
  () => loader(),
);
