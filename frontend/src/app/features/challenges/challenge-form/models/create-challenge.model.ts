export interface CreateChallengeModel {
  name: string;
  startDate: Date;
  endDate: Date;
}
