import { Injectable, inject } from '@angular/core';
import { CreateChallengeModel } from '../models';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({ providedIn: 'root' })
export class ChallengeFormApiService {
  private httpClient = inject(HttpClient);

  public create(model: CreateChallengeModel): Observable<void> {
    return this.httpClient.post<void>(
      `${environment.apiUrl}/challenges`,
      model,
    );
  }
}
