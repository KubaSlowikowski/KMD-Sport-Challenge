import { Component, inject } from '@angular/core';
import { CreateChallengeModel } from '../models';
import { ChallengeFormDumbComponent } from '../challenge-form-dumb';
import { ChallengeFormApiService } from '../logic';
import { ChallengeFormTranslocoProvider } from '../i18n';

@Component({
  selector: 'app-challenge-form-smart',
  standalone: true,
  imports: [ChallengeFormDumbComponent],
  templateUrl: './challenge-form-smart.component.html',
  providers: [ChallengeFormTranslocoProvider],
})
export class ChallengeFormSmartComponent {
  private service = inject(ChallengeFormApiService);

  public submit(value: CreateChallengeModel): void {
    this.service.create(value).subscribe();
  }
}
