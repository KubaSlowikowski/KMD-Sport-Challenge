import {
  ChangeDetectionStrategy,
  Component,
  inject,
  output,
} from '@angular/core';
import {
  FormsModule,
  NonNullableFormBuilder,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { TranslocoModule } from '@jsverse/transloco';
import { ButtonModule } from 'primeng/button';
import { CalendarModule } from 'primeng/calendar';
import { InputTextModule } from 'primeng/inputtext';
import { CHALLENGE_FORM_TRANSLATION_SCOPE } from '../i18n/challenge-form.provider';
import { CreateChallengeModel } from '../models';
import { RippleModule } from 'primeng/ripple';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-challenge-form-dumb',
  standalone: true,
  imports: [
    FormsModule,
    ReactiveFormsModule,
    InputTextModule,
    ButtonModule,
    RippleModule,
    CalendarModule,
    TranslocoModule,
    RouterLink,
  ],
  templateUrl: './challenge-form-dumb.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChallengeFormDumbComponent {
  private fb = inject(NonNullableFormBuilder);

  public scope = CHALLENGE_FORM_TRANSLATION_SCOPE;

  public form = this.fb.group({
    name: ['', [Validators.required, Validators.maxLength(50)]],
    startDate: [null as Date | null, [Validators.required]],
    endDate: [null as Date | null, [Validators.required]],
  });
  public formSubmitted = output<CreateChallengeModel>();
  public currentDate = new Date();

  public invokeSubmit(): void {
    this.form.markAllAsTouched();
    if (this.form.invalid) {
      return;
    }

    const formValue = this.form.getRawValue();
    const value: CreateChallengeModel = {
      ...formValue,
      startDate: formValue.startDate!,
      endDate: formValue.endDate!,
    };
    this.formSubmitted.emit(value);
  }

  public getOneDayAfter(date: Date): Date {
    return new Date(date.getTime() + 24 * 60 * 60 * 1000);
  }
}
