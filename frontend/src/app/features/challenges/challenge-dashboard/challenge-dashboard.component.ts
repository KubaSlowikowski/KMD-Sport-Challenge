import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { ActivatedRoute, RouterModule } from '@angular/router';
import { FeedStore } from '../../feed/+store';
import { RankingSelector, RankingState } from '../../ranking-store';
import { Store } from '@ngrx/store';
import { ChallengeContext } from '../challenge-logic';
import { map } from 'rxjs';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

@Component({
  selector: 'sports-app-challenge-dashboard',
  standalone: true,
  templateUrl: './challenge-dashboard.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [CommonModule, RouterModule],
  providers: [ChallengeContext],
})
export class ChallengeDashboardComponent {
  private feedStore = inject(FeedStore);
  private rankingStateStore = inject(Store<RankingState>);
  private route = inject(ActivatedRoute);
  private challengeContext = inject(ChallengeContext);

  public $currentFeedPage = this.feedStore.$currentPage;

  public $currentCategory = this.rankingStateStore.selectSignal(
    RankingSelector.getSelectedCategoryName,
  );

  constructor() {
    this.route.params
      .pipe(
        map(p => p['challengeId']),
        takeUntilDestroyed(),
      )
      .subscribe(challengeId => {
        this.challengeContext.set(challengeId);
      });
  }
}
