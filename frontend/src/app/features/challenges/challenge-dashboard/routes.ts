import { Routes } from '@angular/router';
import { ChallengeDashboardComponent } from './challenge-dashboard.component';

export const routes: Routes = [
  {
    path: '',
    component: ChallengeDashboardComponent,
    children: [
      {
        path: 'feed',
        loadChildren: () =>
          import('../../feed/routes').then(mod => mod.FEED_LIST_ROUTES),
      },
      {
        path: 'profile',
        loadChildren: () =>
          import('../../profile/routes').then(mod => mod.routes),
      },
      {
        path: 'ranking',
        loadChildren: () =>
          import('../../ranking/routes').then(mod => mod.routes),
      },
      {
        path: 'missing-exertion',
        loadChildren: () =>
          import('../../missing-exertion/routes').then(mod => mod.routes),
      },
      {
        path: 'admin',
        loadChildren: () =>
          import('../../admin/routes').then(mod => mod.ADMIN_ROUTES),
      },
      {
        path: '**',
        redirectTo: 'feed',
      },
    ],
  },
];
