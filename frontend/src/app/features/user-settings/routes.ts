import { Routes } from '@angular/router';
import { UserSettingsComponent } from './user-settings.component';

export const routes: Routes = [
  {
    path: '',
    component: UserSettingsComponent,
  },
];
