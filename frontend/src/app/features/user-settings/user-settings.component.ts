import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Store } from '@ngrx/store';
import { Router, RouterLink } from '@angular/router';
import { ConfirmationDialogService } from '../confirmation-dialog';
import { UserActions, UserState } from '../user';

@Component({
  selector: 'sports-app-user-settings',
  standalone: true,
  imports: [CommonModule, RouterLink],
  templateUrl: './user-settings.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserSettingsComponent {
  private store = inject(Store<UserState>);
  private confirmationService = inject(ConfirmationDialogService);
  private router = inject(Router);

  public logout(): void {
    this.store.dispatch(UserActions.logout());
  }

  public leaveChallenge(): void {
    this.confirmationService.ask({
      title: 'Are you sure you want to leave our challenge?',
      text: 'You can always rejoin the challenge, but in general we will miss you 🥺.',
      approveLabel: 'Leave it',
      declineLabel: 'Stay and fight',
      approveFn: () => {
        this.store.dispatch(UserActions.leaveChallenge());
      },
    });
  }
}
